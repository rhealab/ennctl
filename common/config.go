package common

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"fmt"

	"gopkg.in/yaml.v2"

	"github.com/mitchellh/go-homedir"
)

// AppCtx presents the context of running command
var AppCtx = &AppContext{}

// LoadConfig reads config from $HOME/.ennctl/config
func LoadConfig() (*EnnConfig, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}
	cfgFile := filepath.Join(home, ".ennctl", "config")
	data, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: fail to load ennctl config file.\nplease refer to %s for help.\n", HelpLink)
		return nil, err
	}
	cfg := &EnnConfig{}
	if err = yaml.Unmarshal(data, cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}

// InitContext initialize the application context
func InitContext() error {
	ec, err := LoadConfig()
	if err != nil {
		return err
	}
	ecm := ec.EnnConfigMap()
	if err = ecm.Valid(); err != nil {
		return err
	}
	ctxName := ecm.CurrentContext
	userName := ecm.Contexts[ctxName].User
	clusterName := ecm.Contexts[ctxName].Cluster
	namespace := ecm.Contexts[ctxName].Namespace
	u, err := ecm.Users[userName].GetUsername()
	if err != nil {
		return err
	}
	AppCtx.Username = *u
	p, err := ecm.Users[userName].GetPassword()
	if err != nil {
		return err
	}
	AppCtx.Password = *p
	if &namespace == nil || len(namespace) == 0 {
		namespace = "default"
	}
	AppCtx.Namespace = namespace
	AppCtx.GatewayURL = ecm.Clusters[clusterName].Gateway
	AppCtx.TerminalURL = ecm.Clusters[clusterName].Terminal
	AppCtx.SkipAuth = ecm.Clusters[clusterName].SkipAuth
	if !AppCtx.SkipAuth {
		t, err := Login()
		if err != nil {
			return err
		}
		AppCtx.Token = *t
	} else {
		AppCtx.Token = ""
	}
	if err := AppCtx.Valid(); err != nil {
		return err
	}
	return nil
}

// InitKubeConfig initialize the application context
func InitKubeConfig() error {
	ec, err := LoadConfig()
	if err != nil {
		return err
	}
	return ec.KubeConfig().RefreshKubeConfig()
}

//LoadContext load context from ennctl config.
func LoadContext() error {
	home, err := homedir.Dir()
	if err != nil {
		return err
	}
	return os.Remove(filepath.Join(home, ".ennctl", "kube"))
}
