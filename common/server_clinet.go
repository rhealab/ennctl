package common

import (
	"bytes"
	"crypto/tls"
	"io/ioutil"
	"net/http"
)

// Exec kubernetes client for ennctl send http request to kubernetes api server
func Exec(retry bool, url string, method string, jsonBytes []byte) ([]byte, int, error) {
	Logger.Log("url", url)
	Logger.Log("json", string(jsonBytes))

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonBytes))
	if err != nil {
		return nil, -1, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-client-type", ClientTypeEnnctl)
	req.Header.Set("Authorization", "Bearer "+AppCtx.Token)

	res, err := client.Do(req)

	if err != nil {
		return nil, -1, err
	}

	defer res.Body.Close()

	resBody, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil, -1, err
	}

	if !AppCtx.SkipAuth && !retry && (res.StatusCode == 401) {
		token, err := Login()
		if err != nil {
			return nil, -1, err
		}
		// we also need load token to memory for this query
		AppCtx.Token = *token
		return Exec(true, url, method, jsonBytes)
	}

	return resBody, res.StatusCode, nil
}
