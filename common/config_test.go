package common

import "testing"
import "github.com/stretchr/testify/assert"

func TestLoadKubeConfig(t *testing.T) {
	LoadConfig()
}

func TestInitContext(t *testing.T) {
	InitContext()
	assert.NotNil(t, AppCtx.Username)
	assert.NotNil(t, AppCtx.Password)
	assert.NotNil(t, AppCtx.Token)
	assert.NotNil(t, AppCtx.GatewayURL)
	assert.NotNil(t, AppCtx.TerminalURL)
}
