package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {
	InitContext()
	tk, err := Login()
	if *tk == "" {
		t.Error("token is not found")
	}
	assert.Equal(t, err, nil)
}
