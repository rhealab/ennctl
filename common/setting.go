package common

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	homedir "github.com/mitchellh/go-homedir"
	yaml "gopkg.in/yaml.v1"
)

// Setting is the struct of cli setting passed by user from command line
type Setting struct {
	EnnContext string
}

// InitContext initialize app context
func (s *Setting) InitContext() error {
	var ctxName string
	ec, err := s.LoadConfig()
	if err != nil {
		return err
	}
	ecm := ec.EnnConfigMap()
	if err = ecm.Valid(); err != nil {
		return err
	}
	// Handle Context
	if s.EnnContext == EmptyString {
		ctxName = ecm.CurrentContext
	} else {
		ctxName = s.EnnContext
		if ecm.Contexts[ctxName] == nil {
			return fmt.Errorf("Error in configuration: context was not found for specified context: %s", ctxName)
		}
	}
	// Handle cluster, username and password
	userName := ecm.Contexts[ctxName].User
	clusterName := ecm.Contexts[ctxName].Cluster
	u, err := ecm.Users[userName].GetUsername()
	if err != nil {
		return err
	}
	AppCtx.Username = *u
	p, err := ecm.Users[userName].GetPassword()
	if err != nil {
		return err
	}
	AppCtx.Password = *p
	// Handle namespace
	namespace := ecm.Contexts[ctxName].Namespace
	if &namespace == nil || len(namespace) == 0 {
		namespace = "default"
	}
	AppCtx.Namespace = namespace
	// Handle Gateway && Terminal
	AppCtx.GatewayURL = ecm.Clusters[clusterName].Gateway
	AppCtx.TerminalURL = ecm.Clusters[clusterName].Terminal
	AppCtx.SkipAuth = ecm.Clusters[clusterName].SkipAuth

	if !AppCtx.SkipAuth {
		t, err := Login()
		if err != nil {
			return err
		}
		AppCtx.Token = *t
	} else {
		AppCtx.Token = ""
	}
	if err := AppCtx.Valid(); err != nil {
		return err
	}
	return nil
}

// LoadConfig load config with setting
func (s *Setting) LoadConfig() (*EnnConfig, error) {
	var p string
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}
	p = filepath.Join(home, ".ennctl", "config")

	data, err := ioutil.ReadFile(p)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: fail to load ennctl config file.\nplease refer to %s for help.\n", HelpLink)
		return nil, err
	}
	cfg := &EnnConfig{}
	if err = yaml.Unmarshal(data, cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
