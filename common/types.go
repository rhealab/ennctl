package common

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"

	homedir "github.com/mitchellh/go-homedir"
)

// EnnConfig new enn config
type EnnConfig struct {
	Clusters       []*NamedEnnCluster `yaml:"clusters"`
	Users          []*NamedEnnUser    `yaml:"users"`
	Contexts       []*NamedEnnContext `yaml:"contexts"`
	CurrentContext string             `yaml:"current-context"`
}

// ToYAML convert to yaml bytes
func (ec *EnnConfig) ToYAML() ([]byte, error) {
	return yaml.Marshal(ec)
}

// RefreshEnnConfig refresh ennctl config file
func (ec *EnnConfig) RefreshEnnConfig() error {
	home, err := homedir.Dir()
	if err != nil {
		return err
	}
	ecBytes, err := ec.ToYAML()
	if err != nil {
		return err
	}
	cfgFile := filepath.Join(home, ".ennctl", "config")
	ioutil.WriteFile(cfgFile, ecBytes, 0644)
	os.Remove(filepath.Join(home, ".ennctl", "token"))
	return nil
}

// KubeConfig refresh ennctl config file
func (ec *EnnConfig) KubeConfig() *KubeConfig {
	var kubeClusters []*NamedKubeCluster
	var kubeUsers []*NamedKubeUser
	for _, c := range ec.Clusters {
		kubeClusters = append(kubeClusters, c.NamedKubeCluster())
	}
	for _, u := range ec.Users {
		kubeUsers = append(kubeUsers, u.NamedKubeUser())
	}
	return &KubeConfig{
		Clusters:       kubeClusters,
		Users:          kubeUsers,
		Contexts:       ec.Contexts,
		CurrentContext: ec.CurrentContext,
	}
}

// EnnConfigMap convert EnnConfig to EnnConfigMap
func (ec *EnnConfig) EnnConfigMap() *EnnConfigMap {
	clusters := make(map[string]*EnnCluster)
	for _, cluster := range ec.Clusters {
		clusters[cluster.Name] = cluster.Cluster
	}
	users := make(map[string]*EnnUser)
	for _, user := range ec.Users {
		users[user.Name] = user.User
	}
	contexts := make(map[string]*EnnContext)
	for _, context := range ec.Contexts {
		contexts[context.Name] = context.Context
	}
	return &EnnConfigMap{
		Clusters:       clusters,
		Users:          users,
		Contexts:       contexts,
		CurrentContext: ec.CurrentContext,
	}
}

// EnnCluster holds information for enn backend
type EnnCluster struct {
	Gateway  string `yaml:"gatewayURL"`
	Terminal string `yaml:"terminalURL"`
	SkipAuth bool   `yaml:"skipAuth"`
}

// NamedEnnCluster enn cluster with name
type NamedEnnCluster struct {
	Name    string      `yaml:"name"`
	Cluster *EnnCluster `yaml:"cluster"`
}

// NamedKubeCluster to NamedKubeCluster
func (nec *NamedEnnCluster) NamedKubeCluster() *NamedKubeCluster {
	nkc := &NamedKubeCluster{
		Name: nec.Name,
		Cluster: &KubeCluster{
			Server:   fmt.Sprintf("%s/gw/as", nec.Cluster.Gateway),
			Insecure: true,
		},
	}
	return nkc
}

// EnnUser holds confidential info.
type EnnUser struct {
	Username string `yaml:"username,omitempty"`
	Password string `yaml:"password,omitempty"`
	//+optional could be base64, plaintext
	Type *UserType `yaml:"type,omitempty"`
}

// GetUsername get user name based on its type
func (eu *EnnUser) GetUsername() (*string, error) {
	if eu.Type == nil {
		return &eu.Username, nil
	}
	switch *eu.Type {
	case Base64UserType:
		b, err := base64.StdEncoding.DecodeString(eu.Username)
		if err != nil {
			return nil, err
		}
		ret := string(b)
		return &ret, nil
	case PlaintextUserType:
		return &eu.Username, nil
	default:
		return &eu.Username, nil
	}
}

// GetPassword get user name based on its type
func (eu *EnnUser) GetPassword() (*string, error) {
	if eu.Type == nil {
		return &eu.Password, nil
	}
	switch *eu.Type {
	case Base64UserType:
		b, err := base64.StdEncoding.DecodeString(eu.Password)
		if err != nil {
			return nil, err
		}
		ret := string(b)
		return &ret, nil
	case PlaintextUserType:
		return &eu.Password, nil
	default:
		return &eu.Password, nil
	}
}

// UserType for security
type UserType string

const (
	//PlaintextUserType user info is plaintext
	PlaintextUserType UserType = "plaintext"
	//Base64UserType user info is base64 encoded
	Base64UserType UserType = "base64"
)

// NamedEnnUser enn user with name
type NamedEnnUser struct {
	Name string   `yaml:"name"`
	User *EnnUser `yaml:"user"`
}

// NamedKubeUser to NamedKubeCluster
func (neu *NamedEnnUser) NamedKubeUser() *NamedKubeUser {
	u, _ := neu.User.GetUsername()
	p, _ := neu.User.GetPassword()
	nku := &NamedKubeUser{
		Name: neu.Name,
		User: &KubeUser{
			Username: *u,
			Password: *p,
		},
	}
	return nku
}

// NamedKubeUser enn user with name
type NamedKubeUser struct {
	Name string    `yaml:"name"`
	User *KubeUser `yaml:"user"`
}

// KubeUser holds confidential info.
type KubeUser struct {
	Username string `yaml:"username,omitempty"`
	Password string `yaml:"password,omitempty"`
}

// EnnContext holds reference to cluster/user pari
type EnnContext struct {
	Cluster   string `yaml:"cluster"`
	Namespace string `yaml:"namespace,omitempty"`
	User      string `yaml:"user"`
}

// NamedEnnContext enn user with name
type NamedEnnContext struct {
	Name    string      `yaml:"name"`
	Context *EnnContext `yaml:"context"`
}

// EnnConfigMap is the map version of enn config
type EnnConfigMap struct {
	// Clusters is a map of referencable names to cluster configs
	Clusters map[string]*EnnCluster `yaml:"clusters"`
	// AuthInfos is a map of referencable names to user configs
	Users map[string]*EnnUser `yaml:"users"`
	// Contexts is a map of referencable names to context configs
	Contexts map[string]*EnnContext `yaml:"contexts"`
	// CurrentContext is the name of the context that you would like to use by default
	CurrentContext string `yaml:"current-context"`
}

// Valid check EnnConfigMap is valid or not
func (ecm *EnnConfigMap) Valid() error {
	ctx := ecm.CurrentContext
	if ecm.Contexts[ctx] == nil {
		return fmt.Errorf("Error in configuration: context was not found for specified context: %s", ctx)
	}
	cluster := ecm.Contexts[ctx].Cluster
	if ecm.Clusters[cluster] == nil {
		return fmt.Errorf("Error in configuration: cluster was not found for specified cluster: %s", cluster)
	}
	user := ecm.Contexts[ctx].User
	if ecm.Users[user] == nil {
		return fmt.Errorf("Error in configuration: user was not found for specified user: %s", user)
	}
	return nil
}

// AppContext defines the required context for command application
type AppContext struct {
	SkipAuth    bool
	Token       string
	Username    string
	Password    string
	Namespace   string
	GatewayURL  string
	TerminalURL string
}

// Valid check AppContext valid or not
func (ac *AppContext) Valid() error {
	if ac.GatewayURL == "" {
		return fmt.Errorf("Error in configuration: required field missing. \"gatewayURL\" is required.\nRefer to %s for help", HelpLink)
	}
	if ac.TerminalURL == "" {
		return fmt.Errorf("Error in configuration: required field missing. \"terminalURL\" is required.\nRefer to %s for help", HelpLink)
	}
	if !ac.SkipAuth {
		if ac.Username == "" {
			return fmt.Errorf("Error in configuration: required field missing. \"username\" is required if skipAuth is false.\nRefer to %s for help", HelpLink)
		}
		if ac.Password == "" {
			return fmt.Errorf("Error in configuration: required field missing. \"password\" is required if skipAuth is false.\nRefer to %s for help", HelpLink)
		}
	}
	return nil
}

// KubeConfigPath path of generated kube config file
var KubeConfigPath string

// KubeConfig new enn config
type KubeConfig struct {
	Clusters       []*NamedKubeCluster `yaml:"clusters"`
	Users          []*NamedKubeUser    `yaml:"users"`
	Contexts       []*NamedEnnContext  `yaml:"contexts"`
	CurrentContext string              `yaml:"current-context"`
}

// YAML convert to yaml bytes
func (kc *KubeConfig) YAML() ([]byte, error) {
	return yaml.Marshal(kc)
}

// RefreshKubeConfig refresh ennctl config file
func (kc *KubeConfig) RefreshKubeConfig() error {
	home, err := homedir.Dir()
	if err != nil {
		return err
	}
	y, err := kc.YAML()
	if err != nil {
		return err
	}
	kubeConfigFile := filepath.Join(home, ".ennctl", "kube")
	ioutil.WriteFile(kubeConfigFile, y, 0777)
	KubeConfigPath = kubeConfigFile
	return nil
}

// KubeCluster holds information for enn backend
type KubeCluster struct {
	Server   string `yaml:"server"`
	Insecure bool   `yaml:"insecure-skip-tls-verify"`
}

// NamedKubeCluster enn cluster with name
type NamedKubeCluster struct {
	Name    string       `yaml:"name"`
	Cluster *KubeCluster `yaml:"cluster"`
}

// ErrorCodeResponse is the struct used in cc backend response.
type ErrorCodeResponse struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Payload map[string]interface{} `json:"payload"`
}

func (err ErrorCodeResponse) Error() string {
	return err.Message
}

//PublicKeyResponse response from console for pem public key.
type PublicKeyResponse struct {
	PemKey string `json:"publicKey"`
}
