package common

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// LoginResponse presents the response of login REST api
type LoginResponse struct {
	Message string `json:"message"`
	Token   string `json:"token"`
	Status  int32  `json:"status"`
}

// LoginRequest presents the request payload of login API
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Login will call backend API with username and password. Return token if successful.
func Login() (*string, error) {
	// disable tls
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Transport: tr,
	}

	// encrypt password with public key
	pk, err := getPublicKey()
	if err != nil {
		return nil, err
	}
	encrypted, err := rsa.EncryptOAEP(sha1.New(), rand.Reader, pk, []byte(AppCtx.Password), []byte(""))
	if err != nil {
		return nil, err
	}
	pBase64 := base64.StdEncoding.EncodeToString(encrypted)
	// generate url and json payload
	u := fmt.Sprintf("%s/gw/ac/api/v1/login/encrypt", AppCtx.GatewayURL)
	j, err := json.Marshal(LoginRequest{AppCtx.Username, pBase64})
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", u, bytes.NewBuffer(j))
	if err != nil {
		return nil, err
	}
	Logger.Log("url", u)
	Logger.Log("json", string(j))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-client-type", ClientTypeEnnctl)
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if !IsSuccessHTTPCode(res.StatusCode) {
		var errCode ErrorCodeResponse
		json.Unmarshal(body, &errCode)
		fmt.Fprintf(os.Stderr, "error: %s\npayload:%v\n", errCode.Error(), errCode.Payload)
		os.Exit(1)
	}
	Logger.Log("response_status", res.StatusCode, "response_body:", string(body))
	var loginResponse LoginResponse
	if err := json.Unmarshal(body, &loginResponse); err != nil {
		return nil, err
	}
	return &loginResponse.Token, nil
}

// LoadToken reads cached token in local file $HOME/.ennctl/token
// func LoadToken() string {
// 	home, _ := homedir.Dir()
// 	token, err := ioutil.ReadFile(filepath.Join(home, ".ennctl", "token"))
// 	if err != nil {
// 		return ""
// 	}
// 	return string(token)
// }

func getPublicKey() (*rsa.PublicKey, error) {
	var err error
	// disable tls
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Transport: tr,
	}

	u := fmt.Sprintf("%s/gw/ac/api/v1/key/public", AppCtx.GatewayURL)
	req, err := http.NewRequest(GET, u, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-client-type", ClientTypeEnnctl)
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if !IsSuccessHTTPCode(res.StatusCode) {
		var errCode ErrorCodeResponse
		json.Unmarshal(body, &errCode)
		fmt.Fprintf(os.Stderr, "Fail to get public key from account server.\nerror: %s\npayload:%v\n", errCode.Error(), errCode.Payload)
		os.Exit(1)
	}
	Logger.Log("response_status", res.StatusCode, "response_body:", string(body))
	var pkr PublicKeyResponse
	if err = json.Unmarshal(body, &pkr); err != nil {
		return nil, err
	}
	block, _ := pem.Decode([]byte(pkr.PemKey))
	k, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	key := k.(*rsa.PublicKey)
	return key, nil
}
