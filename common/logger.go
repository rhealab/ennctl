package common

import (
	"os"

	"github.com/go-kit/kit/log"
)

// Logger is the fundamental interface for all log operations.
var Logger log.Logger

func init() {
	Logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	Logger = log.With(Logger, "app", "ennctl")
	Logger = log.With(Logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	Logger = log.NewNopLogger()
}
