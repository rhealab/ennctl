package common

// ClientTypeEnnctl requests from ennctl should have this client type
// for audit function in console ecosystem.
const ClientTypeEnnctl string = "ennctl"

// http method
const (
	// GET http method
	GET = "GET"
	// POST http method
	POST = "POST"
	// PUT http method
	PUT = "PUT"
	// DELETE http method
	DELETE = "DELETE"
)

//EmptyString representation of empty string
const EmptyString = ""

// HelpLink link for ennctl setup guide
const HelpLink string = "http://10.19.140.200:32243/getting-started/how-to-setup"
