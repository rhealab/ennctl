package common

import (
	"fmt"
	"strconv"
	"time"
)

// BytesToString is used to convert bytes to byte string
func BytesToString(bytes float64) string {
	var ret string
	if bytes < 1024 {
		ret = fmt.Sprintf("%.2fBi", bytes)
	} else if bytes < (1024 * 1024) {
		ret = fmt.Sprintf("%.2fKi", bytes/1024)
	} else if bytes < (1024 * 1024 * 1024) {
		ret = fmt.Sprintf("%.2fMi", bytes/1024/1024)
	} else {
		ret = fmt.Sprintf("%.2fGi", bytes/1024/1024/1024)
	}
	return ret
}

// TimestampToAge is used to convert UTC timestamp to age string
func TimestampToAge(input int64) string {
	if input == 0 {
		return "Unknown"
	}
	tmp := time.Now().Unix() - input/1000
	var ret string
	if tmp < 0 {
		ret = "0s"
	} else if tmp < 60 {
		ret = fmt.Sprintf("%ds", tmp)
	} else if tmp < (60 * 60) {
		ret = fmt.Sprintf("%dm", tmp/60)
	} else if tmp < (60 * 60 * 24) {
		ret = fmt.Sprintf("%dh", tmp/60/60)
	} else {
		ret = fmt.Sprintf("%dd", tmp/60/60/24)
	}
	return ret
}

// StringToBytes is a util function parsing strings.
func StringToBytes(input string) int64 {
	ret := stringToInt64(input)
	if ret != -1 {
		return ret
	}
	unit := input[len(input)-2:]
	digits := input[:len(input)-2]
	switch unit {
	case "Bi", "B":
		ret = stringToInt64(digits)
	case "Ki", "KB":
		ret = stringToInt64(digits)
		if ret != -1 {
			ret = ret * 1024
		}
	case "Mi", "MB":
		ret = stringToInt64(digits)
		if ret != -1 {
			ret = ret * 1024 * 1024
		}
	case "Gi", "GB":
		ret = stringToInt64(digits)
		if ret != -1 {
			ret = ret * 1024 * 1024 * 1024
		}
	default:
		ret = -1
	}
	return ret
}

func stringToInt64(input string) int64 {
	if num, err := strconv.Atoi(input); err == nil {
		return int64(num)
	}
	return -1
}

// StringToFloat64 is a util function used to convert string to float64
func StringToFloat64(input string) float64 {
	if num, err := strconv.ParseFloat(input, 64); err == nil {
		return num
	}
	return -1
}

//IsSuccessHTTPCode is a util function to determine whether an HTTP request is success or not
func IsSuccessHTTPCode(httpStatusCode int) bool {
	return httpStatusCode >= 100 && httpStatusCode < 400
}
