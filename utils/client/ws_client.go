package client

import (
	"cc/ennctl/common"
	"fmt"
	"io"
	"os"

	"github.com/ZihanTang/term"
	"github.com/gorilla/websocket"
	"golang.org/x/sys/unix"
)

// StartWSClient is the entrypoint to start a websocket connection
// with tty, and provide an interactive experience for for exec command.
func StartWSClient(url string) error {
	ws, err := getWebSocketConnection(false, url)
	if err != nil {
		return err
	}
	t, _ := term.Open("/dev/tty")
	defer ws.Close()
	defer t.Restore()
	defer t.Close()
	ch := make(chan byte, 1024)
	ch <- 1
	go readFromWebsocket(ws, os.Stdout, ch, t)
	return serveUserInput(ws, os.Stdin, ch, t)
}

// getWebSocketConnection to acquire a websocket connection with cc-terminal
// and provide 1 retry if token is not correct.
func getWebSocketConnection(retry bool, url string) (*websocket.Conn, error) {
	ws, _, err := websocket.DefaultDialer.Dial(url, nil)
	common.Logger.Log("websockt url", url)
	if !retry && (err != nil) {
		var token *string
		token, err = common.Login()
		if err != nil {
			return nil, err
		}
		common.AppCtx.Token = *token
		return getWebSocketConnection(true, url)
	}
	if err != nil {
		return nil, err
	}
	return ws, nil
}

//serveUserInput read user's input from keyboard and send them to cc-terminal
//via a websocket connection.
func serveUserInput(ws *websocket.Conn, r io.Reader, ch chan byte, t *term.Term) error {
	// /n 10
	// /r 13
	//  pre1, pre2, pre3,pre4, cur
	// t, _ := term.Open("/dev/tty")
	defer ws.Close()
	defer t.Restore()
	defer t.Close()
	term.RawMode(t)
	uws, err := unix.IoctlGetWinsize(int(t.GetFd()), unix.TIOCGWINSZ)
	if err != nil {
		return err
	}
	cmd := fmt.Sprintf("stty cols %d rows %d\n", uws.Col, uws.Row)
	if err := ws.WriteMessage(websocket.BinaryMessage, []byte(cmd)); err != nil {
		t.Restore()
		t.Close()
		ws.Close()
		return err
	}
	bytes := make([]byte, 1)
	for {
		_, err := t.Read(bytes)
		if err != nil {
			t.Restore()
			t.Close()
			ws.Close()
			return err
		}
		if err := ws.WriteMessage(websocket.BinaryMessage, bytes); err != nil {
			t.Restore()
			t.Close()
			ws.Close()
			return err
		}
	}
}

//readFromWebsocket read content from cc-terminal via websocket connection and
//display them to stdout
func readFromWebsocket(ws *websocket.Conn, w io.Writer, ch chan byte, t *term.Term) {
	defer ws.Close()
	defer t.Restore()
	defer t.Close()
	for {
		_, message, err := ws.ReadMessage()
		if err != nil {
			if err.Error() != "websocket: close 1000 (normal)" {
				fmt.Printf("error: %s", err.Error())
				t.Restore()
				os.Exit(1)
			}
			t.Restore()
			os.Exit(0)
		}
		if _, err := w.Write(message[1:]); err != nil {
			break
		}
	}
}
