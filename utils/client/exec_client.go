package client

import (
	"io"
	"os"

	"github.com/gorilla/websocket"
)

// ExecClient is the entrypoint for executing commands in a container without a tty.
func ExecClient(url string) error {
	ws, err := getWebSocketConnection(false, url)
	if err != nil {
		return err
	}
	defer ws.Close()
	return readResponse(ws, os.Stdout)
}

//readResponse read response from cc-terminal and display it in stdout
func readResponse(ws *websocket.Conn, w io.Writer) error {
	defer ws.Close()
	for {
		_, message, err := ws.ReadMessage()
		if err != nil {
			if err.Error() != "websocket: close 1000 (normal)" {
				return err
			}
			os.Exit(0)
		}
		if _, err := w.Write(message[1:]); err != nil {
			return err
		}
	}
}
