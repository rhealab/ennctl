package client

import (
	"archive/tar"
	"bufio"
	"crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"

	"encoding/base64"
	"time"

	"github.com/gorilla/websocket"
)

// CopyToPodClient is the entrypoint to copy files to pod
// via cc-terminal using binary tar.
func CopyToPodClient(url string, src FileSpec, dest FileSpec) error {
	ws, err := getWebSocketConnection(false, url)
	defer ws.Close()
	if err != nil {
		return err
	}
	r, w := io.Pipe()
	go func() {
		defer w.Close()
		err := makeTar(w, src.File, dest.File)
		if err != nil {
			fmt.Println(err.Error())
		}
	}()
	reader := bufio.NewReader(r)
	buf := make([]byte, 0, 1024)
	for {
		n, err := reader.Read(buf[:cap(buf)])
		buf = buf[:n]
		if n == 0 {
			if err == nil {
				continue
			}
			if err == io.EOF {
				break
			}
			return err
		}
		ws.WriteMessage(websocket.BinaryMessage, buf)
	}
	ws.WriteMessage(websocket.CloseMessage, nil)
	// wait close message
	for {
		_, _, err := ws.ReadMessage()
		if err != nil {
			break
		}
	}
	return nil
}

//makeTar is the function to tar files to pipe
func makeTar(w io.Writer, src, dest string) error {
	tw := tar.NewWriter(w)
	defer tw.Close()
	destDir, destFile := path.Split(dest)
	if destFile == "" {
		dest = path.Join(destDir, path.Base(src))
	}
	srcPath := path.Clean(src)
	destPath := path.Clean(dest)
	return tarRecursive(path.Dir(srcPath), path.Base(srcPath), path.Dir(destPath), path.Base(destPath), tw)
}

//tarRecursive is the function to tar file recursively
func tarRecursive(srcBase, srcFile, destBase, destFile string, tw *tar.Writer) error {
	filePath := path.Join(srcBase, srcFile)
	stat, err := os.Lstat(filePath)
	if err != nil {
		return err
	}
	if stat.IsDir() {
		// directory
		files, err := ioutil.ReadDir(filePath)
		if err != nil {
			return err
		}
		if len(files) == 0 {
			// empty directory
			hdr, _ := tar.FileInfoHeader(stat, filePath)
			hdr.Name = destFile
			if err := tw.WriteHeader(hdr); err != nil {
				return err
			}
		}
		for _, f := range files {
			if err := tarRecursive(srcBase, path.Join(srcFile, f.Name()), destBase, path.Join(destFile, f.Name()), tw); err != nil {
				return err
			}
		}
		return nil
	} else if stat.Mode()&os.ModeSymlink != 0 {
		// symlink
		hdr, _ := tar.FileInfoHeader(stat, filePath)
		target, err := os.Readlink(filePath)
		if err != nil {
			return err
		}
		hdr.Linkname = target
		hdr.Name = path.Join(destBase, destFile)
		if err := tw.WriteHeader(hdr); err != nil {
			return err
		}

	} else {
		// file
		hdr, err := tar.FileInfoHeader(stat, filePath)
		if err != nil {
			return err
		}
		hdr.Name = path.Join(destBase, destFile)
		if err = tw.WriteHeader(hdr); err != nil {
			return err
		}
		f, err := os.Open(filePath)
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = io.Copy(tw, f)
		return err
	}
	return nil
}

// CopyFromPodClient client copy files/directory from pod via cc-terminal
// using tar binary.
func CopyFromPodClient(url string, src FileSpec, dest FileSpec) error {
	// Copy tar from server
	ws, err := getWebSocketConnection(false, url)
	defer ws.Close()
	if err != nil {
		return err
	}
	filename := getCurrentTimeSha256()
	f, _ := os.Create(filename)
	defer f.Close()
	for {
		_, message, err := ws.ReadMessage()
		if err != nil {
			break
		}
		if message[0] == 1 {
			f.Write(message[1:])
		} else {
			f.Write(message)
		}
	}
	f.Close()
	// Read data from tmp file
	f, _ = os.Open(filename)
	cnt := -1
	tr := tar.NewReader(f)
	srcBase := src.File
	destBase := dest.File
	for {
		header, err := tr.Next()
		// Handle error
		if err != nil {
			if err != io.EOF {
				fmt.Println(err.Error())
			}
			break
		}
		cnt++
		mode := header.FileInfo().Mode()
		destDir, destFile := path.Split(destBase)
		if destFile == "" {
			destBase = path.Join(destDir, path.Base(srcBase))
		}
		filename := path.Join(destBase, header.Name[len(srcBase):])
		baseName := path.Dir(filename)
		// parrent dir
		if err = os.MkdirAll(baseName, 0755); err != nil {
			fmt.Println(err.Error())
			break
		}
		// if filename is directory, make it and continue
		if header.FileInfo().IsDir() {
			if err = os.MkdirAll(filename, 0755); err != nil {
				fmt.Println(err.Error())
			}
			continue
		}
		if cnt == 0 && !header.FileInfo().IsDir() {
			exists, err1 := dirExists(filename)
			if err1 != nil {
				fmt.Println(err1.Error())
				break
			}
			if exists {
				filename = path.Join(filename, path.Base(filename))
			}
		}
		if mode&os.ModeSymlink != 0 {
			err = os.Symlink(header.Linkname, filename)
			if err != nil {
				fmt.Println(header.Linkname)
				fmt.Println(filename)
				fmt.Println(err.Error())
				break
			}
		} else {
			outFile, err := os.Create(filename)
			if err != nil {
				fmt.Println(err.Error())
				break
			}
			defer outFile.Close()
			io.Copy(outFile, tr)
			outFile.Close()
		}
	}

	os.Remove(filename)
	if cnt == -1 {
		return fmt.Errorf("error: %s no such file or directory", src.File)
	}
	return nil
}

//dirExists check whether a directory exists
func dirExists(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err == nil && fi.IsDir() {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func getCurrentTimeSha256() string {
	hasher := sha256.New()
	hasher.Write([]byte(time.Now().String()))
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}
