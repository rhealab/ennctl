package client

import (
	"net/url"
	"strings"
)

// FileSpec the struct used in cp function, it contains podName and fileName for
// cp files from or to pod.
type FileSpec struct {
	PodName string
	File    string
}

//Init split user's input to FileSpec.
func (file *FileSpec) Init(str string) *FileSpec {
	chunks := strings.Split(str, ":")
	if len(chunks) == 1 {
		file.File = chunks[0]
	} else if len(chunks) == 2 {
		file.PodName = chunks[0]
		file.File = chunks[1]
	}
	return file
}

// GetEscapedFile returns url encoding of path
func (file *FileSpec) GetEscapedFile() string {
	return url.PathEscape(file.File)
}
