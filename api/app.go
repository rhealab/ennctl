package api

import (
	"encoding/json"
	"errors"

	"github.com/ghodss/yaml"
)

// App is the sturct for console storage type.
type App struct {
	APIVersion string     `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string     `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
}

// JSON get json string
func (a *App) JSON() ([]byte, error) {
	return json.Marshal(a)
}

// YAML get json string
func (a *App) YAML() ([]byte, error) {
	return yaml.Marshal(a)
}

// Valid get json string
func (a *App) Valid() error {
	if a.APIVersion != AppVersion {
		return errors.New(APIVersionNotSupportedError)
	}
	if a.ObjectMeta.Name == EmptyResource {
		return errors.New(NameIsRequiredError)
	}
	if a.ObjectMeta.Namespace == EmptyResource {
		return errors.New(NamespaceIsRequiredError)
	}
	return nil
}

//AppCreateRequest app to AppCreateRequest
func (a *App) AppCreateRequest() *AppCreateRequest {
	return &AppCreateRequest{
		Name: a.ObjectMeta.Name,
	}
}

// AppCreateRequest is used to construct post body in app creation API
type AppCreateRequest struct {
	Name string `yaml:"name" json:"name"`
}

// JSON to json
func (a AppCreateRequest) JSON() ([]byte, error) {
	return json.Marshal(a)
}
