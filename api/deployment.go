package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/ghodss/yaml"
)

// Console deploy modifies the original k8s deployment as following
// we have a field named storage, which is a map to volume claim field
// we add controlls to annotation likes criticalPod and privilege mode.

// DeploymentList struct for DeploymentList
type DeploymentList struct {
	APIVersion string       `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Items      []Deployment `json:"items,omitempty" yaml:"items,omitempty"`
	Kind       string       `json:"kind,omitempty" yaml:"kind,omitempty"`
}

// Deployment struct for deployment
type Deployment struct {
	APIVersion string            `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string            `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta        `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       DeploymentSpec    `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status     *DeploymentStatus `json:"status,omitempty" yaml:"status,omitempty"`
}

// Valid validation
func (d Deployment) Valid() error {
	if d.ObjectMeta.Name == EmptyResource {
		return errors.New(NameIsRequiredError)
	}
	if d.ObjectMeta.Namespace == EmptyResource {
		return errors.New(NamespaceIsRequiredError)
	}
	return d.Spec.valid()
}

//K8sStylize convert  console style to k8s original style
func (d *Deployment) K8sStylize() *Deployment {
	// set/overwrite X_APP label in meta.label and spec.template.meta.label.
	if d.ObjectMeta.App != nil {
		if d.ObjectMeta.Label == nil {
			d.ObjectMeta.Label = map[string]string{
				ConsoleAppLabel: *d.ObjectMeta.App,
			}
		} else {
			d.ObjectMeta.Label[ConsoleAppLabel] = *d.ObjectMeta.App
		}
		if d.Spec.Template.ObjectMeta.Label == nil {
			d.Spec.Template.ObjectMeta.Label = map[string]string{
				ConsoleAppLabel: *d.ObjectMeta.App,
			}
		} else {
			d.Spec.Template.ObjectMeta.Label[ConsoleAppLabel] = *d.ObjectMeta.App
		}
		d.ObjectMeta.App = nil
	}

	// convert storage
	if d.Spec.Template.Spec.Storages != nil {
		tmp := d.Spec.Template.Spec.Volumes
		for _, storage := range *d.Spec.Template.Spec.Storages {
			tmp = append(tmp, storage.toVolume())
		}
		d.Spec.Template.Spec.Volumes = tmp
		d.Spec.Template.Spec.Storages = nil
	}
	return d
}

//Name get name
func (d *Deployment) Name() string {
	return d.ObjectMeta.Name
}

//App get app
func (d *Deployment) App() string {
	if d.ObjectMeta.Label != nil {
		if val, ok := d.ObjectMeta.Label[ConsoleAppLabel]; ok {
			return val
		}
	}
	return Legacy
}

//Desired get Desired
func (d *Deployment) Desired() string {
	return fmt.Sprintf("%d", *d.Spec.Replicas)
}

//Current get Desired
func (d *Deployment) Current() string {
	return fmt.Sprintf("%d", d.Status.Replicas)
}

//UpToDate get Desired
func (d *Deployment) UpToDate() string {
	return fmt.Sprintf("%d", d.Status.UpdatedReplicas)
}

//Available get Desired
func (d *Deployment) Available() string {
	return fmt.Sprintf("%d", d.Status.AvailableReplicas)
}

//Image get Desired
func (d *Deployment) Image() string {
	ret := Empty
	for _, c := range d.Spec.Template.Spec.Containers {
		ret += c.Image + ","
	}
	if ret != Empty {
		return ret[:len(ret)-1]
	}
	return ret
}

//Containers get Desired
func (d *Deployment) Containers() string {
	ret := Empty
	for _, c := range d.Spec.Template.Spec.Containers {
		ret += c.Name + ","
	}
	if ret != Empty {
		return ret[:len(ret)-1]
	}
	return ret
}

//Age get age
func (d *Deployment) Age() string {
	dur := time.Since(*d.ObjectMeta.CreationTimestamp)
	if dur.Seconds() <= 60 {
		return fmt.Sprintf("%ds", int(dur.Seconds()))
	} else if dur.Minutes() <= 60 {
		return fmt.Sprintf("%dm", int(dur.Minutes()))
	} else if dur.Hours() <= 24 {
		return fmt.Sprintf("%dh", int(dur.Hours()))
	}
	return fmt.Sprintf("%dd", int(dur.Hours()/24))
}

//ConsoleStylize convert console style deployment to k8s original
func (d *Deployment) ConsoleStylize() *Deployment {
	if app, ok := d.ObjectMeta.Label[ConsoleAppLabel]; ok {
		d.ObjectMeta.App = &app
		delete(d.ObjectMeta.Label, ConsoleAppLabel)
	}
	var tmp []StorageTemplate
	var v []Volume
	for _, volume := range d.Spec.Template.Spec.Volumes {
		if volume.PersistentVolumeClaim != nil {
			tmp = append(tmp, volume.toStorageTemplate())
		} else {
			v = append(v, volume)
		}
	}
	d.Spec.Template.Spec.Storages = &tmp
	d.Spec.Template.Spec.Volumes = v
	return d
}

// JSON get json string
func (d *Deployment) JSON() ([]byte, error) {
	return json.Marshal(d)
}

// YAML get json string
func (d *Deployment) YAML() ([]byte, error) {
	return yaml.Marshal(d)
}

// SliceJSON get json string
func (d *Deployment) SliceJSON() ([]byte, error) {
	var tmp []Deployment
	tmp = append(tmp, *d)
	return json.Marshal(tmp)
}

// DeploymentSpec is the specification of deployment
type DeploymentSpec struct {
	Replicas                *int32             `json:"replicas,omitempty" yaml:"replicas,omitempty"`
	Selector                *LabelSelector     `json:"selector,omitempty" yaml:"selector,omitempty"`
	Template                PodTemplateSpec    `json:"template" yaml:"template"`
	Strategy                DeploymentStrategy `json:"strategy,omitempty" yaml:"strategy,omitempty"`
	MinReadySeconds         int32              `json:"minReadySeconds,omitempty" yaml:"minReadySeconds,omitempty"`
	RevisionHistoryLimit    *int32             `json:"revisionHistoryLimit,omitempty" yaml:"revisionHistoryLimit,omitempty"`
	Paused                  bool               `json:"paused,omitempty" yaml:"paused,omitempty"`
	ProgressDeadlineSeconds *int32             `json:"progressDeadlineSeconds,omitempty" yaml:"progressDeadlineSeconds,omitempty"`
}

// Implement validator interface for DeploySpec
func (ds DeploymentSpec) valid() error {
	return nil
}

// DeploymentStrategy describes how to replace existing pods with new ones.
type DeploymentStrategy struct {
	Type          DeploymentStrategyType   `json:"type,omitempty" yaml:"type,omitempty"`
	RollingUpdate *RollingUpdateDeployment `json:"rollingUpdate,omitempty" yaml:"rollingUpdate,omitempty"`
}

// DeploymentStrategyType current update strategy
type DeploymentStrategyType string

const (
	// RecreateDeploymentStrategyType Kill all existing pods before creating new ones.
	RecreateDeploymentStrategyType DeploymentStrategyType = "Recreate"
	// RollingUpdateDeploymentStrategyType Replace the old RCs by new one using rolling update i.e gradually scale down the old RCs and scale up the new one.
	RollingUpdateDeploymentStrategyType DeploymentStrategyType = "RollingUpdate"
)

// RollingUpdateDeployment rolling update configuration
type RollingUpdateDeployment struct {
	MaxUnavailable *int32 `json:"maxUnavailable,omitempty" yaml:"maxUnavailable,omitempty"`
	MaxSurge       *int32 `json:"maxSurge,omitempty" yaml:"maxSurge,omitempty"`
}

// DeploymentStatus most recent status of deployment
type DeploymentStatus struct {
	ObservedGeneration  int64                 `json:"observedGeneration,omitempty" yaml:"observedGeneration,omitempty"`
	Replicas            int32                 `json:"replicas,omitempty" yaml:"replicas,omitempty"`
	UpdatedReplicas     int32                 `json:"updatedReplicas,omitempty" yaml:"updatedReplicas,omitempty"`
	ReadyReplicas       int32                 `json:"readyReplicas,omitempty" yaml:"readyReplicas,omitempty"`
	AvailableReplicas   int32                 `json:"availableReplicas,omitempty" yaml:"availableReplicas,omitempty"`
	UnavailableReplicas int32                 `json:"unavailableReplicas,omitempty" yaml:"unavailableReplicas,omitempty"`
	Conditions          []DeploymentCondition `json:"conditions,omitempty" yaml:"conditions,omitempty"`
	CollisionCount      *int32                `json:"collisionCount,omitempty" yaml:"collisionCount,omitempty"`
}

// DeploymentCondition deployment state
type DeploymentCondition struct {
	Type               DeploymentConditionType `json:"type" yaml:"type"`
	Status             ConditionStatus         `json:"status" json:"status"`
	LastUpdateTime     string                  `json:"lastUpdateTime,omitempty" yaml:"lastUpdateTime,omitempty"`
	LastTransitionTime string                  `json:"lastTransitionTime,omitempty" yaml:"lastTransitionTime,omitempty"`
	Reason             string                  `json:"reason,omitempty" yaml:"reason,omitempty"`
	Message            string                  `json:"message,omitempty" yaml:"message,omitempty"`
}

//DeploymentConditionType type
type DeploymentConditionType string

const (
	//DeploymentAvailable available
	DeploymentAvailable DeploymentConditionType = "Available"
	//DeploymentProgressing Progressing
	DeploymentProgressing DeploymentConditionType = "Progressing"
	//DeploymentReplicaFailure ReplicaFailure
	DeploymentReplicaFailure DeploymentConditionType = "ReplicaFailure"
)
