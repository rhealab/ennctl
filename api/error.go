package api

const (
	// NoResourceNameError is the error message in case of no resource name is provided.
	NoResourceNameError string = "error: resource(s) were provided, but no name specified"
	//AppNameNotMatchError is the error message in case user specify a wrong app name
	AppNameNotMatchError string = "error: app name does not match. Please check the resource name and the app name you privided."
)
const (
	// NameIsRequiredError is the error message in case of creating resource withtout a name
	NameIsRequiredError string = "error: NAME is required"

	//ResourceIsRequiredError enncreate require a resource name to be specified
	ResourceIsRequiredError string = "error: RESOURCE is required"

	// NamespaceIsRequiredError is the error message in case of creating resource withtout a name
	NamespaceIsRequiredError string = "error: NAMESPACE is required"

	//UserDetailError get user detail is not support atm.
	UserDetailError string = "error: get detail of a specific user is not support. please use ennctl get users instead."

	//EditNotAllowedError edit not allowed
	EditNotAllowedError string = "error: the resource type does not exist or edit on requested resource is not allowed."

	//CpRequirePodError pod name is required for cp command
	CpRequirePodError string = "error: you must either copy file from pod, or copy file to pod."

	// RoleIsRequiredError is the error message in case of creating user without specifying a role_assignment
	RoleIsRequiredError string = "error: ROLE is required"

	// AppIsRequiredError is the error message in case of missing app field.
	AppIsRequiredError string = "error: APP is required"

	// ImageIsRequiredError is the error message in case of missing image url in container spec.
	ImageIsRequiredError string = "error: IMAGE is required"

	// APIVersionNotSupportedError in the error message in case of specifying an unsupported API version.
	APIVersionNotSupportedError string = "error: the spercified API version is not supported"

	// FilenameIsRequiredError is the error message in case of create/replace from yaml/json file
	FilenameIsRequiredError string = "error: FILENAME is required."
)

const (
	// UserIDIsRequiredError is the error message in case user.yaml missing userId field
	UserIDIsRequiredError string = "error: USER userId field is required"
	//UserRoleIsNotSupportedError is the error message in case of user.yaml specifying a wront role.
	UserRoleIsNotSupportedError string = "error: USER role is not support. The role field must be any of NS_ADMIN or DEVELOPER"
	// SysAdmListIsRequiredError is the error message in case of sysadmin.yaml specifying empty list for both sysAdmins field.
	SysAdmListIsRequiredError string = "error: SYSADMIN spec.sysAdmins is required."
	// RoleNameIsNotValidError is the error message in case of user specified unsupported role via -r option.
	RoleNameIsNotValidError string = "error: ROLE is not supported. -r should any of developer or nsadmin."
)

const (
	// StorageTypeIsRequired is the error message in case of specifying an unspported storage type
	StorageTypeIsRequired string = "error: STORAGE TYPE is required"
	// StorageAccessModeIsNotSupport is the error message in case of specifying an unspported storage access mode
	StorageAccessModeIsNotSupport string = "error: STORAGE ACCESS MODE is not support, it must be any of ReadWriteOnce, ReadOnlyMany or ReadWriteMany"

	// StorageRequestIsRequiredError is the error message in case storage.yaml missing storage request field.
	StorageRequestIsRequiredError string = "error: STORAGE request field is required"
)

const (
	// CPURequestIsRequiredError is the error message in case ns.yaml missing cpu request field
	CPURequestIsRequiredError string = "error: RESOURCE_QUOTA cpu request field is required"

	// MemoryLimitIsRequiredError is the error message in case ns.yaml missing memory limit field
	MemoryLimitIsRequiredError string = "error: RESOURCE_QUOTA memory limit field is required"

	// MemoryRequestIsRequiredError is the error message in case ns.yaml missing memory request field
	MemoryRequestIsRequiredError string = "error: RESOURCE_QUOTA memory request field is required"

	// HostPathRequestIsRequiredError is the error message in case ns.yaml missing hostpah request field
	HostPathRequestIsRequiredError string = "error: RESOURCE_QUOTA localStorage request field is required"

	// RbdRequestIsRequiredError is the error message in case ns.yaml missing rbd request field
	RbdRequestIsRequiredError string = "error: RESOURCE_QUOTA remoteStorage request field is required"

	// CPULimitIsRequiredError is the error message in case ns.yaml missing cpu limit field
	CPULimitIsRequiredError string = "error: RESOURCE_QUOTA cpu limit field is required"
)

const (
	// ContainerPortIsRequiredError is the error message in case deployment.yaml missing ports information.
	ContainerPortIsRequiredError string = "error: POD containerPort field is required"

	// ContianerPortIsNotValidError is the error message in case deployment.yaml specifying an invalid port number
	ContianerPortIsNotValidError string = "error: POD containerPort filed is not valid, must be a positive integer"

	// ContainerPortProtocolIsNotSupportError is the error message in case deployment.yaml specified unsupported protocol
	ContainerPortProtocolIsNotSupportError string = "error: POD containerPort protocol must be either TCP or UDP"

	// MoutPathIsRequiredError is the error message in case deployment.yaml missing mountPath but specifiying volumeMounts
	MoutPathIsRequiredError string = "error: POD mountPath field is required if you are using volumeMount"
)

const (
	//ReplicasMustBePositiveError is the error message in case deployment.yaml missing or specitying an wrong replicas field.
	ReplicasMustBePositiveError string = "error: DEPLOYMENT replicas field is required and must be an positive integer."

	//AppNameIsRequiredForK8sDeployError is the error message in case user use K8s origin yaml but did not provide app name in flag
	AppNameIsRequiredForK8sDeployError string = "error: DEPLOYMENT -a <app-name> is required flag if you choose to create deployment with k8s original yaml file"

	//LabelIsRequiredError is the error message in case deployment.yaml missing labels filed.
	LabelIsRequiredError string = "error: DEPLOYMENT labels field is required."

	//AppNameIsRequiredForEditDeployment is the error message in case edit deployment but missing appname.
	AppNameIsRequiredForEditDeployment string = "error: DEPLOYMENT <APP-NAME> is required for editting deployment."

	//AppNameIsRequiredForScaleDeployment is the error message in case user scale deployment without specifying appname.
	AppNameIsRequiredForScaleDeployment string = "error: DEPLOYMENT <APP-NAME> is required for scaling deployment."

	//ReplicasMustExistAndBePositiveForScale is the error message in case user forgot to specify replicas or specifyinga  number less than 0.
	ReplicasMustExistAndBePositiveForScale string = "error: SCALE The --replicas=COUNT flag is required, and COUNT must be greater than or equal to 0."
)

const (
	//ServicePortRequiredError is the error message in case service missing ports field.
	ServicePortRequiredError string = "error: SERVICE ports field is required."

	//ServiceTargetPortInvalidError is the error message in case service target port is invalid.
	ServiceTargetPortInvalidError string = "error: SERVICE targetPort must be between 1 and 65535."

	//AppNameIsRequiredForK8sSvcError is the error message in case user use K8s origin yaml but did not provide app name in flag
	AppNameIsRequiredForK8sSvcError string = "error: SERVICE -a <app-name> is required flag if you choose to create service with k8s original yaml file"

	//AppNameIsRequiredForEditService is the error message in case edit service but missing appname.
	AppNameIsRequiredForEditService string = "error: SERVICE <APP-NAME> is required for editting service."

	//ServicePortInvalidError is the error message in case service port is invalid.
	ServicePortInvalidError string = "error: SERVICE port must be between 1 and 65535."

	//ServicePortProtocolIsNotSupportError is the error message in case service protocol is something other than UDP or TCP
	ServicePortProtocolIsNotSupportError string = "error: SERVICE port protocol must be either TCP or UDP"
)

const (
	//PodNameAndCommandAreRequiredError is the error message in case user does not specify podname and command name
	PodNameAndCommandAreRequiredError string = "error: expected `exec POD_NAME COMMAND`\nPOD_NAME and COMMAND are required arguments for the exec command"

	//PodNameIsRequiredError is the error message in case user does not input pod name
	PodNameIsRequiredError string = "error: expected 'log POD_NAME -c [CONTAINER_NAME]'.\nPOD_NAME is a required argument for the log command"

	//PodHaveNoContainerError is the error message in case pod does not have container information
	PodHaveNoContainerError string = "error: the specified pod does not have a running container."
)
const (
	//ScaleNotAllowedError we only allow to scale deployment and statefulset for now
	ScaleNotAllowedError string = "error: the resource type does not exist or scale on requested resource is not allowed."
	// StatefulSetReplicasInvalidError is the error message in case statefulset missing or specitying an wrong replicas field.
	StatefulSetReplicasInvalidError string = "error: STATEFULSET replicas field is required and must be an positive integer."
	// StatefulSetServiceRequiredError is the error message in case statefulset missing or specitying an wrong replicas field.
	StatefulSetServiceRequiredError string = "error: STATEFULSET service name is required."
	//CreateStatefulSetFromK8sNotSupport is the error message in case user attempt to create statefulset from original k8s yaml.
	CreateStatefulSetFromK8sNotSupport string = "error: STATEFULSET create statefulset from original k8s yaml is not support"
	//ReplaceStatefulSetFromK8sNotSupport is the error message in case user attempt to create statefulset from original k8s yaml.
	ReplaceStatefulSetFromK8sNotSupport string = "error: STATEFULSET replace statefulset from original k8s yaml is not support"
	//AppNameIsRequiredForEditStatefulSet is the error message in case edit service but missing appname.
	AppNameIsRequiredForEditStatefulSet string = "error: STATEFULSET <APP-NAME> is required for editting statefulset."
	//AppNameIsRequiredForScaleStatefulset is the error message in case user scale statefulset without specifying appname.
	AppNameIsRequiredForScaleStatefulset string = "error: STATEFULSET <APP-NAME> is required for scaling statefulset."
)
