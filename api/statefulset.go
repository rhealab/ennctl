package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/ghodss/yaml"
)

// StatefulSetList struct for StatefulSetList
type StatefulSetList struct {
	APIVersion string        `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Items      []StatefulSet `json:"items,omitempty" yaml:"items,omitempty"`
	Kind       string        `json:"kind,omitempty" yaml:"kind,omitempty"`
}

//StatefulSet struct for statefulset
type StatefulSet struct {
	APIVersion string             `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string             `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta         `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       StatefulSetSpec    `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status     *StatefulSetStatus `json:"status,omitempty" yaml:"status,omitempty"`
}

// Valid validation
func (s StatefulSet) Valid() error {
	if s.ObjectMeta.Name == EmptyResource {
		return errors.New(NameIsRequiredError)
	}
	if s.ObjectMeta.Namespace == EmptyResource {
		return errors.New(NamespaceIsRequiredError)
	}
	return s.Spec.valid()
}

//K8sStylize convert  console style to k8s original style
func (s *StatefulSet) K8sStylize() *StatefulSet {
	// set/overwrite X_APP label
	if s.ObjectMeta.App != nil {
		if s.ObjectMeta.Label == nil {
			s.ObjectMeta.Label = map[string]string{
				ConsoleAppLabel: *s.ObjectMeta.App,
			}
		} else {
			s.ObjectMeta.Label[ConsoleAppLabel] = *s.ObjectMeta.App
		}
		if s.Spec.Template.ObjectMeta.Label == nil {
			s.Spec.Template.ObjectMeta.Label = map[string]string{
				ConsoleAppLabel: *s.ObjectMeta.App,
			}
		} else {
			s.Spec.Template.ObjectMeta.Label[ConsoleAppLabel] = *s.ObjectMeta.App
		}
		s.ObjectMeta.App = nil
	}
	// convert storage
	if s.Spec.Template.Spec.Storages != nil {
		tmp := s.Spec.Template.Spec.Volumes
		for _, storage := range *s.Spec.Template.Spec.Storages {
			tmp = append(tmp, storage.toVolume())
		}
		s.Spec.Template.Spec.Volumes = tmp
		s.Spec.Template.Spec.Storages = nil
	}
	return s
}

//Name get name
func (s *StatefulSet) Name() string {
	return s.ObjectMeta.Name
}

//App get app
func (s *StatefulSet) App() string {
	if s.ObjectMeta.Label != nil {
		if val, ok := s.ObjectMeta.Label[ConsoleAppLabel]; ok {
			return val
		}
	}
	return Legacy
}

//Containers get Desired
func (s *StatefulSet) Containers() string {
	ret := Empty
	for _, c := range s.Spec.Template.Spec.Containers {
		ret += c.Name + ","
	}
	if ret != Empty {
		return ret[:len(ret)-1]
	}
	return ret
}

//Desired get Desired
func (s *StatefulSet) Desired() string {
	return fmt.Sprintf("%d", *s.Spec.Replicas)
}

//Current get Desired
func (s *StatefulSet) Current() string {
	return fmt.Sprintf("%d", s.Status.Replicas)
}

//Image get Desired
func (s *StatefulSet) Image() string {
	ret := Empty
	for _, c := range s.Spec.Template.Spec.Containers {
		ret += c.Image + ","
	}
	if ret != Empty {
		return ret[:len(ret)-1]
	}
	return ret
}

//Age get age
func (s *StatefulSet) Age() string {
	dur := time.Since(*s.ObjectMeta.CreationTimestamp)
	if dur.Seconds() <= 60 {
		return fmt.Sprintf("%ds", int(dur.Seconds()))
	} else if dur.Minutes() <= 60 {
		return fmt.Sprintf("%dm", int(dur.Minutes()))
	} else if dur.Hours() <= 24 {
		return fmt.Sprintf("%dh", int(dur.Hours()))
	}
	return fmt.Sprintf("%dd", int(dur.Hours()/24))
}

//ConsoleStylize convert console style deployment to k8s original
func (s *StatefulSet) ConsoleStylize() *StatefulSet {
	if app, ok := s.ObjectMeta.Label[ConsoleAppLabel]; ok {
		s.ObjectMeta.App = &app
		delete(s.ObjectMeta.Label, ConsoleAppLabel)
	}
	var tmp []StorageTemplate
	var v []Volume
	for _, volume := range s.Spec.Template.Spec.Volumes {
		if volume.PersistentVolumeClaim != nil {
			tmp = append(tmp, volume.toStorageTemplate())
		} else {
			v = append(v, volume)
		}
	}
	s.Spec.Template.Spec.Storages = &tmp
	s.Spec.Template.Spec.Volumes = v
	return s
}

// JSON get json string
func (s *StatefulSet) JSON() ([]byte, error) {
	return json.Marshal(s)
}

// YAML get json string
func (s *StatefulSet) YAML() ([]byte, error) {
	return yaml.Marshal(s)
}

// SliceJSON get json string
func (s *StatefulSet) SliceJSON() ([]byte, error) {
	var tmp []StatefulSet
	tmp = append(tmp, *s)
	return json.Marshal(tmp)
}

//StatefulSetSpec struct for ss spec
type StatefulSetSpec struct {
	Replicas             *int32                    `json:"replicas,omitempty" yaml:"replicas,omitempty"`
	Selector             *LabelSelector            `json:"selector,omitempty" yaml:"selector,omitempty"`
	Template             PodTemplateSpec           `json:"template" yaml:"template"`
	VolumeClaimTemplates []PersistentVolumeClaim   `json:"volumeClaimTemplates,omitempty" yaml:"volumeClaimTemplates,omitempty"`
	ServiceName          string                    `json:"serviceName" yaml:"serviceName"`
	PodManagementPolicy  PodManagementPolicyType   `json:"podManagementPolicy,omitempty" yaml:"podManagementPolicy,omitempty"`
	UpdateStrategy       StatefulSetUpdateStrategy `json:"updateStrategy,omitempty" yaml:"updateStrategy,omitempty"`
	RevisionHistoryLimit *int32                    `json:"revisionHistoryLimit,omitempty" yaml:"revisionHistoryLimit,omitempty"`
}

func (ss StatefulSetSpec) valid() error {
	if *ss.Replicas < 0 {
		return errors.New(ReplicasMustBePositiveError)
	}
	if ss.ServiceName == EmptyResource {
		return errors.New(StatefulSetServiceRequiredError)
	}
	return nil
}

// PodManagementPolicyType policy for creating pods in statefulset
type PodManagementPolicyType string

const (
	//OrderedReadyPodManagement OrderedReady
	OrderedReadyPodManagement PodManagementPolicyType = "OrderedReady"
	//ParallelPodManagement Parallel
	ParallelPodManagement PodManagementPolicyType = "Parallel"
)

// StatefulSetUpdateStrategy ss update strategy
type StatefulSetUpdateStrategy struct {
	Type          StatefulSetUpdateStrategyType     `json:"type,omitempty" yaml:"type,omitempty"`
	RollingUpdate *RollingUpdateStatefulSetStrategy `json:"rollingUpdate,omitempty" yaml:"rollingUpdate,omitempty"`
}

// StatefulSetUpdateStrategyType update strategy
type StatefulSetUpdateStrategyType string

const (
	//RollingUpdateStatefulSetStrategyType rolling update
	RollingUpdateStatefulSetStrategyType StatefulSetUpdateStrategyType = "RollingUpdate"
	//OnDeleteStatefulSetStrategyType on delete
	OnDeleteStatefulSetStrategyType StatefulSetUpdateStrategyType = "OnDelete"
)

// RollingUpdateStatefulSetStrategy rolling update strategy
type RollingUpdateStatefulSetStrategy struct {
	Partition *int32 `json:"partition,omitempty" yaml:"partition,omitempty"`
}

// StatefulSetStatus most recent state of a StatefulSet.
type StatefulSetStatus struct {
	ObservedGeneration *int64                 `json:"observedGeneration,omitempty" yaml:"observedGeneration,omitempty"`
	Replicas           int32                  `json:"replicas" yaml:"replicas"`
	ReadyReplicas      int32                  `json:"readyReplicas,omitempty" yaml:"readyReplicas,omitempty"`
	CurrentReplicas    int32                  `json:"currentReplicas,omitempty" yaml:"currentReplicas,omitempty"`
	UpdatedReplicas    int32                  `json:"updatedReplicas,omitempty" yaml:"updatedReplicas,omitempty"`
	CurrentRevision    string                 `json:"currentRevision,omitempty" yaml:"currentRevision,omitempty"`
	UpdateRevision     string                 `json:"updateRevision,omitempty" yaml:"updateRevision,omitempty"`
	CollisionCount     *int32                 `json:"collisionCount,omitempty" yaml:"collisionCount,omitempty"`
	Conditions         []StatefulSetCondition `json:"conditions,omitempty" yaml:"conditions,omitempty"`
}

//StatefulSetConditionType condition type
type StatefulSetConditionType string

// StatefulSetCondition statefulset condition
type StatefulSetCondition struct {
	Type               StatefulSetConditionType `json:"type" yaml:"type"`
	Status             ConditionStatus          `json:"status" yaml:"status"`
	LastTransitionTime string                   `json:"lastTransitionTime,omitempty" yaml:"lastTransitionTime,omitempty"`
	Reason             string                   `json:"reason,omitempty" yaml:"reason,omitempty"`
	Message            string                   `json:"message,omitempty" yaml:"message,omitempty"`
}
