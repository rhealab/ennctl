package api

import (
	"fmt"
	"strconv"
	"time"
)

// PodList struct for PodList
type PodList struct {
	APIVersion string `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Items      []Pod  `json:"items,omitempty" yaml:"items,omitempty"`
	Kind       string `json:"kind,omitempty" yaml:"kind,omitempty"`
}

// Pod struct for pod
type Pod struct {
	APIVersion string     `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string     `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       PodSpec    `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status     PodStatus  `json:"status,omitempty" protobuf:"bytes,3,opt,name=status"`
}

//Name get name
func (p *Pod) Name() string {
	return p.ObjectMeta.Name
}

//ContainerSum get sum of container
func (p *Pod) ContainerSum() int {
	return len(p.Spec.Containers)
}

//App get app
func (p *Pod) App() string {
	if p.ObjectMeta.Label != nil {
		if val, ok := p.ObjectMeta.Label[ConsoleAppLabel]; ok {
			return val
		}
	}
	return Legacy
}

//DefaultContainerName get default container
func (p *Pod) DefaultContainerName() string {
	return p.Spec.Containers[0].Name
}

//PodIP get name
func (p *Pod) PodIP() string {
	return p.Status.PodIP
}

//Node get name
func (p *Pod) Node() string {
	return p.Spec.NodeName
}

//Ready get ready
func (p *Pod) Ready() string {
	cnt := 0
	for _, cs := range p.Status.ContainerStatuses {
		if cs.Ready {
			cnt++
		}
	}
	return fmt.Sprintf("%d/%d", cnt, len(p.Status.ContainerStatuses))
}

//PodStatus get ready
func (p *Pod) PodStatus() string {
	status := string(p.Status.Phase)
	if p.Status.Reason != Empty {
		return p.Status.Reason
	}
	initializing := false
	for idx, ics := range p.Status.InitContainerStatuses {
		terminated := ics.State.Terminated
		if terminated != nil {
			if terminated.ExitCode == 0 {
				continue
			} else {
				if terminated.Reason != Empty {
					status = "Init:" + terminated.Reason
				} else {
					if terminated.Signal == 0 {
						status = "Init:ExitCode:" + strconv.Itoa(int(terminated.ExitCode))
					} else {
						status = "Init:Signal:" + strconv.Itoa(int(terminated.Signal))
					}
				}
				initializing = true
				continue
			}
		}
		waiting := ics.State.Waiting
		if waiting != nil && waiting.Reason != Empty && waiting.Reason != "PodInitializing" {
			status = "Init:" + waiting.Reason
			initializing = true
			continue
		}
		status = fmt.Sprintf("Init:%d/%d", idx, len(p.Status.InitContainerStatuses))
	}
	if !initializing {
		for _, cs := range p.Status.ContainerStatuses {
			waiting := cs.State.Waiting
			if waiting != nil && waiting.Reason != Empty {
				status = waiting.Reason
				continue
			}
			terminated := cs.State.Terminated
			if terminated != nil {
				if terminated.Reason != Empty {
					status = terminated.Reason
				} else {
					if terminated.Signal != 0 {
						status = fmt.Sprintf("Signal:%d", terminated.Signal)
					} else {
						status = fmt.Sprintf("ExitCode:%d", terminated.ExitCode)
					}
				}
				continue
			}
		}
	}
	if p.ObjectMeta.DeletionTimestamp != nil {
		if p.Status.Reason == "NodeLost" {
			status = "Unknown"
		} else {
			status = "Terminating"
		}
	}
	return string(status)
}

//RestartCount get ready
func (p *Pod) RestartCount() string {
	var cnt int32
	cnt = 0
	for _, c := range p.Status.ContainerStatuses {
		cnt = cnt + c.RestartCount
	}
	return strconv.Itoa(int(cnt))
}

//Age get age
func (p *Pod) Age() string {
	d := time.Since(*p.ObjectMeta.CreationTimestamp)
	if d.Seconds() <= 60 {
		return fmt.Sprintf("%ds", int(d.Seconds()))
	} else if d.Minutes() <= 60 {
		return fmt.Sprintf("%dm", int(d.Minutes()))
	} else if d.Hours() <= 24 {
		return fmt.Sprintf("%dh", int(d.Hours()))
	}
	return fmt.Sprintf("%dd", int(d.Hours()/24))
}

// PodTemplateSpec describes the data a pod should have when created from a template
type PodTemplateSpec struct {
	ObjectMeta ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       PodSpec    `json:"spec,omitempty" yaml:"spec,omitempty"`
}

// PodSpec is a description of a pod.
type PodSpec struct {
	Volumes                       []Volume               `json:"volumes,omitempty" yaml:"volumes,omitempty"`
	Storages                      *[]StorageTemplate     `json:"storage,omitempty" yaml:"storage,omitempty"`
	InitContainers                []Container            `json:"initContainers,omitempty" yaml:"initContainers,omitempty"`
	Containers                    []Container            `json:"containers" yaml:"containers"`
	RestartPolicy                 RestartPolicy          `json:"restartPolicy,omitempty" yaml:"restartPolicy,omitempty"`
	TerminationGracePeriodSeconds *int64                 `json:"terminationGracePeriodSeconds,omitempty" yaml:"terminationGracePeriodSeconds,omitempty"`
	ActiveDeadlineSeconds         *int64                 `json:"activeDeadlineSeconds,omitempty" yaml:"activeDeadlineSeconds,omitempty"`
	DNSPolicy                     DNSPolicy              `json:"dnsPolicy,omitempty" yaml:"dnsPolicy,omitempty"`
	NodeSelector                  map[string]string      `json:"nodeSelector,omitempty" yaml:"nodeSelector,omitempty"`
	ServiceAccountName            string                 `json:"serviceAccountName,omitempty" yaml:"serviceAccountName,omitempty"`
	DeprecatedServiceAccount      string                 `json:"serviceAccount,omitempty" yaml:"serviceAccount,omitempty"`
	AutomountServiceAccountToken  *bool                  `json:"automountServiceAccountToken,omitempty" yaml:"automountServiceAccountToken,omitempty"`
	NodeName                      string                 `json:"nodeName,omitempty" yaml:"nodeName,omitempty"`
	HostNetwork                   bool                   `json:"hostNetwork,omitempty" yaml:"hostNetwork,omitempty"`
	HostPID                       bool                   `json:"hostPID,omitempty" yaml:"hostPID,omitempty"`
	HostIPC                       bool                   `json:"hostIPC,omitempty" yaml:"hostIPC,omitempty"`
	SecurityContext               *PodSecurityContext    `json:"securityContext,omitempty" yaml:"securityContext,omitempty"`
	ImagePullSecrets              []LocalObjectReference `json:"imagePullSecrets,omitempty" yaml:"imagePullSecrets,omitempty"`
	Hostname                      string                 `json:"hostname,omitempty" yaml:"hostname,omitempty"`
	Subdomain                     string                 `json:"subdomain,omitempty" yaml:"subdomain,omitempty"`
	Affinity                      *Affinity              `json:"affinity,omitempty" yaml:"affinity,omitempty"`
	SchedulerName                 string                 `json:"schedulerName,omitempty" yaml:"schedulerName,omitempty"`
	Tolerations                   []Toleration           `json:"tolerations,omitempty" yaml:"tolerations,omitempty"`
	HostAliases                   []HostAlias            `json:"hostAliases,omitempty" yaml:"hostAliases,omitempty"`
	PriorityClassName             string                 `json:"priorityClassName,omitempty" yaml:"priorityClassName,omitempty"`
	Priority                      *int32                 `json:"priority,omitempty" yaml:"priority,omitempty"`
	DNSConfig                     *PodDNSConfig          `json:"dnsConfig,omitempty" protobuf:"bytes,26,opt,name=dnsConfig"`
}

//PodDNSConfig dns configuration
type PodDNSConfig struct {
	Nameservers []string             `json:"nameservers,omitempty" yaml:"nameservers,omitempty"`
	Searches    []string             `json:"searches,omitempty" yaml:"searches,omitempty"`
	Options     []PodDNSConfigOption `json:"options,omitempty" protobuf:"bytes,3,rep,name=options"`
}

// PodDNSConfigOption dns option
type PodDNSConfigOption struct {
	Name  string  `json:"name,omitempty" yaml:"name,omitempty"`
	Value *string `json:"value,omitempty" yaml:"value,omitempty"`
}

//HostAlias HostAlias struct
type HostAlias struct {
	IP        string   `json:"ip,omitempty" yaml:"ip,omitempty"`
	Hostnames []string `json:"hostnames,omitempty" yaml:"hostnames,omitempty"`
}

//Container A single application container that you want to run within a pod.
type Container struct {
	Name                     string                   `json:"name" yaml:"name"`
	Image                    string                   `json:"image,omitempty" yaml:"image,omitempty"`
	Command                  []string                 `json:"command,omitempty" yaml:"command,omitempty"`
	Args                     []string                 `json:"args,omitempty" yaml:"args,omitempty"`
	WorkingDir               string                   `json:"workingDir,omitempty" yaml:"workingDir,omitempty"`
	Ports                    []ContainerPort          `json:"ports,omitempty" yaml:"ports,omitempty"`
	EnvFrom                  []EnvFromSource          `json:"envFrom,omitempty" yaml:"envFrom,omitempty"`
	Env                      []EnvVar                 `json:"env,omitempty" yaml:"env,omitempty"`
	Resources                ResourceRequirements     `json:"resources,omitempty" yaml:"resources,omitempty"`
	VolumeMounts             []VolumeMount            `json:"volumeMounts,omitempty" yaml:"volumeMounts,omitempty"`
	VolumeDevices            []VolumeDevice           `json:"volumeDevices,omitempty" yaml:"volumeDevices,omitempty"`
	LivenessProbe            *Probe                   `json:"livenessProbe,omitempty" yaml:"livenessProbe,omitempty"`
	ReadinessProbe           *Probe                   `json:"readinessProbe,omitempty" yaml:"readinessProbe,omitempty"`
	Lifecycle                *Lifecycle               `json:"lifecycle,omitempty" yaml:"lifecycle,omitempty"`
	TerminationMessagePath   string                   `json:"terminationMessagePath,omitempty" yaml:"terminationMessagePath,omitempty"`
	TerminationMessagePolicy TerminationMessagePolicy `json:"terminationMessagePolicy,omitempty" yaml:"terminationMessagePolicy,omitempty"`
	ImagePullPolicy          PullPolicy               `json:"imagePullPolicy,omitempty" yaml:"imagePullPolicy,omitempty"`
	SecurityContext          *SecurityContext         `json:"securityContext,omitempty" yaml:"securityContext,omitempty"`
	Stdin                    bool                     `json:"stdin,omitempty" yaml:"stdin,omitempty" `
	StdinOnce                bool                     `json:"stdinOnce,omitempty" yaml:"stdinOnce,omitempty"`
	TTY                      bool                     `json:"tty,omitempty" yaml:"tty,omitempty"`
}

//ContainerPort is container port
type ContainerPort struct {
	Name          string   `json:"name,omitempty" yaml:"name,omitempty"`
	HostPort      int32    `json:"hostPort,omitempty" yaml:"hostPort,omitempty"`
	ContainerPort int32    `json:"containerPort" yaml:"containerPort"`
	Protocol      Protocol `json:"protocol,omitempty" protobuf:"bytes,4,opt,name=protocol,casttype=Protocol"`
	HostIP        string   `json:"hostIP,omitempty" yaml:"hostIP,omitempty"`
}

//EnvFromSource is EnvFromSource.
type EnvFromSource struct {
	Prefix       string              `json:"prefix,omitempty" yaml:"prefix,omitempty"`
	ConfigMapRef *ConfigMapEnvSource `json:"configMapRef,omitempty" yaml:"configMapRef,omitempty"`
	SecretRef    *SecretEnvSource    `json:"secretRef,omitempty" yaml:"secretRef,omitempty"`
}

// Protocol defines network protocols supported for things like container ports.
type Protocol string

const (
	// ProtocolTCP TCP protocol
	ProtocolTCP Protocol = "TCP"
	// ProtocolUDP UDP protocol
	ProtocolUDP Protocol = "UDP"
)

// ConfigMapEnvSource is ConfigMapEnvSource
type ConfigMapEnvSource struct {
	LocalObjectReference `json:",inline" yaml:",inline"`
	Optional             *bool `json:"optional,omitempty" yaml:"optional,omitempty"`
}

// SecretEnvSource is SecretEnvSource
type SecretEnvSource struct {
	LocalObjectReference `json:",inline" yaml:",inline"`
	Optional             *bool `json:"optional,omitempty" yaml:"optional,omitempty"`
}

// LocalObjectReference is LocalObjectReference
type LocalObjectReference struct {
	Name string `json:"name,omitempty" yaml:"name,omitempty"`
}

// EnvVar represents an environment variable present in a Container.
type EnvVar struct {
	Name      string        `json:"name" yaml:"name"`
	Value     string        `json:"value,omitempty" yaml:"value,omitempty"`
	ValueFrom *EnvVarSource `json:"valueFrom,omitempty" yaml:"valueFrom,omitempty"`
}

// EnvVarSource is EnvVarSource
type EnvVarSource struct {
	FieldRef         *ObjectFieldSelector   `json:"fieldRef,omitempty" yaml:"fieldRef,omitempty"`
	ResourceFieldRef *ResourceFieldSelector `json:"resourceFieldRef,omitempty" protobuf:"bytes,2,opt,name=resourceFieldRef"`
	ConfigMapKeyRef  *ConfigMapKeySelector  `json:"configMapKeyRef,omitempty" protobuf:"bytes,3,opt,name=configMapKeyRef"`
	SecretKeyRef     *SecretKeySelector     `json:"secretKeyRef,omitempty" protobuf:"bytes,4,opt,name=secretKeyRef"`
}

// ObjectFieldSelector is ObjectFieldSelector
type ObjectFieldSelector struct {
	APIVersion string `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	FieldPath  string `json:"fieldPath" yaml:"fieldPath"`
}

// ResourceFieldSelector is ResourceFieldSelector
type ResourceFieldSelector struct {
	ContainerName string `json:"containerName,omitempty" yaml:"containerName,omitempty"`
	Resource      string `json:"resource" yaml:"resource"`
	Divisor       string `json:"divisor,omitempty" yaml:"divisor,omitempty"`
}

//ConfigMapKeySelector is ConfigMapKeySelector
type ConfigMapKeySelector struct {
	LocalObjectReference `json:",inline" yaml:",inline"`
	Key                  string `json:"key" yaml:"key"`
	Optional             *bool  `json:"optional,omitempty" yaml:"optional,omitempty"`
}

//SecretKeySelector is SecretKeySelector
type SecretKeySelector struct {
	LocalObjectReference `json:",inline" yaml:",inline"`
	Key                  string `json:"key" yaml:"key"`
	Optional             *bool  `json:"optional,omitempty" yaml:"optional,omitempty"`
}

// Probe is the health check
type Probe struct {
	Handler             `json:",inline" yaml:",inline"`
	InitialDelaySeconds int32 `json:"initialDelaySeconds,omitempty" yaml:"initialDelaySeconds,omitempty"`
	TimeoutSeconds      int32 `json:"timeoutSeconds,omitempty" yaml:"timeoutSeconds,omitempty"`
	PeriodSeconds       int32 `json:"periodSeconds,omitempty" yaml:"periodSeconds,omitempty"`
	SuccessThreshold    int32 `json:"successThreshold,omitempty" yaml:"successThreshold,omitempty"`
	FailureThreshold    int32 `json:"failureThreshold,omitempty" yaml:"failureThreshold,omitempty"`
}

// Handler defines the action
type Handler struct {
	Exec      *ExecAction      `json:"exec,omitempty" yaml:"exec,omitempty"`
	HTTPGet   *HTTPGetAction   `json:"httpGet,omitempty" yaml:"httpGet,omitempty"`
	TCPSocket *TCPSocketAction `json:"tcpSocket,omitempty" yaml:"tcpSocket,omitempty"`
}

// ExecAction command based action
type ExecAction struct {
	Command []string `json:"command,omitempty" yaml:"command,omitempty"`
}

// HTTPHeader describes a custom header to be used in HTTP probes
type HTTPHeader struct {
	// The header field name
	Name string `json:"name" yaml:"name"`
	// The header field value
	Value string `json:"value" yaml:"value"`
}

// HTTPGetAction http based action
type HTTPGetAction struct {
	Path        string       `json:"path,omitempty" yaml:"path,omitempty"`
	Port        int32        `json:"port" yaml:"port"`
	Host        string       `json:"host,omitempty" yaml:"host,omitempty"`
	Scheme      URIScheme    `json:"scheme,omitempty" yaml:"scheme,omitempty"`
	HTTPHeaders []HTTPHeader `json:"httpHeaders,omitempty" yaml:"httpHeaders,omitempty"`
}

// URIScheme http or https
type URIScheme string

const (
	// URISchemeHTTP url like http://
	URISchemeHTTP URIScheme = "HTTP"
	// URISchemeHTTPS url like https://
	URISchemeHTTPS URIScheme = "HTTPS"
)

// TCPSocketAction socket based action
type TCPSocketAction struct {
	Port int32  `json:"port" yaml:"port"`
	Host string `json:"host,omitempty" yaml:"host,omitempty"`
}

// Lifecycle are actions takes during lifecycle
type Lifecycle struct {
	PostStart *Handler `json:"postStart,omitempty" yaml:"postStart,omitempty"`
	PreStop   *Handler `json:"preStop,omitempty" yaml:"preStop,omitempty"`
}

// TerminationMessagePolicy how to store termination message
type TerminationMessagePolicy string

const (
	// TerminationMessageReadFile save to file
	TerminationMessageReadFile TerminationMessagePolicy = "File"
	// TerminationMessageFallbackToLogsOnError save to log
	TerminationMessageFallbackToLogsOnError TerminationMessagePolicy = "FallbackToLogsOnError"
)

// PullPolicy image pull policy for image
type PullPolicy string

const (
	// PullAlways allways pull
	PullAlways PullPolicy = "Always"
	// PullNever never pull image, use local only
	PullNever PullPolicy = "Never"
	// PullIfNotPresent pull image if image is not present
	PullIfNotPresent PullPolicy = "IfNotPresent"
)

// PodSecurityContext pod-level security attributes and common container settings.
type PodSecurityContext struct {
	SELinuxOptions     *SELinuxOptions `json:"seLinuxOptions,omitempty" yaml:"seLinuxOptions,omitempty"`
	RunAsUser          *int64          `json:"runAsUser,omitempty" yaml:"runAsUser,omitempty"`
	RunAsNonRoot       *bool           `json:"runAsNonRoot,omitempty" yaml:"runAsNonRoot,omitempty"`
	SupplementalGroups []int64         `json:"supplementalGroups,omitempty" yaml:"supplementalGroups,omitempty"`
	FSGroup            *int64          `json:"fsGroup,omitempty" yaml:"fsGroup,omitempty"`
}

// SecurityContext security configuration.
type SecurityContext struct {
	Capabilities             *Capabilities   `json:"capabilities,omitempty" protobuf:"bytes,1,opt,name=capabilities"`
	Privileged               *bool           `json:"privileged,omitempty" protobuf:"varint,2,opt,name=privileged"`
	SELinuxOptions           *SELinuxOptions `json:"seLinuxOptions,omitempty" protobuf:"bytes,3,opt,name=seLinuxOptions"`
	RunAsUser                *int64          `json:"runAsUser,omitempty" protobuf:"varint,4,opt,name=runAsUser"`
	RunAsNonRoot             *bool           `json:"runAsNonRoot,omitempty" protobuf:"varint,5,opt,name=runAsNonRoot"`
	ReadOnlyRootFilesystem   *bool           `json:"readOnlyRootFilesystem,omitempty" protobuf:"varint,6,opt,name=readOnlyRootFilesystem"`
	AllowPrivilegeEscalation *bool           `json:"allowPrivilegeEscalation,omitempty" protobuf:"varint,7,opt,name=allowPrivilegeEscalation"`
}

// SELinuxOptions labels applied to container
type SELinuxOptions struct {
	User  string `json:"user,omitempty" yaml:"user,omitempty"`
	Role  string `json:"role,omitempty" yaml:"role,omitempty"`
	Type  string `json:"type,omitempty" yaml:"type,omitempty"`
	Level string `json:"level,omitempty" yaml:"level,omitempty"`
}

// Capability is POSIX capabilities type
type Capability string

//Capabilities are Adds and removes POSIX capabilities from running containers.
type Capabilities struct {
	Add  []Capability `json:"add,omitempty" yaml:"add,omitempty"`
	Drop []Capability `json:"drop,omitempty" yaml:"drop,omitempty"`
}

//RestartPolicy how to restart pod
type RestartPolicy string

const (
	//RestartPolicyAlways always restart
	RestartPolicyAlways RestartPolicy = "Always"
	//RestartPolicyOnFailure on failure restart
	RestartPolicyOnFailure RestartPolicy = "OnFailure"
	//RestartPolicyNever never restart
	RestartPolicyNever RestartPolicy = "Never"
)

// DNSPolicy how to configure a pod's DNS.
type DNSPolicy string

const (
	// DNSClusterFirstWithHostNet uses cluster DNS first.
	DNSClusterFirstWithHostNet DNSPolicy = "ClusterFirstWithHostNet"

	// DNSClusterFirst uses cluster DNS first unless hostNetwork is true.
	DNSClusterFirst DNSPolicy = "ClusterFirst"

	// DNSDefault uses the default.
	DNSDefault DNSPolicy = "Default"

	// DNSNone uses empty DNS settings.
	DNSNone DNSPolicy = "None"
)

// Affinity is a group of affinities
type Affinity struct {
	NodeAffinity    *NodeAffinity    `json:"nodeAffinity,omitempty" yaml:"nodeAffinity,omitempty"`
	PodAffinity     *PodAffinity     `json:"podAffinity,omitempty" yaml:"podAffinity,omitempty"`
	PodAntiAffinity *PodAntiAffinity `json:"podAntiAffinity,omitempty" yaml:"podAntiAffinity,omitempty"`
}

// PodAffinity pod affinities.
type PodAffinity struct {
	RequiredDuringSchedulingIgnoredDuringExecution  []PodAffinityTerm         `json:"requiredDuringSchedulingIgnoredDuringExecution,omitempty" yaml:"requiredDuringSchedulingIgnoredDuringExecution,omitempty"`
	PreferredDuringSchedulingIgnoredDuringExecution []WeightedPodAffinityTerm `json:"preferredDuringSchedulingIgnoredDuringExecution,omitempty" yaml:"preferredDuringSchedulingIgnoredDuringExecution,omitempty"`
}

// PodAntiAffinity pod anti affinities.
type PodAntiAffinity struct {
	RequiredDuringSchedulingIgnoredDuringExecution  []PodAffinityTerm         `json:"requiredDuringSchedulingIgnoredDuringExecution,omitempty" yaml:"requiredDuringSchedulingIgnoredDuringExecution,omitempty"`
	PreferredDuringSchedulingIgnoredDuringExecution []WeightedPodAffinityTerm `json:"preferredDuringSchedulingIgnoredDuringExecution,omitempty" yaml:"preferredDuringSchedulingIgnoredDuringExecution,omitempty"`
}

// PodAffinityTerm is term
type PodAffinityTerm struct {
	LabelSelector LabelSelector `json:"labelSelector,omitempty" yaml:"labelSelector,omitempty"`
	Namespaces    []string      `json:"namespaces,omitempty" yaml:"namespaces,omitempty"`
	TopologyKey   string        `json:"topologyKey" yaml:"topologyKey"`
}

// NodeAffinity  group of node affinities
type NodeAffinity struct {
	RequiredDuringSchedulingIgnoredDuringExecution  *NodeSelector             `json:"requiredDuringSchedulingIgnoredDuringExecution,omitempty" yaml:"requiredDuringSchedulingIgnoredDuringExecution,omitempty"`
	PreferredDuringSchedulingIgnoredDuringExecution []PreferredSchedulingTerm `json:"preferredDuringSchedulingIgnoredDuringExecution,omitempty" yaml:"preferredDuringSchedulingIgnoredDuringExecution,omitempty"`
}

// NodeSelector struct for selector term
type NodeSelector struct {
	NodeSelectorTerms []NodeSelectorTerm `json:"nodeSelectorTerms" yaml:"nodeSelectorTerms"`
}

// NodeSelectorTerm nst
type NodeSelectorTerm struct {
	MatchExpressions []NodeSelectorRequirement `json:"matchExpressions" yaml:"matchExpressions"`
}

// NodeSelectorRequirement nst label
type NodeSelectorRequirement struct {
	Key      string               `json:"key" yaml:"key"`
	Operator NodeSelectorOperator `json:"operator" yaml:"operator"`
	Values   []string             `json:"values,omitempty" yaml:"values,omitempty"`
}

// NodeSelectorOperator node selector operator
type NodeSelectorOperator string

const (
	//NodeSelectorOpIn in
	NodeSelectorOpIn NodeSelectorOperator = "In"
	//NodeSelectorOpNotIn not in
	NodeSelectorOpNotIn NodeSelectorOperator = "NotIn"
	//NodeSelectorOpExists exists
	NodeSelectorOpExists NodeSelectorOperator = "Exists"
	//NodeSelectorOpDoesNotExist not exist
	NodeSelectorOpDoesNotExist NodeSelectorOperator = "DoesNotExist"
	//NodeSelectorOpGt greater than
	NodeSelectorOpGt NodeSelectorOperator = "Gt"
	//NodeSelectorOpLt less than
	NodeSelectorOpLt NodeSelectorOperator = "Lt"
)

//PreferredSchedulingTerm pst
type PreferredSchedulingTerm struct {
	Weight     int32            `json:"weight" yaml:"weight"`
	Preference NodeSelectorTerm `json:"preference" yaml:"preference" `
}

//WeightedPodAffinityTerm wpat
type WeightedPodAffinityTerm struct {
	Weight int32 `json:"weight" yaml:"weight"`
	// Required. A pod affinity term, associated with the corresponding weight.
	PodAffinityTerm PodAffinityTerm `json:"podAffinityTerm" yaml:"podAffinityTerm"`
}

//Toleration tolerations
type Toleration struct {
	Key               string             `json:"key,omitempty" yaml:"key,omitempty"`
	Operator          TolerationOperator `json:"operator,omitempty" yaml:"operator,omitempty" `
	Value             string             `json:"value,omitempty" yaml:"value,omitempty"`
	Effect            TaintEffect        `json:"effect,omitempty" yaml:"effect,omitempty"`
	TolerationSeconds *int64             `json:"tolerationSeconds,omitempty" yaml:"tolerationSeconds,omitempty"`
}

//TolerationOperator t operator
type TolerationOperator string

const (
	//TolerationOpExists exists
	TolerationOpExists TolerationOperator = "Exists"
	//TolerationOpEqual equals
	TolerationOpEqual TolerationOperator = "Equal"
)

//TaintEffect taint effects
type TaintEffect string

const (
	//TaintEffectNoSchedule no schedule
	TaintEffectNoSchedule TaintEffect = "NoSchedule"
	//TaintEffectPreferNoSchedule prefer no schedule
	TaintEffectPreferNoSchedule TaintEffect = "PreferNoSchedule"
	//TaintEffectNoExecute NoExecute
	TaintEffectNoExecute TaintEffect = "NoExecute"
)

// PodStatus pod status
type PodStatus struct {
	Phase                 PodPhase          `json:"phase,omitempty" yaml:"phase,omitempty"`
	Conditions            []PodCondition    `json:"conditions,omitempty" yaml:"conditions,omitempty"`
	Message               string            `json:"message,omitempty" yaml:"message,omitempty"`
	Reason                string            `json:"reason,omitempty" yaml:"reason,omitempty"`
	HostIP                string            `json:"hostIP,omitempty" yaml:"hostIP,omitempty"`
	PodIP                 string            `json:"podIP,omitempty" yaml:"podIP,omitempty"`
	StartTime             string            `json:"startTime,omitempty" yaml:"startTime,omitempty"`
	InitContainerStatuses []ContainerStatus `json:"initContainerStatuses,omitempty" yaml:"initContainerStatuses,omitempty"`
	ContainerStatuses     []ContainerStatus `json:"containerStatuses,omitempty" yaml:"containerStatuses,omitempty"`
	QOSClass              PodQOSClass       `json:"qosClass,omitempty" yaml:"qosClass,omitempty"`
}

// PodPhase string for current pod phase
type PodPhase string

const (
	// PodPending means the pod has been accepted by the system, but container is not ready
	PodPending PodPhase = "Pending"
	// PodRunning means the pod has been bound to a node and all of the containers have been started.
	PodRunning PodPhase = "Running"
	// PodSucceeded means that all containers in the pod have voluntarily terminated
	PodSucceeded PodPhase = "Succeeded"
	// PodFailed means that all containers in the pod have terminated with at lease one failure
	PodFailed PodPhase = "Failed"
	// PodUnknown means that pod status can not be obtained
	PodUnknown PodPhase = "Unknown"
)

// PodCondition contains details for the current condition of this pod.
type PodCondition struct {
	Type               PodConditionType `json:"type" yaml:"type"`
	Status             ConditionStatus  `json:"status" yaml:"status"`
	LastProbeTime      string           `json:"lastProbeTime,omitempty" yaml:"lastProbeTime,omitempty"`
	LastTransitionTime string           `json:"lastTransitionTime,omitempty" yaml:"lastTransitionTime,omitempty"`
	Reason             string           `json:"reason,omitempty" yaml:"reason,omitempty"`
	Message            string           `json:"message,omitempty" yaml:"message,omitempty"`
}

// PodConditionType string
type PodConditionType string

// These are valid conditions of pod.
const (
	PodScheduled           PodConditionType = "PodScheduled"
	PodReady               PodConditionType = "Ready"
	PodInitialized         PodConditionType = "Initialized"
	PodReasonUnschedulable PodConditionType = "Unschedulable"
)

// ContainerStatus details of current state
type ContainerStatus struct {
	Name  string         `json:"name" yaml:"name"`
	State ContainerState `json:"state,omitempty" yaml:"state,omitempty"`
	// Details about the container's last termination condition.
	// +optional
	LastTerminationState ContainerState `json:"lastState,omitempty" protobuf:"bytes,3,opt,name=lastState"`
	// Specifies whether the container has passed its readiness probe.
	Ready bool `json:"ready" protobuf:"varint,4,opt,name=ready"`
	// The number of times the container has been restarted, currently based on
	// the number of dead containers that have not yet been removed.
	// Note that this is calculated from dead containers. But those containers are subject to
	// garbage collection. This value will get capped at 5 by GC.
	RestartCount int32 `json:"restartCount" protobuf:"varint,5,opt,name=restartCount"`
	// The image the container is running.
	// More info: https://kubernetes.io/docs/concepts/containers/images
	// TODO(dchen1107): Which image the container is running with?
	Image string `json:"image" protobuf:"bytes,6,opt,name=image"`
	// ImageID of the container's image.
	ImageID string `json:"imageID" protobuf:"bytes,7,opt,name=imageID"`
	// Container's ID in the format 'docker://<container_id>'.
	// +optional
	ContainerID string `json:"containerID,omitempty" protobuf:"bytes,8,opt,name=containerID"`
}

// ContainerState possible state
type ContainerState struct {
	Waiting    *ContainerStateWaiting    `json:"waiting,omitempty" yaml:"waiting,omitempty"`
	Running    *ContainerStateRunning    `json:"running,omitempty" yaml:"running,omitempty"`
	Terminated *ContainerStateTerminated `json:"terminated,omitempty" yaml:"terminated,omitempty"`
}

// ContainerStateWaiting waiting
type ContainerStateWaiting struct {
	Reason  string `json:"reason,omitempty" yaml:"reason,omitempty"`
	Message string `json:"message,omitempty" yaml:"message,omitempty" `
}

// ContainerStateRunning running
type ContainerStateRunning struct {
	StartedAt string `json:"startedAt,omitempty" yaml:"startedAt,omitempty"`
}

// ContainerStateTerminated terminated
type ContainerStateTerminated struct {
	ExitCode    int32  `json:"exitCode" yaml:"exitCode"`
	Signal      int32  `json:"signal,omitempty" yaml:"signal,omitempty"`
	Reason      string `json:"reason,omitempty" yaml:"reason,omitempty"`
	Message     string `json:"message,omitempty" protobuf:"bytes,4,opt,name=message"`
	StartedAt   string `json:"startedAt,omitempty" yaml:"startedAt,omitempty"`
	FinishedAt  string `json:"finishedAt,omitempty" yaml:"finishedAt,omitempty"`
	ContainerID string `json:"containerID,omitempty" yaml:"containerID,omitempty"`
}

// PodQOSClass qos
type PodQOSClass string

const (
	// PodQOSGuaranteed Guaranteed
	PodQOSGuaranteed PodQOSClass = "Guaranteed"
	// PodQOSBurstable Burstable
	PodQOSBurstable PodQOSClass = "Burstable"
	// PodQOSBestEffort BestEffort
	PodQOSBestEffort PodQOSClass = "BestEffort"
)
