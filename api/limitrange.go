package api

import (
	"encoding/json"
)

// GlobalLimitRange is the struct for console limit range
type GlobalLimitRange struct {
	APIVersion string         `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string         `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta     `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       LimitRangeSpec `json:"spec,omitempty" yaml:"spec,omitempty"`
}

//LimitRangeCreateRequest convert consleDeployment to LimitRangeCreateRequest
func (l *GlobalLimitRange) LimitRangeCreateRequest() *LimitRangeCreateRequest {
	return &l.Spec.LimitRange
}

// LimitRangeSpec spec of limit range
type LimitRangeSpec struct {
	LimitRange LimitRangeCreateRequest `yaml:"limitRange,omitempty" json:"limitRange,omitempty"`
}

//========================= console limit range request =========================

// LimitRangeCreateRequest is used to construct post body in app creation API
type LimitRangeCreateRequest struct {
	MinCPU               float32 `yaml:"minCpu,omitempty" json:"minCpu,omitempty"`
	MinMemory            int64   `yaml:"minMemory,omitempty" json:"minMemory,omitempty"`
	DefaultRequestCPU    float32 `yaml:"defaultRequestCpu,omitempty" json:"defaultRequestCpu,omitempty"`
	DefaultRequestMemory int64   `yaml:"defaultRequestMemory,omitempty" json:"defaultRequestMemory,omitempty"`
	DefaultLimitCPU      float32 `yaml:"defaultLimitCpu,omitempty" json:"defaultLimitCpu,omitempty"`
	DefaultLimitMemory   int64   `yaml:"defaultLimitMemory,omitempty" json:"defaultLimitMemory,omitempty"`
}

// JSON to json bytes
func (l LimitRangeCreateRequest) JSON() ([]byte, error) {
	return json.Marshal(l)
}
