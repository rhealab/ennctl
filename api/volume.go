package api

// VolumeMount is mounting point of container
type VolumeMount struct {
	Name             string                `json:"name" yaml:"name"`
	ReadOnly         bool                  `json:"readOnly,omitempty" yaml:"readOnly,omitempty"`
	MountPath        string                `json:"mountPath" yaml:"mountPath"`
	SubPath          string                `json:"subPath,omitempty" yaml:"subPath,omitempty"`
	MountPropagation *MountPropagationMode `json:"mountPropagation,omitempty" yaml:"mountPropagation,omitempty"`
}

// MountPropagationMode is mount propagation mode
type MountPropagationMode string

const (
	//MountPropagationHostToContainer HostToContainer
	MountPropagationHostToContainer MountPropagationMode = "HostToContainer"
	//MountPropagationBidirectional Bidirectional
	MountPropagationBidirectional MountPropagationMode = "Bidirectional"
)

// VolumeDevice is map of raw device
type VolumeDevice struct {
	Name       string `json:"name" yaml:"name"`
	DevicePath string `json:"devicePath" yaml:"devicePath"`
}

// Volume a named volume
type Volume struct {
	Name                  string                             `json:"name" yaml:"name"`
	PersistentVolumeClaim *PersistentVolumeClaimVolumeSource `json:"persistentVolumeClaim,omitempty" yaml:"persistentVolumeClaim,omitempty"`
	HostPath              *HostPathVolumeSource              `json:"hostPath,omitempty" yaml:"hostPath,omitempty"`
	Secret                *SecretVolumeSource                `json:"secret,omitempty" yaml:"secret,omitempty" `
	ConfigMap             *ConfigMapVolumeSource             `json:"configMap,omitempty" yaml:"configMap,omitempty"`
	NFS                   *NFSVolumeSource                   `json:"nfs,omitempty" yaml:"nfs,omitempty"`
	RBD                   *RBDVolumeSource                   `json:"rbd,omitempty" yaml:"rbd,omitempty"`
	CephFS                *CephFSVolumeSource                `json:"cephfs,omitempty" yaml:"cephfs,omitempty"`
	EmptyDir              *EmptyDirVolumeSource              `json:"emptyDir,omitempty" yaml:"emptyDir,omitempty"`
}

//toStorageTemplate convert to Storage
func (v *Volume) toStorageTemplate() StorageTemplate {
	return StorageTemplate{
		Name:     v.Name,
		ReadOnly: v.PersistentVolumeClaim.ReadOnly,
	}
}

//PersistentVolumeClaimVolumeSource pvc
type PersistentVolumeClaimVolumeSource struct {
	ClaimName string `json:"claimName" yaml:"claimName"`
	ReadOnly  *bool  `json:"readOnly,omitempty" yaml:"readOnly,omitempty"`
}

//StorageTemplate console storage
type StorageTemplate struct {
	Name     string `json:"name" yaml:"name"`
	ReadOnly *bool  `json:"readOnly,omitempty" yaml:"readOnly,omitempty"`
}

//toVolume convert to Volume
func (s *StorageTemplate) toVolume() Volume {
	pvc := PersistentVolumeClaimVolumeSource{
		ClaimName: s.Name,
		ReadOnly:  s.ReadOnly,
	}
	return Volume{
		Name: s.Name,
		PersistentVolumeClaim: &pvc,
	}
}

//HostPathVolumeSource hostPath vs
type HostPathVolumeSource struct {
	Path string        `json:"path" yaml:"path"`
	Type *HostPathType `json:"type,omitempty" yaml:"type,omitempty"`
}

//HostPathType hostPath type
type HostPathType string

const (
	//HostPathUnset blank
	HostPathUnset HostPathType = ""
	//HostPathDirectoryOrCreate dir or create
	HostPathDirectoryOrCreate HostPathType = "DirectoryOrCreate"
	//HostPathDirectory dir
	HostPathDirectory HostPathType = "Directory"
	//HostPathFileOrCreate file or create
	HostPathFileOrCreate HostPathType = "FileOrCreate"
	//HostPathFile file
	HostPathFile HostPathType = "File"
	//HostPathSocket socket
	HostPathSocket HostPathType = "Socket"
	//HostPathCharDev char dev
	HostPathCharDev HostPathType = "CharDevice"
	//HostPathBlockDev block dev
	HostPathBlockDev HostPathType = "BlockDevice"
)

//SecretVolumeSource secret volume
type SecretVolumeSource struct {
	SecretName  string      `json:"secretName,omitempty" yaml:"secretName,omitempty"`
	Items       []KeyToPath `json:"items,omitempty" yaml:"items,omitempty"`
	DefaultMode *int32      `json:"defaultMode,omitempty" yaml:"defaultMode,omitempty"`
	Optional    *bool       `json:"optional,omitempty" yaml:"optional,omitempty"`
}

//ConfigMapVolumeSource cm volume
type ConfigMapVolumeSource struct {
	LocalObjectReference `json:",inline" yaml:",inline"`
	Items                []KeyToPath `json:"items,omitempty" yaml:"items,omitempty"`
	DefaultMode          *int32      `json:"defaultMode,omitempty" yaml:"defaultMode,omitempty"`
	Optional             *bool       `json:"optional,omitempty" yaml:"optional,omitempty"`
}

//KeyToPath key to path within a volume
type KeyToPath struct {
	Key  string `json:"key" yaml:"key"`
	Path string `json:"path" yaml:"path"`
	Mode *int32 `json:"mode,omitempty" yaml:"mode,omitempty"`
}

// NFSVolumeSource volume schema for nfs type
type NFSVolumeSource struct {
	Server   string `json:"server" yaml:"server"`
	Path     string `json:"path" yaml:"path"`
	ReadOnly bool   `json:"readOnly,omitempty" yaml:"readOnly,omitempty" `
}

// RBDVolumeSource volume schema for rbd type
type RBDVolumeSource struct {
	CephMonitors []string              `json:"monitors" yaml:"monitors"`
	RBDImage     string                `json:"image" yaml:"image"`
	FSType       string                `json:"fsType,omitempty" yaml:"fsType,omitempty"`
	RBDPool      string                `json:"pool,omitempty" yaml:"pool,omitempty"`
	RadosUser    string                `json:"user,omitempty" yaml:"user,omitempty"`
	Keyring      string                `json:"keyring,omitempty" yaml:"keyring,omitempty"`
	SecretRef    *LocalObjectReference `json:"secretRef,omitempty" yaml:"secretRef,omitempty"`
	ReadOnly     bool                  `json:"readOnly,omitempty" json:"readOnly,omitempty"`
}

// CephFSVolumeSource volume schema for cpeh fs
type CephFSVolumeSource struct {
	Monitors   []string              `json:"monitors" yaml:"monitors"`
	Path       string                `json:"path,omitempty" yaml:"path,omitempty"`
	User       string                `json:"user,omitempty" yaml:"user,omitempty"`
	SecretFile string                `json:"secretFile,omitempty" yaml:"secretFile,omitempty"`
	SecretRef  *LocalObjectReference `json:"secretRef,omitempty" yaml:"secretRef,omitempty"`
	ReadOnly   bool                  `json:"readOnly,omitempty" yaml:"readOnly,omitempty"`
}

// EmptyDirVolumeSource schema for empty dir.
type EmptyDirVolumeSource struct {
	Medium    StorageMedium `json:"medium,omitempty" yaml:"medium,omitempty"`
	SizeLimit *string       `json:"sizeLimit,omitempty" yaml:"sizeLimit,omitempty"`
}

// StorageMedium storage type for empty dir
type StorageMedium string

const (
	//StorageMediumDefault default
	StorageMediumDefault StorageMedium = ""
	//StorageMediumMemory memory
	StorageMediumMemory StorageMedium = "Memory"
	//StorageMediumHugePages pages
	StorageMediumHugePages StorageMedium = "HugePages"
)
