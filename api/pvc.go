package api

//PersistentVolumeClaim pvcs
type PersistentVolumeClaim struct {
	APIVersion string `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       PersistentVolumeClaimSpec   `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status     PersistentVolumeClaimStatus `json:"status,omitempty" yaml:"status,omitempty"`
}

//PersistentVolumeClaimSpec pcv spec
type PersistentVolumeClaimSpec struct {
	AccessModes      []PersistentVolumeAccessMode `json:"accessModes,omitempty" yaml:"accessModes,omitempty"`
	Selector         *LabelSelector               `json:"selector,omitempty" yaml:"selector,omitempty"`
	Resources        ResourceRequirements         `json:"resources,omitempty" yaml:"resources,omitempty"`
	VolumeName       string                       `json:"volumeName,omitempty" yaml:"volumeName,omitempty"`
	StorageClassName *string                      `json:"storageClassName,omitempty" yaml:"storageClassName,omitempty"`
	VolumeMode       *PersistentVolumeMode        `json:"volumeMode,omitempty" yaml:"volumeMode,omitempty"`
}

//PersistentVolumeAccessMode access modes
type PersistentVolumeAccessMode string

const (
	//ReadWriteOnce mouted as read/write to exactly 1 host
	ReadWriteOnce PersistentVolumeAccessMode = "ReadWriteOnce"
	//ReadOnlyMany mounted as readonly to many hosts
	ReadOnlyMany PersistentVolumeAccessMode = "ReadOnlyMany"
	//ReadWriteMany mounted as read/write to many hosts
	ReadWriteMany PersistentVolumeAccessMode = "ReadWriteMany"
)

//PersistentVolumeMode pv mode
type PersistentVolumeMode string

const (
	// PersistentVolumeBlock mounted as raw block device
	PersistentVolumeBlock PersistentVolumeMode = "Block"
	// PersistentVolumeFilesystem mounted as file system
	PersistentVolumeFilesystem PersistentVolumeMode = "Filesystem"
)

// PersistentVolumeClaimStatus pvc status
type PersistentVolumeClaimStatus struct {
	Phase       PersistentVolumeClaimPhase       `json:"phase,omitempty" yaml:"phase,omitempty" `
	AccessModes []PersistentVolumeAccessMode     `json:"accessModes,omitempty" yaml:"accessModes,omitempty"`
	Capacity    ResourceList                     `json:"capacity,omitempty" yaml:"capacity,omitempty"`
	Conditions  []PersistentVolumeClaimCondition `json:"conditions,omitempty" yaml:"conditions,omitempty"`
}

//PersistentVolumeClaimPhase pvc phase
type PersistentVolumeClaimPhase string

const (
	//ClaimPending pending phase
	ClaimPending PersistentVolumeClaimPhase = "Pending"
	//ClaimBound  bound phase
	ClaimBound PersistentVolumeClaimPhase = "Bound"
	//ClaimLost lost phase
	ClaimLost PersistentVolumeClaimPhase = "Lost"
)

// PersistentVolumeClaimCondition pvc condition
type PersistentVolumeClaimCondition struct {
	Type               PersistentVolumeClaimConditionType `json:"type" yaml:"type"`
	Status             ConditionStatus                    `json:"status" yaml:"status"`
	LastProbeTime      string                             `json:"lastProbeTime,omitempty" yaml:"lastProbeTime,omitempty"`
	LastTransitionTime string                             `json:"lastTransitionTime,omitempty" json:"lastTransitionTime,omitempty"`
	Reason             string                             `json:"reason,omitempty" yaml:"reason,omitempty"`
	Message            string                             `json:"message,omitempty" yaml:"message,omitempty"`
}

// PersistentVolumeClaimConditionType pvc condition type
type PersistentVolumeClaimConditionType string

const (
	// PersistentVolumeClaimResizing resizing triggered by user
	PersistentVolumeClaimResizing PersistentVolumeClaimConditionType = "Resizing"
)
