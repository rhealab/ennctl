package api

import (
	"encoding/json"
	"errors"
)

// SysAdmin is the sturct for console sys admin type.
type SysAdmin struct {
	APIVersion string       `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string       `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta   `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       SysAdminSpec `json:"spec,omitempty" yaml:"spec,omitempty"`
}

//Valid validation
func (s SysAdmin) Valid() error {
	return s.Spec.valid()
}

// SysAdminCreateRequest User to create request
func (s *SysAdmin) SysAdminCreateRequest(actionType UserActionType) *SysAdminCreateRequest {
	switch actionType {
	case AddUserActionType:
		return &SysAdminCreateRequest{
			AddList:    s.Spec.SysadminList,
			RemoveList: nil,
		}
	case RemoveUserActionType:
		return &SysAdminCreateRequest{
			AddList:    nil,
			RemoveList: s.Spec.SysadminList,
		}
	default:
		return nil
	}
}

// SysAdminSpec is the struct for storage specification.
type SysAdminSpec struct {
	SysadminList []string `json:"sysAdmins,omitempty" yaml:"sysAdmins,omitempty"`
}

func (ss SysAdminSpec) valid() error {
	if len(ss.SysadminList) == 0 {
		return errors.New(SysAdmListIsRequiredError)
	}
	return nil
}

// SysAdminCreateRequest is used to construct sysadmin request.
type SysAdminCreateRequest struct {
	AddList    []string `json:"addList"`
	RemoveList []string `json:"removeList"`
}

// JSON get json bytes
func (s SysAdminCreateRequest) JSON() ([]byte, error) {
	return json.Marshal(s)
}

//UserActionType action type on user & sysadmin resource
type UserActionType string

const (
	//AddUserActionType action to add user
	AddUserActionType UserActionType = "addUser"
	//RemoveUserActionType action to add user
	RemoveUserActionType UserActionType = "removeUser"
)
