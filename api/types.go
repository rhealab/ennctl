package api

import "time"

// ObjectMeta is the struct for deployment meta.
type ObjectMeta struct {
	Name                       string            `json:"name,omitempty" yaml:"name,omitempty"`
	Namespace                  string            `json:"namespace,omitempty" yaml:"namespace,omitempty"`
	GenerateName               string            `json:"generateName,omitempty" yaml:"generateName,omitempty"`
	SelfLink                   string            `json:"selfLink,omitempty" json:"selfLink,omitempty"`
	UID                        string            `json:"uid,omitempty" yaml:"uid,omitempty"`
	ResourceVersion            string            `json:"resourceVersion,omitempty" yaml:"resourceVersion,omitempty"`
	Generation                 int64             `json:"generation,omitempty" yaml:"generation,omitempty"`
	DeletionTimestamp          *time.Time        `json:"deletionTimestamp,omitempty" yaml:"deletionTimestamp,omitempty"`
	DeletionGracePeriodSeconds *int64            `json:"deletionGracePeriodSeconds,omitempty" yaml:"deletionGracePeriodSeconds,omitempty"`
	CreationTimestamp          *time.Time        `json:"creationTimestamp,omitempty" yaml:"creationTimestamp,omitempty"`
	Label                      map[string]string `json:"labels,omitempty" yaml:"labels,omitempty"`
	Annotations                map[string]string `json:"annotations,omitempty" yaml:"annotations,omitempty"`
	ClusterName                string            `json:"clusterName,omitempty" yaml:"clusterName,omitempty"`
	Finalizers                 []string          `json:"finalizers,omitempty" yaml:"finalizers,omitempty"`
	Initializers               *Initializers     `json:"initializers,omitempty" yaml:"initializers,omitempty"`
	App                        *string           `json:"app,omitempty" yaml:"app,omitempty"`
}

// Initializers struct for init
type Initializers struct {
	Pending []Initializer `json:"pending" json:"pending"`

	// Result *Status `json:"result,omitempty" protobuf:"bytes,2,opt,name=result"`
}

// Initializer is information about an initializer that has not yet completed.
type Initializer struct {
	// name of the process that is responsible for initializing this object.
	Name string `json:"name" protobuf:"bytes,1,opt,name=name"`
}

// OwnerReference types for owner
type OwnerReference struct {
	APIVersion         string `json:"apiVersion" yaml:"apiVersion"`
	Kind               string `json:"kind" yaml:"kind"`
	Name               string `json:"name" yaml:"name"`
	UID                string `json:"uid" yaml:"uid"`
	Controller         *bool  `json:"controller,omitempty" yaml:"controller,omitempty"`
	BlockOwnerDeletion *bool  `json:"blockOwnerDeletion,omitempty" yaml:"blockOwnerDeletion,omitempty"`
}

// ConditionStatus condition
type ConditionStatus string

const (
	//ConditionTrue True
	ConditionTrue ConditionStatus = "True"
	//ConditionFalse false
	ConditionFalse ConditionStatus = "False"
	//ConditionUnknown unknown
	ConditionUnknown ConditionStatus = "Unknown"
)

// ErrorCodeResponse is the struct used in cc backend response.
type ErrorCodeResponse struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Payload map[string]interface{} `json:"payload"`
}

// Error is the struct holding error message
type Error struct {
	Code    int
	Message string
}
