package api

import (
	"encoding/json"
	"errors"
)

// User is the sturct for console storage type.
type User struct {
	APIVersion string     `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string     `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       UserSpec   `json:"spec,omitempty" yaml:"spec,omitempty"`
}

// UserCreateRequest User to create request
func (u *User) UserCreateRequest(actionType UserActionType) *UserCreateRequest {
	switch actionType {
	case AddUserActionType:
		return &UserCreateRequest{
			AddList:    u.Spec.UserList,
			RemoveList: nil,
		}
	case RemoveUserActionType:
		return &UserCreateRequest{
			AddList:    nil,
			RemoveList: u.Spec.UserList,
		}
	default:
		return nil
	}
}

// Valid validation
func (u User) Valid() error {
	if u.APIVersion != UserVersion {
		return errors.New(APIVersionNotSupportedError)
	}
	if u.ObjectMeta.Namespace == EmptyResource {
		return errors.New(NamespaceIsRequiredError)
	}
	return u.Spec.valid()
}

// UserSpec is the struct for storage specification.
type UserSpec struct {
	UserList []RoleInfo `json:"users,omitempty" yaml:"users,omitempty"`
}

func (us UserSpec) valid() error {
	for _, role := range us.UserList {
		if err := role.valid(); err != nil {
			return err
		}
	}
	return nil
}

// RoleInfo is used to constuct UserCreateRequest
type RoleInfo struct {
	UserID string `json:"userId,omitempty" yaml:"userId,omitempty"`
	Role   string `json:"role,omitempty" yaml:"role,omitempty"`
}

func (ri RoleInfo) valid() error {
	if ri.UserID == EmptyResource {
		return errors.New(UserIDIsRequiredError)
	}
	if ri.Role != NsadminAllCap && ri.Role != DeveloperAllCap {
		return errors.New(UserRoleIsNotSupportedError)
	}
	return nil
}

//========================= console user request =========================

// UserCreateRequest is used to construct role assignment info request of assign/remove roles API
type UserCreateRequest struct {
	AddList    []RoleInfo `json:"addList,omitempty" json:"addList,omitempty"`
	RemoveList []RoleInfo `json:"removeList,omitempty" json:"addList,omitempty"`
}

// JSON get json bytes
func (u UserCreateRequest) JSON() ([]byte, error) {
	return json.Marshal(u)
}
