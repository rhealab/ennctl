package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"

	"github.com/ghodss/yaml"
)

//Load function load from file
func Load(path string) (*Resource, error) {
	var r Resource
	yBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(yBytes, &r)
	if err != nil {
		return nil, err
	}
	return &r, nil
}

func printerJSON(src []byte) string {
	var prettyJSON bytes.Buffer
	json.Indent(&prettyJSON, src, "", "    ")
	return string(prettyJSON.Bytes())
}

func dispJSON(src []byte) {
	fmt.Println(printerJSON(src))
}

// stringToBytes is a util function parsing strings.
func float64StringToBytes(input string) int64 {
	ret := stringToFloat64(input)
	if ret != -1 {
		return int64(ret)
	}
	unit := input[len(input)-2:]
	digits := input[:len(input)-2]
	switch unit {
	case "Bi", "B":
		ret = stringToFloat64(digits)
	case "Ki", "KB":
		ret = stringToFloat64(digits)
		if ret != -1 {
			ret = ret * 1024
		}
	case "Mi", "MB":
		ret = stringToFloat64(digits)
		if ret != -1 {
			ret = ret * 1024 * 1024
		}
	case "Gi", "GB":
		ret = stringToFloat64(digits)
		if ret != -1 {
			ret = ret * 1024 * 1024 * 1024
		}
	default:
		ret = -1
	}
	return int64(ret)
}

func stringToFloat64(input string) float64 {
	if num, err := strconv.ParseFloat(input, 32); err == nil {
		return num
	}
	return -1
}

// bytesToStorageString is used to convert bytes to byte string.
// 10.1111Ki -> 10.11Ki
func bytesToStorageString(bytes int64) string {
	var ret string
	if bytes < 1024 {
		ret = fmt.Sprintf("%dBi", bytes)
	} else if bytes < (1024 * 1024) {
		ret = fmt.Sprintf("%.2fKi", float64(bytes)/1024)
	} else if bytes < (1024 * 1024 * 1024) {
		ret = fmt.Sprintf("%.2fMi", float64(bytes)/1024/1024)
	} else {
		ret = fmt.Sprintf("%.2fGi", float64(bytes)/1024/1024/1024)
	}
	return ret
}

func float64ToString(input float64) string {
	return fmt.Sprintf("%.2f", input)
}

// bytesToString is used to convert bytes to byte string
// 10.1111Ki -> 10Ki
func bytesToString(bytes int64) string {
	var ret string
	if bytes < 1024 {
		ret = fmt.Sprintf("%dBi", bytes)
	} else if bytes < (1024 * 1024) {
		ret = fmt.Sprintf("%dKi", bytes/1024)
	} else if bytes < (1024 * 1024 * 1024) {
		ret = fmt.Sprintf("%dMi", bytes/1024/1024)
	} else {
		ret = fmt.Sprintf("%dGi", bytes/1024/1024/1024)
	}
	return ret
}
