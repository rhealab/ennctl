package api

// ResourceRequirements is resource ResourceRequirements
type ResourceRequirements struct {
	Limits   ResourceList `json:"limits,omitempty" yaml:"limits,omitempty"`
	Requests ResourceList `json:"requests,omitempty" yaml:"requests,omitempty"`
}

// ResourceList is a set of (resource name, quantity) pairs.
type ResourceList map[ResourceName]string

// type ResourceList map[ResourceName]resource.Quantity

// ResourceName is the name identifying various resources in a ResourceList.
type ResourceName string

const (
	// ResourceCPU string
	ResourceCPU ResourceName = "cpu"
	// ResourceMemory string
	ResourceMemory ResourceName = "memory"
	// ResourceStorage string
	ResourceStorage ResourceName = "storage"
	// ResourceLocalStorage localStorage
	ResourceLocalStorage ResourceName = "localStorage"
	// ResourceRemoteStorage remoteStorage
	ResourceRemoteStorage ResourceName = "remoteStorage"
	// ResourceNfsStorage nfsStorage
	ResourceNfsStorage ResourceName = "nfsStorage"
	// ResourceEphemeralStorage string
	ResourceEphemeralStorage ResourceName = "ephemeral-storage"
	// ResourceNvidiaGPU string
	ResourceNvidiaGPU ResourceName = "alpha.kubernetes.io/nvidia-gpu"
)
