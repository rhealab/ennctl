package api

// LabelSelector is struct for label selector
type LabelSelector struct {
	MatchLabels      map[string]string          `json:"matchLabels,omitempty" yaml:"matchLabels,omitempty"`
	MatchExpressions []LabelSelectorRequirement `json:"matchExpressions,omitempty" yaml:"matchExpressions,omitempty"`
}

// LabelSelectorRequirement definition of label selector
type LabelSelectorRequirement struct {
	Key      string                `json:"key" yaml:"key"`
	Operator LabelSelectorOperator `json:"operator" yaml:"operator"`
	Values   []string              `json:"values,omitempty" yaml:"values,omitempty"`
}

// LabelSelectorOperator operators
type LabelSelectorOperator string

const (
	//LabelSelectorOpIn in
	LabelSelectorOpIn LabelSelectorOperator = "In"
	//LabelSelectorOpNotIn not in
	LabelSelectorOpNotIn LabelSelectorOperator = "NotIn"
	//LabelSelectorOpExists exists
	LabelSelectorOpExists LabelSelectorOperator = "Exists"
	//LabelSelectorOpDoesNotExist does not exist
	LabelSelectorOpDoesNotExist LabelSelectorOperator = "DoesNotExist"
)
