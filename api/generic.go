package api

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"

	"github.com/ghodss/yaml"
	homedir "github.com/mitchellh/go-homedir"
)

//Resource is generic k8s resource
type Resource struct {
	APIVersion string                  `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string                  `json:"kind,omitempty" yaml:"kind,omitempty"`
	Items      []Resource              `json:"items,omitempty" yaml:"items,omitempty"`
	Metadata   map[string]interface{}  `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       map[string]interface{}  `json:"spec,omitempty" yaml:"spec,omitempty"`
	Data       *map[string]interface{} `json:"data,omitempty" yaml:"data,omitempty"`
	StringData *map[string]string      `json:"stringData,omitempty" yaml:"stringData,omitempty"`
	Type       *string                 `json:"type,omitempty" json:"type,omitempty"`
}

//JSON to json string
func (r Resource) JSON() ([]byte, error) {
	y, err := yaml.Marshal(&r)
	if err != nil {
		return y, err
	}
	j, err := yaml.YAMLToJSON(y)
	if err != nil {
		return j, err
	}
	return j, nil
}

//Deployment Resource to deployment
func (r *Resource) Deployment() (*Deployment, error) {
	var d Deployment
	y, err := yaml.Marshal(r)
	if err != nil {
		return &d, err
	}
	err = yaml.Unmarshal(y, &d)
	return &d, err
}

//StatefulSet Resource to ss
func (r *Resource) StatefulSet() (*StatefulSet, error) {
	var s StatefulSet
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &s)
	return &s, err
}

//Service Resource to svc
func (r *Resource) Service() (*Service, error) {
	var s Service
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &s)
	return &s, err
}

//App Resource to app
func (r *Resource) App() (*App, error) {
	var a App
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &a)
	return &a, err
}

//Namespace Resource to app
func (r *Resource) Namespace() (*Namespace, error) {
	var n Namespace
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &n)
	return &n, err
}

//Storage Resource to storage
func (r *Resource) Storage() (*Storage, error) {
	var s Storage
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &s)
	return &s, err
}

//User Resource to storage
func (r *Resource) User() (*User, error) {
	var u User
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &u)
	return &u, err
}

//SysAdmin Resource to sys admin
func (r *Resource) SysAdmin() (*SysAdmin, error) {
	var s SysAdmin
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &s)
	return &s, err
}

//LimitRange Resource to storage
func (r *Resource) LimitRange() (*GlobalLimitRange, error) {
	var l GlobalLimitRange
	y, err := yaml.Marshal(r)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(y, &l)
	return &l, err
}

// Filter pre process of resource to console and legacy.
func (r *Resource) Filter() (*Resource, *Resource) {
	c, l := r.filterRecursive()
	var console, legacy *Resource
	if len(c) > 0 {
		console = &Resource{
			APIVersion: ListVersion,
			Kind:       ListKind,
			Items:      c,
		}
	}
	if len(l) > 0 {
		legacy = &Resource{
			APIVersion: ListVersion,
			Kind:       ListKind,
			Items:      l,
		}
	}
	return console, legacy
}

func (r *Resource) filterRecursive() ([]Resource, []Resource) {
	var console []Resource
	var legacy []Resource
	if r.Kind == ListKind {
		for _, item := range r.Items {
			c, l := item.filterRecursive()
			if c != nil {
				console = append(console, c...)
			}
			if l != nil {
				legacy = append(legacy, l...)
			}
		}
	} else if ennSupportKind[r.Kind] {
		console = append(console, *r)
	} else {
		legacy = append(legacy, *r)
	}
	return console, legacy
}

// BlackList filter on list
func (r *Resource) BlackList() (*Resource, string) {
	var resources []Resource
	var ret string
	for _, item := range r.Items {
		if blackListKind[item.Kind] {
			ret += fmt.Sprintf("kind %s is forbidden\n", item.Kind)
		} else {
			resources = append(resources, item)
		}
	}
	var retResource *Resource
	if len(resources) > 0 {
		retResource = &Resource{
			APIVersion: ListVersion,
			Kind:       ListKind,
			Items:      resources,
		}
	}
	if len(ret) > 0 {
		ret = ret[:len(ret)-1]
	}
	return retResource, ret
}

//SaveTmpFile to json string
func (r Resource) SaveTmpFile() (*string, error) {
	j, err := r.JSON()
	if err != nil {
		return nil, err
	}
	token := make([]byte, 8)
	rand.Read(token)
	hasher := sha1.New()
	hasher.Write(token)
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}
	tmpFile := filepath.Join(home, ".ennctl", sha)
	f, err := os.Create(tmpFile)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	f.Write(j)
	return &tmpFile, nil
}

var ennSupportKind = map[string]bool{
	AppKind:              true,
	NamespaceKind:        true,
	StorageKind:          true,
	UserKind:             true,
	SysAdminKind:         true,
	DeploymentKind:       true,
	ServiceKind:          true,
	StatefulSetKind:      true,
	GlobalLimitRangeKind: true,
	ListKind:             true,
}

var blackListKind = map[string]bool{
	PVCKind: true,
	PVKind:  true,
}
