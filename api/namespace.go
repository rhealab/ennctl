package api

import (
	"cc/ennctl/common"
	"encoding/json"
	"errors"

	yaml "gopkg.in/yaml.v1"
)

// Namespace struct for deployment
type Namespace struct {
	APIVersion string           `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string           `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta       `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       NamespaceSpec    `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status     *NamespaceStatus `json:"status,omitempty" yaml:"status,omitempty"`
}

// JSON get json string
func (n *Namespace) JSON() ([]byte, error) {
	return json.Marshal(n)
}

// YAML get json string
func (n *Namespace) YAML() ([]byte, error) {
	return yaml.Marshal(n)
}

// Valid validation
func (n Namespace) Valid() error {
	if n.APIVersion != NamespaceVersion {
		return errors.New(APIVersionNotSupportedError)
	}
	if n.ObjectMeta.Name == EmptyResource {
		return errors.New(NameIsRequiredError)
	}
	return n.Spec.valid()
}

// NamespaceCreateRequest convert namespace to namespace create request
func (n *Namespace) NamespaceCreateRequest() *NamespaceCreateRequest {
	ret := NamespaceCreateRequest{
		Name:               n.ObjectMeta.Name,
		AdminList:          n.Spec.NsAdminList,
		AllowCriticalPod:   n.Spec.AllowCriticalPod,
		AllowHostpath:      n.Spec.AllowHostpath,
		AllowPrivilege:     n.Spec.AllowPrivilege,
		CPURequest:         common.StringToFloat64(n.Spec.Resources.Requests[ResourceCPU]),
		CPULimit:           common.StringToFloat64(n.Spec.Resources.Limits[ResourceCPU]),
		MemoryRequestBytes: common.StringToBytes(n.Spec.Resources.Requests[ResourceMemory]),
		MemoryLimitBytes:   common.StringToBytes(n.Spec.Resources.Limits[ResourceMemory]),
	}
	if val, ok := n.Spec.Resources.Requests[ResourceLocalStorage]; ok {
		ret.LocalStorageBytes = common.StringToBytes(val)
	}
	if val, ok := n.Spec.Resources.Requests[ResourceRemoteStorage]; ok {
		ret.RemoteStorageBytes = common.StringToBytes(val)
	}
	return &ret
}

// NamespaceSpec ns specification
type NamespaceSpec struct {
	NsAdminList      []string             `json:"nsadmin,omitempty" yaml:"nsadmin,omitempty"`
	Resources        ResourceRequirements `json:"resources" yaml:"resources"`
	AllowPrivilege   bool                 `json:"allowPrivilege" yaml:"allowPrivilege"`
	AllowHostpath    bool                 `json:"allowHostpath" yaml:"allowHostpath"`
	AllowCriticalPod bool                 `json:"allowCriticalPod" yaml:"allowCriticalPod"`
	Finalizers       []FinalizerName      `json:"finalizers,omitempty" yaml:"finalizers,omitempty"`
}

func (ns NamespaceSpec) valid() error {
	if ns.Resources.Requests[ResourceCPU] == EmptyResource {
		return errors.New(CPURequestIsRequiredError)
	}
	if ns.Resources.Limits[ResourceCPU] == EmptyResource {
		return errors.New(CPULimitIsRequiredError)
	}
	if ns.Resources.Requests[ResourceMemory] == EmptyResource {
		return errors.New(MemoryRequestIsRequiredError)
	}
	if ns.Resources.Limits[ResourceMemory] == EmptyResource {
		return errors.New(MemoryLimitIsRequiredError)
	}
	if ns.Resources.Requests[ResourceLocalStorage] == EmptyResource {
		return errors.New(HostPathRequestIsRequiredError)
	}
	return nil
}

// NamespaceStatus ns status
type NamespaceStatus struct {
	Phase NamespacePhase `json:"phase,omitempty" yaml:"phase,omitempty"`
}

//NamespacePhase phase
type NamespacePhase string

const (
	// NamespaceActive active
	NamespaceActive NamespacePhase = "Active"
	// NamespaceTerminating terminating
	NamespaceTerminating NamespacePhase = "Terminating"
)

//FinalizerName name
type FinalizerName string

const (
	//FinalizerKubernetes kubernetes
	FinalizerKubernetes FinalizerName = "kubernetes"
)

//========================= console namespace request =========================

// NamespaceCreateRequest is used to construct Namespace Creation API request.
type NamespaceCreateRequest struct {
	Name               string   `json:"name"`
	AdminList          []string `json:"adminList"`
	AllowPrivilege     bool     `json:"allowPrivilege"`
	AllowHostpath      bool     `json:"allowHostpath"`
	AllowCriticalPod   bool     `json:"allowCriticalPod"`
	CPURequest         float64  `json:"cpuRequest"`
	CPULimit           float64  `json:"cpuLimit"`
	MemoryRequestBytes int64    `json:"memoryRequestBytes"`
	MemoryLimitBytes   int64    `json:"memoryLimitBytes"`
	LocalStorageBytes  int64    `json:"localStorageBytes,omitempty"`
	RemoteStorageBytes int64    `json:"remoteStorageBytes,omitempty"`
}

// JSON to json bytes
func (n NamespaceCreateRequest) JSON() ([]byte, error) {
	return json.Marshal(n)
}

//========================= console namespace edit API=========================

// NamespaceStat is struct for namespace stat
type NamespaceStat struct {
	Name               string  `json:"name"`
	AllowPrivilege     bool    `json:"allowPrivilege"`
	AllowHostpath      bool    `json:"allowHostpath"`
	AllowCriticalPod   bool    `json:"allowCriticalPod"`
	CPURequest         float64 `json:"cpuRequests"`
	CPULimit           float64 `json:"cpuLimits"`
	MemoryRequestBytes int64   `json:"memoryRequestsBytes"`
	MemoryLimitBytes   int64   `json:"memoryLimitsBytes"`
	HostPathBytes      *int64  `json:"hostPathBytes"`
	RbdBytes           *int64  `json:"rbdBytes"`
	NfsBytes           *int64  `json:"nfsBytes,omitempty"`
}

// Namespace convert namespace stat to console namespace
func (ns *NamespaceStat) Namespace() *Namespace {
	r := ResourceList{
		ResourceCPU:    float64ToString(ns.CPURequest),
		ResourceMemory: bytesToString(ns.MemoryRequestBytes),
	}
	if ns.HostPathBytes != nil {
		r[ResourceLocalStorage] = bytesToString(*ns.HostPathBytes)
	}
	if ns.RbdBytes != nil {
		r[ResourceRemoteStorage] = bytesToString(*ns.RbdBytes)
	}
	if ns.NfsBytes != nil {
		r[ResourceNfsStorage] = bytesToString(*ns.NfsBytes)
	}
	l := ResourceList{
		ResourceCPU:    float64ToString(ns.CPULimit),
		ResourceMemory: bytesToString(ns.MemoryLimitBytes),
	}
	resource := ResourceRequirements{
		Requests: r,
		Limits:   l,
	}
	nsSpec := NamespaceSpec{
		Resources:        resource,
		AllowPrivilege:   ns.AllowPrivilege,
		AllowHostpath:    ns.AllowHostpath,
		AllowCriticalPod: ns.AllowCriticalPod,
	}
	return &Namespace{
		APIVersion: NamespaceVersion,
		Kind:       NamespaceKind,
		ObjectMeta: ObjectMeta{
			Name: ns.Name,
		},
		Spec: nsSpec,
	}
}
