package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/ghodss/yaml"
)

// ServiceList struct for ServiceList
type ServiceList struct {
	APIVersion string    `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Items      []Service `json:"items,omitempty" yaml:"items,omitempty"`
	Kind       string    `json:"kind,omitempty" yaml:"kind,omitempty"`
}

//Service struct for service
type Service struct {
	APIVersion string        `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string        `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta    `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       ServiceSpec   `json:"spec,omitempty" yaml:"spec,omitempty"`
	Status     ServiceStatus `json:"status,omitempty" protobuf:"bytes,3,opt,name=status"`
}

// Valid validation
func (s Service) Valid() error {
	if s.ObjectMeta.Name == EmptyResource {
		return errors.New(NameIsRequiredError)
	}
	if s.ObjectMeta.Namespace == EmptyResource {
		return errors.New(NamespaceIsRequiredError)
	}
	return nil
}

//K8sStylize convert console style to k8s original style
func (s *Service) K8sStylize() *Service {
	if s.ObjectMeta.App == nil {
		return s
	}
	// set/overwrite X_APP label
	if s.ObjectMeta.Label == nil {
		s.ObjectMeta.Label = map[string]string{
			ConsoleAppLabel: *s.ObjectMeta.App,
		}
	} else {
		s.ObjectMeta.Label[ConsoleAppLabel] = *s.ObjectMeta.App
	}
	s.ObjectMeta.App = nil
	return s
}

//Name get name
func (s *Service) Name() string {
	return s.ObjectMeta.Name
}

//App get app
func (s *Service) App() string {
	if s.ObjectMeta.Label != nil {
		if val, ok := s.ObjectMeta.Label[ConsoleAppLabel]; ok {
			return val
		}
	}
	return Legacy
}

//Type get service type
func (s *Service) Type() string {
	return string(s.Spec.Type)
}

//ClusterIP get ClusterIP
func (s *Service) ClusterIP() string {
	return s.Spec.ClusterIP
}

//ExternalIP get ExternalIP
func (s *Service) ExternalIP() string {
	if len(s.Spec.ExternalIPs) == 0 {
		return NoneStr
	}
	var ret string
	for _, IP := range s.Spec.ExternalIPs {
		ret += IP + ","
	}
	return ret[0 : len(ret)-1]
}

//Ports get ExternalIP
func (s *Service) Ports() string {
	if len(s.Spec.Ports) == 0 {
		return NoneStr
	}
	var ret string
	for _, port := range s.Spec.Ports {
		ret += port.String()
	}
	return ret[0 : len(ret)-1]
}

//Age get age
func (s *Service) Age() string {
	dur := time.Since(*s.ObjectMeta.CreationTimestamp)
	if dur.Seconds() <= 60 {
		return fmt.Sprintf("%ds", int(dur.Seconds()))
	} else if dur.Minutes() <= 60 {
		return fmt.Sprintf("%dm", int(dur.Minutes()))
	} else if dur.Hours() <= 24 {
		return fmt.Sprintf("%dh", int(dur.Hours()))
	}
	return fmt.Sprintf("%dd", int(dur.Hours()/24))
}

//Selector to string
func (s *Service) Selector() string {
	ret := Empty
	if s.Spec.Selector != nil {
		for key, val := range s.Spec.Selector {
			ret += fmt.Sprintf("%s=%s,", key, val)
		}
	}
	if ret == Empty {
		return NoneStr
	}
	return ret[:len(ret)-1]
}

//ConsoleStylize convert console style deployment to k8s original
func (s *Service) ConsoleStylize() *Service {
	if app, ok := s.ObjectMeta.Label[ConsoleAppLabel]; ok {
		s.ObjectMeta.App = &app
		delete(s.ObjectMeta.Label, ConsoleAppLabel)
	}
	return s
}

// JSON get json string
func (s *Service) JSON() ([]byte, error) {
	return json.Marshal(s)
}

// YAML get json string
func (s *Service) YAML() ([]byte, error) {
	return yaml.Marshal(s)
}

// SliceJSON get json string
func (s *Service) SliceJSON() ([]byte, error) {
	var tmp []Service
	tmp = append(tmp, *s)
	return json.Marshal(tmp)
}

// ServiceSpec service specification
type ServiceSpec struct {
	Ports                    []ServicePort                    `json:"ports,omitempty" yaml:"ports,omitempty"`
	Selector                 map[string]string                `json:"selector,omitempty" yaml:"selector,omitempty"`
	ClusterIP                string                           `json:"clusterIP,omitempty" yaml:"clusterIP,omitempty"`
	Type                     ServiceType                      `json:"type,omitempty" yaml:"type,omitempty"`
	ExternalIPs              []string                         `json:"externalIPs,omitempty" yaml:"externalIPs,omitempty"`
	SessionAffinity          ServiceAffinity                  `json:"sessionAffinity,omitempty" yaml:"sessionAffinity,omitempty"`
	LoadBalancerIP           string                           `json:"loadBalancerIP,omitempty" yaml:"loadBalancerIP,omitempty"`
	LoadBalancerSourceRanges []string                         `json:"loadBalancerSourceRanges,omitempty" yaml:"loadBalancerSourceRanges,omitempty"`
	ExternalName             string                           `json:"externalName,omitempty" yaml:"externalName,omitempty"`
	ExternalTrafficPolicy    ServiceExternalTrafficPolicyType `json:"externalTrafficPolicy,omitempty" yaml:"externalTrafficPolicy,omitempty"`
	HealthCheckNodePort      int32                            `json:"healthCheckNodePort,omitempty" yaml:"healthCheckNodePort,omitempty"`
	PublishNotReadyAddresses bool                             `json:"publishNotReadyAddresses,omitempty" yaml:"publishNotReadyAddresses,omitempty"`
	SessionAffinityConfig    *SessionAffinityConfig           `json:"sessionAffinityConfig,omitempty" yaml:"sessionAffinityConfig,omitempty"`
}

// ServicePort info of service port
type ServicePort struct {
	Name       string   `json:"name,omitempty" yaml:"name,omitempty"`
	Protocol   Protocol `json:"protocol,omitempty" yaml:"protocol,omitempty"`
	Port       int32    `json:"port" yaml:"port"`
	TargetPort int32    `json:"targetPort,omitempty" yaml:"targetPort,omitempty"`
	NodePort   int32    `json:"nodePort,omitempty" yaml:"nodePort,omitempty"`
}

// String to string
func (sp ServicePort) String() string {
	if sp.NodePort == 0 {
		return fmt.Sprintf("%d/%s,", sp.Port, sp.Protocol)
	}
	return fmt.Sprintf("%d:%d/%s,", sp.Port, sp.NodePort, sp.Protocol)
}

//ServiceType ingress methods of a service
type ServiceType string

const (
	// ServiceTypeClusterIP only be accessible inside cluster via cluster IP
	ServiceTypeClusterIP ServiceType = "ClusterIP"

	// ServiceTypeNodePort exposed on one port of every node, in addition to ClusterIP.
	ServiceTypeNodePort ServiceType = "NodePort"

	// ServiceTypeLoadBalancer exposed via an external load balancer, in addition to NodePort.
	ServiceTypeLoadBalancer ServiceType = "LoadBalancer"

	// ServiceTypeExternalName external name
	ServiceTypeExternalName ServiceType = "ExternalName"
)

// ServiceAffinity Session Affinity type
type ServiceAffinity string

const (
	// ServiceAffinityClientIP ClientIP based
	ServiceAffinityClientIP ServiceAffinity = "ClientIP"

	// ServiceAffinityNone no session affinity
	ServiceAffinityNone ServiceAffinity = "None"
)

//ServiceExternalTrafficPolicyType external traffic policy type
type ServiceExternalTrafficPolicyType string

const (
	// ServiceExternalTrafficPolicyTypeLocal local
	ServiceExternalTrafficPolicyTypeLocal ServiceExternalTrafficPolicyType = "Local"
	// ServiceExternalTrafficPolicyTypeCluster cluster
	ServiceExternalTrafficPolicyTypeCluster ServiceExternalTrafficPolicyType = "Cluster"
)

// SessionAffinityConfig represents the configurations of session affinity.
type SessionAffinityConfig struct {
	ClientIP *ClientIPConfig `json:"clientIP,omitempty" yaml:"clientIP,omitempty"`
}

// ClientIPConfig client ip based session affinity configuration
type ClientIPConfig struct {
	TimeoutSeconds *int32 `json:"timeoutSeconds,omitempty" yaml:"timeoutSeconds,omitempty"`
}

// ServiceStatus svc status
type ServiceStatus struct {
	LoadBalancer LoadBalancerStatus `json:"loadBalancer,omitempty" yaml:"loadBalancer,omitempty"`
}

// LoadBalancerStatus represents the status of a load-balancer.
type LoadBalancerStatus struct {
	Ingress []LoadBalancerIngress `json:"ingress,omitempty" protobuf:"bytes,1,rep,name=ingress"`
}

//LoadBalancerIngress lb ingress
type LoadBalancerIngress struct {
	IP       string `json:"ip,omitempty" yaml:"ip,omitempty"`
	Hostname string `json:"hostname,omitempty" yaml:"hostname,omitempty"`
}
