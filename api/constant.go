package api

//ConsoleAppLabel label for app
const ConsoleAppLabel string = "X_APP"

//EmptyResource empty string
const EmptyResource string = ""

//Empty empty string
const Empty string = ""

const (
	//Legacy string legacy
	Legacy string = "Legacy"
	// NoneStr is used to display none at port, externalIPs and etc.
	NoneStr string = "<none>"
	// NodeStr is used to display external IP for nodePort svc.
	NodeStr string = "<nodes>"
)

//StorageVersion current storage version
const (
	//StorageVersion current storage version
	StorageVersion string = "v1"
	//ListVersion current list version
	ListVersion string = "v1"
	//AppVersion current app version
	AppVersion string = "v1"
	//NamespaceVersion current namespace version
	NamespaceVersion string = "v1"
	//UserVersion current user version
	UserVersion string = "v1"
	//DeployVersion current deploy version
	DeployVersion string = "extensions/v1beta1"
	//ServiceVersion current svc version
	ServiceVersion string = "v1"
	//StatefulSetVersion current ss version
	StatefulSetVersion string = "apps/v1beta1"
)

const (
	// NsadminAllCap is constant string for ns admin
	NsadminAllCap string = "NS_ADMIN"
	// DeveloperAllCap is constant string for developer
	DeveloperAllCap string = "DEVELOPER"
)

const (
	// Rbd : constant string for RBD
	Rbd string = "RBD"
	// CephFs is constant string for CephFS
	CephFs string = "CephFS"
	// HostPath is constant string for HostPath
	HostPath string = "HostPath"
	//StorageClass is constant string for StorageClass
	StorageClass string = "StorageClass"
	// NFS is constant string for NFS
	NFS string = "NFS"
)

const (
	//RWO is constant string for accessmode ReadWriteOnce
	RWO string = "ReadWriteOnce"
	// ROM is constant string for accessmode ReadOnlyMany
	ROM string = "ReadOnlyMany"
	// RWM is constant string for accessmode ReadWriteMany
	RWM string = "ReadWriteMany"
)

const (
	// ServiceKind constant string for kind of Service
	ServiceKind string = "Service"

	// StatefulSetKind constant string for kind of StatefulSet
	StatefulSetKind string = "StatefulSet"

	// ListKind constant string for kind of List
	ListKind string = "List"

	// AppKind is constant string for kind of App
	AppKind string = "App"

	// StorageKind is constant string for kind of storage
	StorageKind string = "Storage"

	// UserKind is constant string for kind of user
	UserKind string = "User"

	// NamespaceKind is constant string for kind of namespace
	NamespaceKind string = "Namespace"

	// SysAdminKind is constant string for kind of sysadmin
	SysAdminKind string = "SysAdmin"

	// DeploymentKind is constant string for kind of deployment
	DeploymentKind string = "Deployment"

	// GlobalLimitRangeKind is constant string for kind of limitRange
	GlobalLimitRangeKind string = "GlobalLimitRange"

	// PVCKind is constant string for kind of limitRange
	PVCKind string = "PersistentVolumeClaim"

	// PVKind is constant string for kind of limitRange
	PVKind string = "PersistentVolume"
)
