package api

import (
	"encoding/json"
	"errors"

	"github.com/ghodss/yaml"
)

// Storage is the sturct for console storage type.
type Storage struct {
	APIVersion string      `json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind       string      `json:"kind,omitempty" yaml:"kind,omitempty"`
	ObjectMeta ObjectMeta  `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Spec       StorageSpec `json:"spec,omitempty" yaml:"spec,omitempty"`
}

// Valid validation
func (s Storage) Valid() error {
	if s.APIVersion != StorageVersion {
		return errors.New(APIVersionNotSupportedError)
	}
	if s.ObjectMeta.Name == EmptyResource {
		return errors.New(NameIsRequiredError)
	}
	if s.ObjectMeta.Namespace == EmptyResource {
		return errors.New(NamespaceIsRequiredError)
	}
	return s.Spec.valid()
}

// JSON get json string
func (s *Storage) JSON() ([]byte, error) {
	return json.Marshal(s)
}

// YAML get json string
func (s *Storage) YAML() ([]byte, error) {
	return yaml.Marshal(s)
}

// StorageCreateRequest convert consoleApp to storage creation request
func (s *Storage) StorageCreateRequest() *StorageCreateRequest {
	return &StorageCreateRequest{
		StorageType: s.Spec.Type,
		StorageName: s.ObjectMeta.Name,
		AccessMode:  s.Spec.AccessMode,
		AmountBytes: float64StringToBytes(s.Spec.Resources.Requests[ResourceStorage]),
		Persisted:   s.Spec.Persisted,
		Unshared:    s.Spec.Unshared,
		ReadOnly:    s.Spec.ReadOnly,
	}
}

// StorageSpec is the struct for storage spec
type StorageSpec struct {
	Type       string               `json:"type" yaml:"type"`
	AccessMode string               `json:"accessModes" yaml:"accessModes"`
	Resources  ResourceRequirements `json:"resources" yaml:"resources"`
	Persisted  bool                 `json:"persisted" yaml:"persisted"`
	ReadOnly   bool                 `json:"readOnly" yaml:"readOnly"`
	Unshared   bool                 `json:"unshared" yaml:"unshared"`
}

func (ss StorageSpec) valid() error {
	if ss.Type == EmptyResource {
		return errors.New(StorageTypeIsRequired)
	}
	if ss.AccessMode != ROM && ss.AccessMode != RWM && ss.AccessMode != RWO {
		return errors.New(StorageAccessModeIsNotSupport)
	}
	if ss.Resources.Requests[ResourceStorage] == EmptyResource {
		return errors.New(StorageRequestIsRequiredError)
	}
	return nil
}

//========================= console storage request =========================

// StorageCreateRequest is used to construct Storage Creation API request
type StorageCreateRequest struct {
	StorageType string `json:"storageType"`
	StorageName string `json:"storageName"`
	AccessMode  string `json:"accessMode"`
	AmountBytes int64  `json:"amountBytes"`
	Persisted   bool   `json:"persisted"`
	Unshared    bool   `json:"unshared"`
	ReadOnly    bool   `json:"readOnly"`
}

//JSON get json bytes
func (s StorageCreateRequest) JSON() ([]byte, error) {
	return json.Marshal(s)
}

// StorageResponse is the structure of get storage API
type StorageResponse struct {
	ID                    int32         `json:"id"`
	Name                  string        `json:"storageName"`
	Namespace             string        `json:"namespaceName"`
	Type                  string        `json:"storageType"`
	AccessMode            string        `json:"accessMode"`
	AmountBytes           int64         `json:"amountBytes"`
	Persisted             bool          `json:"persisted"`
	Unshared              bool          `json:"unshared"`
	ReadOnly              bool          `json:"readOnly"`
	Status                string        `json:"status"`
	Mounted               bool          `json:"mounted"`
	DeploymentName        string        `json:"deploymentName"`
	AppName               string        `json:"appName"`
	UsedAmountBytes       int64         `json:"usedAmountBytes"`
	ModifiedOn            int64         `json:"modifiedOn"`
	ModifiedBy            string        `json:"modifiedBy"`
	CreatedOn             int64         `json:"createdOn"`
	CreatedBy             string        `json:"createdBy"`
	HostPathUsedBytesList []interface{} `json:"hostPathUsedBytesList"`
	RelatedWorkloadList   []interface{} `json:"relatedWorkloadList"`
}

func (s *StorageResponse) getAmoutByteString() string {
	return bytesToStorageString(s.AmountBytes)
}

// Storage convert storage response to  storage struct
func (s *StorageResponse) Storage() *Storage {

	return &Storage{
		APIVersion: StorageVersion,
		Kind:       StorageKind,
		ObjectMeta: ObjectMeta{
			Name:      s.Name,
			Namespace: s.Namespace,
		},
		Spec: StorageSpec{
			Type:       s.Type,
			AccessMode: s.AccessMode,
			Persisted:  s.Persisted,
			Unshared:   s.Unshared,
			ReadOnly:   s.ReadOnly,
			Resources: ResourceRequirements{
				Requests: map[ResourceName]string{
					ResourceStorage: s.getAmoutByteString(),
				},
			},
		},
	}
}
