package console

import (
	"cc/ennctl/common"
	"encoding/json"
	"errors"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console storage printer =========================

// StoragePrinter struct to print
type StoragePrinter struct {
	PrinterConfig
	err          ErrorCodeResponse
	storageSlice []StorageResponse
}

// Print default entrance for printer
func (sp *StoragePrinter) Print() error {
	if !common.IsSuccessHTTPCode(sp.HTTPCode) {
		return sp.printError()
	}
	if sp.Action != Get {
		return sp.printAction()
	}
	if sp.OutputFormat == JSONFormat {
		return sp.filterByName().printJSON()
	} else if sp.OutputFormat == YamlFormat {
		return sp.filterByName().printYAML()
	}
	return sp.filterByName().printName()
}

// Implement printerInterface
func (sp *StoragePrinter) printName() error {
	if len(sp.storageSlice) == 0 {
		return errors.New(NoResourceFound)
	}
	sort.Slice(sp.storageSlice, func(i int, j int) bool {
		return sp.storageSlice[i].Name < sp.storageSlice[j].Name
	})
	var content [][]string
	if sp.OutputFormat == WideFormat {
		content = [][]string{{"NAME", "STATUS", "MOUNT", "CAPACITY", "USED", "TYPE", "ACCESSMODES", "AGE", "APP(s)", "WORKLOAD(S)", "POD(S)", "REPLICAS"}}
		for _, storage := range sp.storageSlice {
			content = append(content, []string{
				storage.getName(),
				storage.getStatus(),
				storage.getMountStatus(),
				storage.getAmount(),
				storage.getUsedAmount(),
				storage.getType(),
				storage.getAccessMode(),
				storage.getAge(),
				storage.getApp(),
				storage.getWorkLoad(),
				storage.getPod(),
				storage.getReplica(),
			})
		}
	} else {
		content = [][]string{{"NAME", "STATUS", "MOUNT", "CAPACITY", "USED", "TYPE", "ACCESSMODES", "AGE"}}
		for _, storage := range sp.storageSlice {
			content = append(content, []string{
				storage.getName(),
				storage.getStatus(),
				storage.getMountStatus(),
				storage.getAmount(),
				storage.getUsedAmount(),
				storage.getType(),
				storage.getAccessMode(),
				storage.getAge(),
			})
		}
	}

	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (sp *StoragePrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npaylaod: %v", sp.Action, sp.Resource, sp.ResourceName, sp.err.Message, sp.err.Payload)
}

func (sp *StoragePrinter) printAction() error {
	fmt.Printf("%s %s %s\n", sp.Resource, sp.ResourceName, actionMap[sp.Action])
	return nil
}

func (sp *StoragePrinter) printJSON() error {
	j, err := json.Marshal(sp.storageSlice)
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (sp *StoragePrinter) printYAML() error {
	y, err := yaml.Marshal(&sp.storageSlice)
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (sp *StoragePrinter) filterByName() *StoragePrinter {
	if sp.ResourceNames == nil {
		return sp
	}
	m := make(map[string]bool)
	for _, rn := range sp.ResourceNames {
		m[rn] = true
	}
	ret := sp.storageSlice[:0]
	for _, s := range sp.storageSlice {
		if m[s.getName()] {
			ret = append(ret, s)
		}
	}
	sp.storageSlice = ret
	return sp
}
