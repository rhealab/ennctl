package console

import (
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console namespace printer =========================

// NamespacePrinter struct to print
type NamespacePrinter struct {
	PrinterConfig
	err            ErrorCodeResponse
	namespaceSlice []NamespaceResponse
}

// Print default entrance for printer
func (np *NamespacePrinter) Print() error {
	if !common.IsSuccessHTTPCode(np.HTTPCode) {
		return np.printError()
	}
	if np.Action != Get {
		return np.printAction()
	}
	if np.OutputFormat == JSONFormat {
		return np.filterByName().printJSON()
	} else if np.OutputFormat == YamlFormat {
		return np.filterByName().printYAML()
	}
	return np.filterByName().printName()
}

func (np *NamespacePrinter) printName() error {
	if len(np.namespaceSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	sort.Slice(np.namespaceSlice, func(i int, j int) bool {
		return np.namespaceSlice[i].Name < np.namespaceSlice[j].Name
	})
	var content [][]string
	if np.OutputFormat == WideFormat {
		content = [][]string{{"NAME", "STATUS", "TYPE", "AGE", "ROLE", "PRIVILEGE-MODE", "HOSTPATH-MODE", "HIGH-PRIORITY"}}
		for _, namespace := range np.namespaceSlice {
			content = append(content, []string{
				namespace.getName(),
				namespace.getStatus(),
				namespace.getType(),
				namespace.getAge(),
				namespace.getRole(),
				namespace.getPrivilege(),
				namespace.getHostPath(),
				namespace.getHighPriority(),
			})
		}
	} else {
		content = [][]string{{"NAME", "STATUS", "TYPE", "AGE"}}
		for _, namespace := range np.namespaceSlice {
			content = append(content, []string{
				namespace.getName(),
				namespace.getStatus(),
				namespace.getType(),
				namespace.getAge(),
			})
		}
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (np *NamespacePrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", np.Action, np.Resource, np.ResourceName, np.err.Message, np.err.Payload)
}

func (np *NamespacePrinter) printAction() error {
	fmt.Printf("%s %s %s\n", np.Resource, np.ResourceName, actionMap[np.Action])
	return nil
}

func (np *NamespacePrinter) printJSON() error {
	j, err := json.Marshal(np.namespaceSlice)
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (np *NamespacePrinter) printYAML() error {
	y, err := yaml.Marshal(&np.namespaceSlice)
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (np *NamespacePrinter) filterByName() *NamespacePrinter {
	if np.ResourceNames == nil {
		return np
	}
	m := make(map[string]bool)
	for _, rn := range np.ResourceNames {
		m[rn] = true
	}
	ret := np.namespaceSlice[:0]
	for _, n := range np.namespaceSlice {
		if m[n.getName()] {
			ret = append(ret, n)
		}
	}
	np.namespaceSlice = ret
	return np
}
