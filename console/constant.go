package console

//ConsoleAppLabel label for app
const ConsoleAppLabel string = "X_APP"

// Legacy constant string for legacy
const Legacy string = "Legacy"

//EmptyResource empty string
const EmptyResource string = ""

//Enable enable string
const Enable string = "Y"

//Disable enable string
const Disable string = "N"

// action string
const (
	// Create is constant string for action create
	Create string = "create"
	// Delete is constant string for action delete
	Delete string = "delete"
	// Get is constant string for action get
	Get string = "get"
	// Replace is constant string for action replace
	Replace string = "replace"
	// Export is constant string for action export
	Export string = "export"
	// Export is constant string for action export
	Import string = "import"
	// Edit is constant string for action get
	Edit string = "edit"
	// Scale is constant string for action edit
	Scale string = "scale"
)

const (
	// YamlFormat constant string for yaml output format
	YamlFormat string = "yaml"

	// JSONFormat constant string for json output format
	JSONFormat string = "json"
	// WideFormat print more information
	WideFormat string = "wide"
)

const (
	// TCP is the consant string for TCP
	TCP string = "TCP"
	// UDP is the constant string for UDP
	UDP string = "UDP"
	// NoneStr is used to display none at port, externalIPs and etc.
	NoneStr string = "<none>"
	// NodePort is constant string for node port svc
	NodePort string = "NodePort"
	// NodeStr is used to display external IP for nodePort svc.
	NodeStr string = "<nodes>"
	// None is used to display None on any field where data is missing
	None string = "None"
)

// ToDo constant string for todo placeholder
const ToDo string = "TODO"

const (
	// DeletionResponseOK is the constant string for deletion OK
	DeletionResponseOK string = "ok"
	// DeletionResponseError is the constant string for deletion error
	DeletionResponseError string = "error"
)

const (
	// Rbd : constant string for RBD
	Rbd string = "RBD"
	// CephFs is constant string for CephFS
	CephFs string = "CephFS"
	// HostPath is constant string for HostPath
	HostPath string = "HostPath"
	//StorageClass is constant string for StorageClass
	StorageClass string = "StorageClass"
	// NFS is constant string for NFS
	NFS string = "NFS"
)

const (
	// ContainerRunningStatus is the container running status from API.
	ContainerRunningStatus string = "running"

	// ContainerWaitingStatus is the container waiting status from API.
	ContainerWaitingStatus string = "waiting"
)

// NoResourceFound constant string for no resource found
const NoResourceFound string = "No resources found."

const (
	// App constant string for app resource
	App string = "app"

	// Storage constant string for storage resource
	Storage string = "storage"

	// User constant string for role_assignment resource.
	User string = "user"

	//Node is constant string for node
	Node string = "node"

	// Sysadmin constant string for sysdmins
	Sysadmin string = "sysadmin"

	// Developer is constant string for developer
	Developer string = "developer"

	// NsAdmin is constant string for nsadmin
	NsAdmin string = "nsadmin"

	// Pod is constant string for pod
	Pod string = "pod"

	// Namespace constant string for namespace
	Namespace string = "namespace"

	// Service constant string for service
	Service string = "service"

	// StatefulSet constant string for statefulset
	StatefulSet string = "statefulset"

	//Deployment is constant string for deployment
	Deployment string = "deployment"

	// LimitRange is constant string for limit range
	LimitRange string = "globalLimitRange"
)
