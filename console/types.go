package console

// ErrorCodeResponse is the struct used in cc backend response.
type ErrorCodeResponse struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Payload map[string]interface{} `json:"payload"`
}
