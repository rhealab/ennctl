package console

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console service printer =========================

// ServicePrinter struct to print
type ServicePrinter struct {
	PrinterConfig
	err          ErrorCodeResponse
	serviceSlice []api.Service
}

// Print default entrance for printer
func (sp *ServicePrinter) Print() error {
	if !common.IsSuccessHTTPCode(sp.HTTPCode) {
		return sp.printError()
	}

	//@TODO remove this code fragment once kubeorigin is ready.
	if sp.Action == Delete {
		return sp.printDeletion()
	}

	if sp.Action != Get {
		return sp.printAction()
	}

	if sp.OutputFormat == JSONFormat {
		return sp.filterByName().applyFilter().printJSON()
	} else if sp.OutputFormat == YamlFormat {
		return sp.filterByName().applyFilter().printYAML()
	}
	return sp.filterByName().applyFilter().printName()
}

// Implement printerInterface
func (sp *ServicePrinter) printName() error {
	if len(sp.serviceSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	sort.Slice(sp.serviceSlice, func(i int, j int) bool {
		return sp.serviceSlice[i].Name() < sp.serviceSlice[j].Name()
	})
	var content [][]string
	if sp.OutputFormat == WideFormat {
		content = [][]string{{"NAME", "APP", "TYPE", "CLUSTER-IP", "EXTERNAL-IP", "PORT(S)", "AGE", "SELECTOR"}}
		for _, svc := range sp.serviceSlice {
			content = append(content, []string{
				svc.Name(),
				svc.App(),
				svc.Type(),
				svc.ClusterIP(),
				svc.ExternalIP(),
				svc.Ports(),
				svc.Age(),
				svc.Selector(),
			})
		}
	} else {
		content = [][]string{{"NAME", "APP", "TYPE", "CLUSTER-IP", "EXTERNAL-IP", "PORT(S)", "AGE"}}
		for _, svc := range sp.serviceSlice {
			content = append(content, []string{
				svc.Name(),
				svc.App(),
				svc.Type(),
				svc.ClusterIP(),
				svc.ExternalIP(),
				svc.Ports(),
				svc.Age(),
			})
		}
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

// @TODO: this is a workaround, we should remove this part of code once deletion from kubeorign give correct response.
func (sp *ServicePrinter) printDeletion() error {
	var deleteResponse map[string]string
	err := json.Unmarshal(sp.RawResponse, &deleteResponse)
	if err != nil {
		return err
	}
	if deleteResponse["status"] == DeletionResponseOK {
		return sp.printAction()
	}
	return sp.printError()
}

func (sp *ServicePrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", sp.Action, sp.Resource, sp.ResourceName, sp.err.Message, sp.err.Payload)
}

func (sp *ServicePrinter) printAction() error {
	fmt.Printf("%s %s %s\n", sp.Resource, sp.ResourceName, actionMap[sp.Action])
	return nil
}

func (sp *ServicePrinter) printJSON() error {
	var j []byte
	var err error
	if len(sp.serviceSlice) == 1 {
		j, err = json.Marshal(sp.serviceSlice[0])
	} else {
		l := api.ServiceList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      sp.serviceSlice,
		}
		j, err = json.Marshal(l)
	}
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (sp *ServicePrinter) printYAML() error {
	var y []byte
	var err error
	if len(sp.serviceSlice) == 1 {
		y, err = yaml.Marshal(&sp.serviceSlice[0])
	} else {
		l := api.ServiceList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      sp.serviceSlice,
		}
		y, err = yaml.Marshal(&l)
	}
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (sp *ServicePrinter) applyFilter() *ServicePrinter {
	if sp.Filter == EmptyResource {
		return sp
	}
	// Filter by Legacy app
	if sp.Filter == Legacy {
		ret := sp.serviceSlice[:0]
		for _, svc := range sp.serviceSlice {
			if _, ok := svc.ObjectMeta.Label[ConsoleAppLabel]; !ok {
				ret = append(ret, svc)
			}
		}
		sp.serviceSlice = ret
		return sp
	}
	// Filter by appName
	ret := sp.serviceSlice[:0]
	for _, svc := range sp.serviceSlice {
		if svc.ObjectMeta.Label[ConsoleAppLabel] == sp.Filter {
			ret = append(ret, svc)
		}
	}
	sp.serviceSlice = ret
	return sp
}

func (sp *ServicePrinter) filterByName() *ServicePrinter {
	if sp.ResourceNames == nil {
		return sp
	}
	m := make(map[string]bool)
	for _, rn := range sp.ResourceNames {
		m[rn] = true
	}
	ret := sp.serviceSlice[:0]
	for _, s := range sp.serviceSlice {
		if m[s.Name()] {
			ret = append(ret, s)
		}
	}
	sp.serviceSlice = ret
	return sp
}
