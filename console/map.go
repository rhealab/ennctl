package console

var namespaceStatusMap = map[string]string{
	"CREATE_SUCCESS": "Active",
	"CREATE_FAILED":  "CreateFailed",
	"CREATE_PENDING": "CreatePending",
	"UPDATE_SUCCESS": "Active",
	"UPDATE_FAILED":  "UpdateFailed",
	"UPDATE_PENDING": "UpdatePending",
	"DELETE_PENDING": "DeletePending",
	"DELETE_SUCCESS": "DeleteSucccess",
	"DELETE_FAILED":  "DeleteFailed",
}

var roleMap = map[string]string{
	"ns_admin":  "NSAdmin",
	"sys_admin": "SysAdmin",
	"developer": "Developer",
	"":          "None",
}

var capRoleMap = map[string]string{
	"SYS_ADMIN": "SysAdmin",
	"NS_ADMIN":  "NSAdmin",
	"DEVELOPER": "Developer",
	"":          "None",
}
var appStatusMap = map[string]string{
	"NORMAL":         "Normal",
	"CREATE_SUCCESS": "Active",
	"CREATE_FAILED":  "CreateFailed",
	"CREATE_PENDING": "CreatePending",
	"UPDATE_SUCCESS": "Active",
	"UPDATE_FAILED":  "UpdateFailed",
	"UPDATE_PENDING": "UpdatePending",
	"DELETE_PENDING": "DeletePending",
	"DELETE_SUCCESS": "DeleteSucccess",
	"DELETE_FAILED":  "DeleteFailed",
}

var appTypeMap = map[string]string{
	"CUSTOM":    "Custom",
	"PREDEFINE": "PreDefined",
}

var userStatusMap = map[string]string{
	"ENABLED": "Active",
}

var storageStatusMap = map[string]string{
	"CREATE_SUCCESS": "Active",
	"CREATE_FAILED":  "CreateFailed",
	"CREATE_PENDING": "CreatePending",
	"UPDATE_SUCCESS": "Active",
	"UPDATE_FAILED":  "UpdateFailed",
	"UPDATE_PENDING": "UpdatePending",
	"DELETE_FAILED":  "DeleteFailed",
	"DELETE_SUCCESS": "DeleteSuccess",
	"DELETE_PENDING": "DeletePending",
}

var accessModeMap = map[string]string{
	"ReadWriteOnce": "RWO",
	"ReadWriteMany": "RWM",
	"ReadOnlyMany":  "ROM",
}
var actionMap = map[string]string{
	Create:  "created",
	Delete:  "deleted",
	Replace: "replaced",
	Edit:    "edited",
	Export:  "exported",
	Import:  "imported",
	Scale:   "scaled",
}
