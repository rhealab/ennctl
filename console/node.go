package console

import (
	"cc/ennctl/common"
	"strconv"
)

// NodeResponse struct of get pod API
type NodeResponse struct {
	Name                      string  `yaml:"name" json:"name"`
	IP                        string  `yaml:"ip" json:"ip"`
	UID                       string  `yaml:"uid" json:"uid"`
	Status                    string  `yaml:"status" json:"status"`
	Role                      string  `yaml:"role" json:"role"`
	PodNumber                 int     `yaml:"podNum" json:"podNum"`
	OS                        string  `yaml:"os" json:"os"`
	Age                       int     `yaml:"age" json:"age"`
	MemorySumBytes            int64   `yaml:"memorySumBytes" json:"memorySumBytes"`
	MemoryRequestsBytes       int64   `yaml:"memoryRequestsBytes" json:"memoryRequestsBytes"`
	MemoryLimitsBytes         int64   `yaml:"memoryLimitsBytes" json:"memoryLimitsBytes"`
	LocalStorageUsageBytes    int64   `yaml:"localStorageUsageBytes" json:"localStorageUsageBytes"`
	LocalStorageSumBytes      int64   `yaml:"localStorageSumBytes" json:"localStorageSumBytes"`
	LocalStorageRequestsBytes int64   `yaml:"localStorageRequestsBytes" json:"localStorageRequestsBytes"`
	CPUSum                    float64 `yaml:"cpuSum" json:"cpuSum"`
	CPULimits                 float64 `yaml:"cpuLimits" json:"cpuLimits"`
	CPURequests               float64 `yaml:"cpuRequests" json:"cpuRequests"`
	KernelVersion             string  `yaml:"kernelVersion" json:"kernelVersion"`
	KubeProxyVersion          string  `yaml:"kubeProxyVersion" json:"kubeProxyVersion"`
	KubeletVersion            string  `yaml:"kubeletVersion" json:"kubeletVersion"`
	CreatedOn                 int64   `yaml:"createdOn" json:"createdOn"`
}

func (node *NodeResponse) getAge() string {
	return common.TimestampToAge(int64(node.CreatedOn))
}

func (node *NodeResponse) getName() string {
	return node.Name
}

func (node *NodeResponse) getStatus() string {
	return node.Status
}

func (node *NodeResponse) getRole() string {
	return node.Role
}

func (node *NodeResponse) getOS() string {
	return node.OS
}

func (node *NodeResponse) getVersion() string {
	return node.KubeletVersion
}

func (node *NodeResponse) getPodNumber() string {
	return strconv.Itoa(node.PodNumber)
}
