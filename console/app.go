package console

import (
	"cc/ennctl/common"
)

//========================= console app response =========================

// AppResponse structure of get apps API
type AppResponse struct {
	ID            int32  `json:"id"`
	Name          string `json:"name"`
	Namespace     string `json:"namespace"`
	Status        string `json:"status"`
	Type          string `json:"type"`
	PredefineType string `json:"predefineType"`
	LastUpdatedOn int64  `json:"lastUpdatedOn"`
	LastUpdatedBy string `json:"lastUpdatedBy"`
	CreatedOn     int64  `json:"createdOn"`
	CreatedBy     string `json:"createdBy"`
}

func (a *AppResponse) getName() string {
	return a.Name
}

func (a *AppResponse) getCreatedBy() string {
	return a.CreatedBy
}

func (a *AppResponse) getNamespace() string {
	return a.Namespace
}

func (a *AppResponse) getType() string {
	return appTypeMap[a.Type]
}

func (a *AppResponse) getStatus() string {
	return appStatusMap[a.Status]
}

func (a *AppResponse) getAge() string {
	return common.TimestampToAge(a.CreatedOn)
}
