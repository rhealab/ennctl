package console

import (
	"cc/ennctl/common"
	"errors"
	"fmt"
)

// UserResponse structure of get apps API
type UserResponse struct {
	ID           string `json:"userId"`
	Name         string `json:"username"`
	Namespace    string `json:"namespace"`
	Rolename     string `json:"roleName"`
	AssignedDate int64  `json:"assignedDate"`
	AssignedBy   string `json:"assignedBy"`
	Enabled      bool   `json:"enabled"`
	Status       string `json:"status"`
}

func (ur *UserResponse) getRole() string {
	return roleMap[ur.Rolename]
}

func (ur *UserResponse) getName() string {
	return ur.ID
}

func (ur *UserResponse) getStatus() string {
	return userStatusMap[ur.Status]
}

func (ur *UserResponse) getAssignedBy() string {
	if ur.AssignedBy == EmptyResource {
		return "Unknown"
	}
	return ur.AssignedBy
}

func (ur *UserResponse) getAge() string {
	return common.TimestampToAge(ur.AssignedDate)
}

// UserUpdateResponse structure of get apps API
type UserUpdateResponse struct {
	AddList    []UserUpdateTemplate `json:"addList"`
	RemoveList []UserUpdateTemplate `json:"removeList"`
	UpdateList []UserUpdateTemplate `json:"updateList"`
}

func (uur *UserUpdateResponse) getCreateResponse() error {
	ret := ""
	for _, user := range uur.AddList {
		if err := user.print(Create); err != nil {
			// ret += err.Error()
			ret += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if ret != EmptyResource {
		return errors.New(ret[0 : len(ret)-1])
	}
	return nil
}

func (uur *UserUpdateResponse) getDeleteResponse() error {
	ret := ""
	for _, user := range uur.RemoveList {
		if err := user.print(Delete); err != nil {
			ret += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if ret != EmptyResource {
		return errors.New(ret[0 : len(ret)-1])
	}
	return nil
}

/*
	"userId": "ddd",
	"role": "DEVELOPER",
	"code": 8000020,
	"message": "the user is not exists",
	"payload": null,
	"httpStatus": 404
*/

// UserUpdateTemplate role update template
type UserUpdateTemplate struct {
	ID         string                 `json:"userId"`
	Role       string                 `json:"role"`
	Code       int                    `json:"code"`
	Message    string                 `json:"message"`
	Payload    map[string]interface{} `json:"payload"`
	HTTPStatus int                    `json:"httpStatus"`
}

func (uut *UserUpdateTemplate) print(action string) error {
	if uut.Code == 0 {
		return uut.printAction(action)
	}
	return uut.printError(action)
}

func (uut *UserUpdateTemplate) printAction(action string) error {
	fmt.Printf("%s %s %s\n", capRoleMap[uut.Role], uut.ID, actionMap[action])
	return nil
}

func (uut *UserUpdateTemplate) printError(action string) error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s", action, uut.Role, uut.ID, uut.Message)
}
