package console

import (
	"cc/ennctl/common"
)

// NamespaceResponse structure of get ns API
type NamespaceResponse struct {
	Name             string   `yaml:"name" json:"name"`
	MyRoleName       string   `yaml:"myRoleName" json:"myRoleName"`
	Type             string   `yaml:"type" json:"type"`
	NsAdminList      []string `yaml:"nsAdminList" json:"nsAdminList"`
	Status           string   `yaml:"status" json:"status"`
	CreatedOn        int64    `yaml:"createdOn" json:"createdOn"`
	CreatedByConsole bool     `yaml:"createdByConsole" json:"createdByConsole"`
	LastAccessOn     int64    `yaml:"lastAccessOn" json:"lastAccessOn"`
	CreatedBy        string   `yaml:"createdBy" json:"createdBy"`
	AllowPrivilege   bool     `yaml:"allowPrivilege" json:"allowPrivilege"`
	AllowHostpath    bool     `yaml:"allowHostpath" json:"allowHostpath"`
	AllowCriticalPod bool     `yaml:"allowCriticalPod" json:"allowCriticalPod"`
}

// NamespaceStatus get status
func (nr *NamespaceResponse) getStatus() string {
	return namespaceStatusMap[nr.Status]
}

// Age get age
func (nr *NamespaceResponse) getAge() string {
	return common.TimestampToAge(nr.CreatedOn)
}

// Role get age
func (nr *NamespaceResponse) getRole() string {
	return roleMap[nr.MyRoleName]
}

// NamespaceType get type
func (nr *NamespaceResponse) getType() string {
	return nr.Type
}

// NamespaceName get name
func (nr *NamespaceResponse) getName() string {
	return nr.Name
}

// Privilege get Privilege
func (nr *NamespaceResponse) getPrivilege() string {
	if nr.AllowPrivilege {
		return Enable
	}
	return Disable
}

// HostPath get Privilege
func (nr *NamespaceResponse) getHostPath() string {
	if nr.AllowHostpath {
		return Enable
	}
	return Disable
}

// HighPriority get Privilege
func (nr *NamespaceResponse) getHighPriority() string {
	if nr.AllowCriticalPod {
		return Enable
	}
	return Disable
}
