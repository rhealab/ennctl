package console

func truncateFloat64(in float64) float64 {
	return float64(int(in*100)) / 100
}
