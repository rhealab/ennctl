package console

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console pod printer =========================

// PodPrinter struct to print pod
type PodPrinter struct {
	PrinterConfig
	err      ErrorCodeResponse
	podSlice []api.Pod
}

// Print default entrance for printer
func (pp *PodPrinter) Print() error {
	if !common.IsSuccessHTTPCode(pp.HTTPCode) {
		return pp.printError()
	}

	if pp.Action != Get {
		return pp.printAction()
	}
	if pp.OutputFormat == JSONFormat {
		return pp.filterByName().applyFilter().printJSON()
	} else if pp.OutputFormat == YamlFormat {
		return pp.filterByName().applyFilter().printYAML()
	}
	return pp.filterByName().applyFilter().printName()
}

// Implement printerInterface
func (pp *PodPrinter) printName() error {
	if len(pp.podSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	sort.Slice(pp.podSlice, func(i int, j int) bool {
		return pp.podSlice[i].Name() < pp.podSlice[j].Name()
	})
	var content [][]string
	if pp.OutputFormat == WideFormat {
		content = [][]string{{"NAME", "READY", "STATUS", "RESTARTS", "AGE", "APP", "IP", "NODE"}}
		for _, pod := range pp.podSlice {
			content = append(content, []string{
				pod.Name(),
				pod.Ready(),
				pod.PodStatus(),
				pod.RestartCount(),
				pod.Age(),
				pod.App(),
				pod.PodIP(),
				pod.Node(),
			})
		}
	} else {
		content = [][]string{{"NAME", "READY", "STATUS", "RESTARTS", "AGE", "APP"}}
		for _, pod := range pp.podSlice {
			content = append(content, []string{
				pod.Name(),
				pod.Ready(),
				pod.PodStatus(),
				pod.RestartCount(),
				pod.Age(),
				pod.App(),
			})
		}
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (pp *PodPrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", pp.Action, pp.Resource, pp.ResourceName, pp.err.Message, pp.err.Payload)
}

func (pp *PodPrinter) printAction() error {
	fmt.Printf("%s %s %s\n", pp.Resource, pp.ResourceName, actionMap[pp.Action])
	return nil
}

func (pp *PodPrinter) printJSON() error {
	var j []byte
	var err error
	if len(pp.podSlice) == 1 {
		j, err = json.Marshal(pp.podSlice[0])
	} else {
		l := api.PodList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      pp.podSlice,
		}
		j, err = json.Marshal(l)
	}
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (pp *PodPrinter) printYAML() error {
	var y []byte
	var err error
	if len(pp.podSlice) == 1 {
		y, err = yaml.Marshal(&pp.podSlice[0])
	} else {
		l := api.PodList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      pp.podSlice,
		}
		y, err = yaml.Marshal(&l)
	}
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (pp *PodPrinter) applyFilter() *PodPrinter {
	if pp.Filter == EmptyResource {
		return pp
	}
	// Filter for Legacy app
	if pp.Filter == Legacy {
		ret := pp.podSlice[:0]
		for _, pod := range pp.podSlice {
			if _, ok := pod.ObjectMeta.Label[ConsoleAppLabel]; !ok {
				ret = append(ret, pod)
			}
		}
		pp.podSlice = ret
		return pp
	}
	// Filter by appName
	ret := pp.podSlice[:0]
	for _, pod := range pp.podSlice {
		if pod.ObjectMeta.Label[ConsoleAppLabel] == pp.Filter {
			ret = append(ret, pod)
		}
	}
	pp.podSlice = ret
	return pp
}

func (pp *PodPrinter) filterByName() *PodPrinter {
	if pp.ResourceNames == nil {
		return pp
	}
	m := make(map[string]bool)
	for _, rn := range pp.ResourceNames {
		m[rn] = true
	}
	ret := pp.podSlice[:0]
	for _, s := range pp.podSlice {
		if m[s.Name()] {
			ret = append(ret, s)
		}
	}
	pp.podSlice = ret
	return pp
}
