package console

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console deployment printer =========================

// DeploymentPrinter struct to print
type DeploymentPrinter struct {
	PrinterConfig
	err             ErrorCodeResponse
	deploymentSlice []api.Deployment
}

// Print default entrance for printer
func (dp *DeploymentPrinter) Print() error {
	if !common.IsSuccessHTTPCode(dp.HTTPCode) {
		return dp.printError()
	}
	if dp.Action != Get {
		return dp.printAction()
	}
	if dp.OutputFormat == JSONFormat {
		return dp.filterByName().applyFilter().sort().printJSON()
	} else if dp.OutputFormat == YamlFormat {
		return dp.filterByName().applyFilter().sort().printYAML()
	}
	return dp.filterByName().applyFilter().sort().printName()
}

func (dp *DeploymentPrinter) printName() error {
	if len(dp.deploymentSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	var content [][]string
	if dp.OutputFormat == WideFormat {
		content = [][]string{{"NAME", "APP", "DESIRED", "CURRENT", "UP-TO-DATE", "AVAILABLE", "AGE", "CONTAINER(S)", "IMAGE(S)"}}
		for _, deployment := range dp.deploymentSlice {
			content = append(content, []string{
				deployment.Name(),
				deployment.App(),
				deployment.Desired(),
				deployment.Current(),
				deployment.UpToDate(),
				deployment.Available(),
				deployment.Age(),
				deployment.Containers(),
				deployment.Image(),
			})
		}
	} else {
		content = [][]string{{"NAME", "APP", "DESIRED", "CURRENT", "UP-TO-DATE", "AVAILABLE", "AGE"}}
		for _, deployment := range dp.deploymentSlice {
			content = append(content, []string{
				deployment.Name(),
				deployment.App(),
				deployment.Desired(),
				deployment.Current(),
				deployment.UpToDate(),
				deployment.Available(),
				deployment.Age(),
			})
		}
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (dp *DeploymentPrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", dp.Action, dp.Resource, dp.ResourceName, dp.err.Message, dp.err.Payload)
}

func (dp *DeploymentPrinter) printAction() error {
	fmt.Printf("%s %s %s\n", dp.Resource, dp.ResourceName, actionMap[dp.Action])
	return nil
}

func (dp *DeploymentPrinter) printJSON() error {
	var j []byte
	var err error
	if len(dp.deploymentSlice) == 1 {
		j, err = json.Marshal(dp.deploymentSlice[0])
	} else {
		l := api.DeploymentList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      dp.deploymentSlice,
		}
		j, err = json.Marshal(l)
	}
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (dp *DeploymentPrinter) printYAML() error {
	var y []byte
	var err error
	if len(dp.deploymentSlice) == 1 {
		y, err = yaml.Marshal(&dp.deploymentSlice[0])
	} else {
		l := api.DeploymentList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      dp.deploymentSlice,
		}
		y, err = yaml.Marshal(l)
	}
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (dp *DeploymentPrinter) applyFilter() *DeploymentPrinter {
	if dp.Filter == EmptyResource {
		return dp
	}
	// Filter Legacy apps
	if dp.Filter == Legacy {
		// Filter by appName
		ret := dp.deploymentSlice[:0]
		for _, deployment := range dp.deploymentSlice {
			if _, ok := deployment.ObjectMeta.Label[ConsoleAppLabel]; !ok {
				ret = append(ret, deployment)
			}
		}
		dp.deploymentSlice = ret
		return dp
	}
	// Filter by appName
	ret := dp.deploymentSlice[:0]
	for _, deployment := range dp.deploymentSlice {
		if deployment.ObjectMeta.Label[ConsoleAppLabel] == dp.Filter {
			ret = append(ret, deployment)
		}
	}
	dp.deploymentSlice = ret
	return dp
}

func (dp *DeploymentPrinter) sort() *DeploymentPrinter {
	sort.Slice(dp.deploymentSlice, func(i int, j int) bool {
		return dp.deploymentSlice[i].Name() < dp.deploymentSlice[j].Name()
	})
	return dp
}

func (dp *DeploymentPrinter) filterByName() *DeploymentPrinter {
	if dp.ResourceNames == nil {
		return dp
	}
	m := make(map[string]bool)
	for _, rn := range dp.ResourceNames {
		m[rn] = true
	}
	ret := dp.deploymentSlice[:0]
	for _, s := range dp.deploymentSlice {
		if m[s.Name()] {
			ret = append(ret, s)
		}
	}
	dp.deploymentSlice = ret
	return dp
}
