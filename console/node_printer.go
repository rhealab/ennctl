package console

import (
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console namespace printer =========================

// NodePrinter struct to print
type NodePrinter struct {
	PrinterConfig
	err       ErrorCodeResponse
	nodeSlice []NodeResponse
}

// Print default entrance for printer
func (np *NodePrinter) Print() error {
	if !common.IsSuccessHTTPCode(np.HTTPCode) {
		return np.printError()
	}
	if np.OutputFormat == JSONFormat {
		return np.truncate().filterByName().printJSON()
	} else if np.OutputFormat == YamlFormat {
		return np.truncate().filterByName().printYAML()
	}
	return np.truncate().filterByName().printName()
}

func (np *NodePrinter) printName() error {
	if len(np.nodeSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	sort.Slice(np.nodeSlice, func(i int, j int) bool {
		return np.nodeSlice[i].Name < np.nodeSlice[j].Name
	})
	content := [][]string{{"NAME", "STATUS", "ROLE", "OS", "POD(#)", "AGE", "Kubelet"}}
	for _, node := range np.nodeSlice {
		content = append(content, []string{
			node.getName(),
			node.getStatus(),
			node.getRole(),
			node.getOS(),
			node.getPodNumber(),
			node.getAge(),
			node.getVersion(),
		})
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (np *NodePrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", np.Action, np.Resource, np.ResourceName, np.err.Message, np.err.Payload)
}

func (np *NodePrinter) printJSON() error {
	j, err := json.Marshal(np.nodeSlice)
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (np *NodePrinter) printYAML() error {
	y, err := yaml.Marshal(&np.nodeSlice)
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (np *NodePrinter) truncate() *NodePrinter {
	ret := np.nodeSlice[:0]
	for _, node := range np.nodeSlice {
		// truncate cpu counts to x.yy style by issue https://10.19.248.12:30888/cc/ennctl/issues/66
		node.CPULimits = truncateFloat64(node.CPULimits)
		node.CPURequests = truncateFloat64(node.CPURequests)
		node.CPUSum = truncateFloat64(node.CPUSum)
		ret = append(ret, node)
	}
	np.nodeSlice = ret
	return np
}

func (np *NodePrinter) filterByName() *NodePrinter {
	if np.ResourceNames == nil {
		return np
	}
	m := make(map[string]bool)
	for _, rn := range np.ResourceNames {
		m[rn] = true
	}
	ret := np.nodeSlice[:0]
	for _, n := range np.nodeSlice {
		if m[n.getName()] {
			ret = append(ret, n)
		}
	}
	np.nodeSlice = ret
	return np
}
