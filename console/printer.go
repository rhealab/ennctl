package console

import (
	"bytes"
	"cc/ennctl/api"
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"strings"
)

// PrinterConfig for printer configs
type PrinterConfig struct {
	HTTPCode      int
	Action        string
	Resource      string
	ResourceName  string
	ResourceNames []string
	Filter        string
	OutputFormat  string
	RawResponse   []byte
}

// AppPrinter init app printer
func (cfg PrinterConfig) AppPrinter() (*AppPrinter, error) {
	ap := &AppPrinter{
		PrinterConfig: cfg,
	}
	ap.Resource = App
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		ap.err = errorResponse
		return ap, nil
	}
	if cfg.Action != Get {
		return ap, nil
	}
	var ars []AppResponse
	err := json.Unmarshal(cfg.RawResponse, &ars)
	if err != nil {
		return nil, err
	}
	ap.appSlice = ars
	return ap, nil
}

// DeploymentPrinter initialize Deployment printer
func (cfg PrinterConfig) DeploymentPrinter() (*DeploymentPrinter, error) {
	dp := &DeploymentPrinter{
		PrinterConfig: cfg,
	}
	dp.Resource = Deployment
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		dp.err = errorResponse
		return dp, nil
	}
	if cfg.Action != Get {
		return dp, nil
	}
	var drs []api.Deployment
	err := json.Unmarshal(cfg.RawResponse, &drs)
	if err != nil {
		return nil, err
	}
	dp.deploymentSlice = drs
	return dp, nil
}

// ServicePrinter  initialize StatefulSet printer
func (cfg PrinterConfig) ServicePrinter() (*ServicePrinter, error) {
	sp := &ServicePrinter{
		PrinterConfig: cfg,
	}
	sp.Resource = Service
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		sp.err = errorResponse
		return sp, nil
	}
	if cfg.Action != Get {
		return sp, nil
	}
	var srs []api.Service
	err := json.Unmarshal(cfg.RawResponse, &srs)
	if err != nil {
		return nil, err
	}
	sp.serviceSlice = srs
	return sp, nil
}

// StatefulSetPrinter  used to initialize StatefulSet printer
func (cfg PrinterConfig) StatefulSetPrinter() (*StatefulSetPrinter, error) {
	sp := &StatefulSetPrinter{
		PrinterConfig: cfg,
	}
	sp.Resource = StatefulSet
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		sp.err = errorResponse
		return sp, nil
	}
	if cfg.Action != Get {
		return sp, nil
	}
	var srs []api.StatefulSet
	err := json.Unmarshal(cfg.RawResponse, &srs)
	if err != nil {
		return nil, err
	}
	sp.statefulSetSlice = srs
	return sp, nil
}

// StoragePrinter used to initialize Deployment printer
func (cfg PrinterConfig) StoragePrinter() (*StoragePrinter, error) {
	sp := &StoragePrinter{
		PrinterConfig: cfg,
	}
	sp.Resource = Storage
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		sp.err = errorResponse
		return sp, nil
	}
	if cfg.Action != Get {
		return sp, nil
	}
	var srs []StorageResponse
	err := json.Unmarshal(cfg.RawResponse, &srs)
	if err != nil {
		return nil, err
	}
	sp.storageSlice = srs
	return sp, nil
}

// UserPrinter function is used to initialize App printer
func (cfg PrinterConfig) UserPrinter() (*UserPrinter, error) {
	up := &UserPrinter{
		PrinterConfig: cfg,
	}
	up.Resource = User
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		up.err = errorResponse
		return up, nil
	}
	if cfg.Action != Get {
		var userUpdateResonse UserUpdateResponse
		err := json.Unmarshal(up.RawResponse, &userUpdateResonse)
		if err != nil {
			return nil, err
		}
		up.updateResponse = userUpdateResonse
		return up, nil
	}
	var urs []UserResponse
	err := json.Unmarshal(cfg.RawResponse, &urs)
	if err != nil {
		return nil, err
	}
	up.userSlice = urs
	return up, nil
}

// PodPrinter function is used to initialize StatefulSet printer
func (cfg PrinterConfig) PodPrinter() (*PodPrinter, error) {
	pp := &PodPrinter{
		PrinterConfig: cfg,
	}
	pp.Resource = Pod
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		pp.err = errorResponse
		return pp, nil
	}
	if cfg.Action != Get {
		return pp, nil
	}
	var prs []api.Pod
	err := json.Unmarshal(cfg.RawResponse, &prs)
	if err != nil {
		return nil, err
	}
	pp.podSlice = prs
	return pp, nil
}

// NodePrinter function is used to initialize Namespace printer
func (cfg PrinterConfig) NodePrinter() (*NodePrinter, error) {
	np := &NodePrinter{
		PrinterConfig: cfg,
	}
	np.Resource = Node
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		np.err = errorResponse
		return np, nil
	}
	if cfg.Action != Get {
		return np, nil
	}
	var nodeResponses []NodeResponse
	err := json.Unmarshal(cfg.RawResponse, &nodeResponses)
	if err != nil {
		return nil, err
	}
	np.nodeSlice = nodeResponses
	return np, nil
}

// LimitRangePrinter function is used to initialize Deployment printer
func (cfg PrinterConfig) LimitRangePrinter() (*LimitRangePrinter, error) {
	lmp := &LimitRangePrinter{
		PrinterConfig: cfg,
	}
	lmp.Resource = LimitRange
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		lmp.err = errorResponse
		return lmp, nil
	}
	if cfg.Action != Get {
		return lmp, nil
	}
	var lmcr LimitRangeCreateRequest
	err := json.Unmarshal(cfg.RawResponse, &lmcr)
	if err != nil {
		return nil, err
	}
	lmp.limitRange = lmcr
	return lmp, nil
}

// LogPrinter function is used to initialize Namespace printer
func (cfg PrinterConfig) LogPrinter() (*LogPrinter, error) {
	lp := &LogPrinter{
		PrinterConfig: cfg,
	}
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		lp.err = errorResponse
	}
	return lp, nil
}

// NamespacePrinter function is used to initialize Namespace printer
func (cfg PrinterConfig) NamespacePrinter() (*NamespacePrinter, error) {
	np := &NamespacePrinter{
		PrinterConfig: cfg,
	}
	np.Resource = Namespace
	if !common.IsSuccessHTTPCode(cfg.HTTPCode) {
		var errorResponse ErrorCodeResponse
		err := json.Unmarshal(cfg.RawResponse, &errorResponse)
		if err != nil {
			return nil, err
		}
		np.err = errorResponse
		return np, nil
	}
	if cfg.Action != Get {
		return np, nil
	}
	var nrs []NamespaceResponse
	err := json.Unmarshal(cfg.RawResponse, &nrs)
	if err != nil {
		return nil, err
	}
	np.namespaceSlice = nrs
	return np, nil
}

func printerJSON(src []byte) string {
	var prettyJSON bytes.Buffer
	json.Indent(&prettyJSON, src, "", "    ")
	return string(prettyJSON.Bytes())
}

func dispJSON(src []byte) {
	fmt.Println(printerJSON(src))
}

func dispYAML(src []byte) {
	fmt.Println(string(src))
}

func printBeautyMatrix(matrix [][]string) string {
	height := len(matrix)
	width := len(matrix[0])
	beautyContent := make([][]string, height)
	for i := 0; i < height; i++ {
		beautyContent[i] = make([]string, width)
	}

	for i := 0; i < width; i++ {
		maxLen := 0
		for j := 0; j < height; j++ {
			if maxLen < len(matrix[j][i]) {
				maxLen = len(matrix[j][i])
			}
		}

		for j := 0; j < height; j++ {
			oldStr := matrix[j][i]
			beautyContent[j][i] = oldStr + getBlankString(maxLen+3-len(oldStr))
		}
	}

	str := ""
	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			str += beautyContent[i][j]
		}
		if i != (height - 1) {
			str += "\n"
		}
	}

	if strings.HasSuffix(str, "\n") {
		str = str[0 : len(str)-1]
	}
	return str
}

func getBlankString(length int) string {
	str := ""
	for i := 0; i < length; i++ {
		str += " "
	}
	return str
}
