package console

import (
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console app printer =========================

// AppPrinter for app prineter, inheritate from BasePrinter.
type AppPrinter struct {
	PrinterConfig
	err      ErrorCodeResponse
	appSlice []AppResponse
	Filename string
}

// Print is the default print method for app printer
func (ap *AppPrinter) Print() error {
	if !common.IsSuccessHTTPCode(ap.HTTPCode) {
		return ap.printError()
	}
	if ap.Action != Get {
		return ap.printAction()
	}
	if ap.OutputFormat == JSONFormat {
		return ap.filterByName().printJSON()
	} else if ap.OutputFormat == YamlFormat {
		return ap.filterByName().printYAML()
	}
	return ap.filterByName().printName()
}

func (ap *AppPrinter) printYAML() error {
	ret, err := yaml.Marshal(&ap.appSlice)
	if err != nil {
		return err
	}
	dispYAML(ret)
	return nil
}

func (ap *AppPrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", ap.Action, ap.Resource, ap.ResourceName, ap.err.Message, ap.err.Payload)
}

func (ap *AppPrinter) printAction() error {
	if ap.Action == Export {
		fmt.Printf("%s %s %s to file %s\n", ap.Resource, ap.ResourceName, actionMap[ap.Action], ap.Filename)
		return nil
	}
	if ap.Action == Import {
		fmt.Printf("%s in file <%s> %s\n", ap.Resource, ap.Filename, actionMap[ap.Action])
		return nil
	}
	fmt.Printf("%s %s %s\n", ap.Resource, ap.ResourceName, actionMap[ap.Action])
	return nil
}

func (ap *AppPrinter) printJSON() error {
	j, err := json.Marshal(ap.appSlice)
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (ap *AppPrinter) filterByName() *AppPrinter {
	if ap.ResourceNames == nil {
		return ap
	}
	m := make(map[string]bool)
	for _, rn := range ap.ResourceNames {
		m[rn] = true
	}
	ret := ap.appSlice[:0]
	for _, a := range ap.appSlice {
		if m[a.getName()] {
			ret = append(ret, a)
		}
	}
	ap.appSlice = ret
	return ap
}

func (ap *AppPrinter) printName() error {
	if len(ap.appSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	sort.Slice(ap.appSlice, func(i int, j int) bool {
		return ap.appSlice[i].Name < ap.appSlice[j].Name
	})
	content := [][]string{{"NAME", "NAMESPACE", "TYPE", "STATUS", "CREATOR", "AGE"}}
	for _, app := range ap.appSlice {
		content = append(content, []string{
			app.getName(),
			app.getNamespace(),
			app.getType(),
			app.getStatus(),
			app.getCreatedBy(),
			app.getAge(),
			app.getAge(),
		})
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}
