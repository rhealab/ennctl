package console

import (
	"cc/ennctl/common"
	"fmt"
)

//========================= console log printer =========================

// LogPrinter struct to print
type LogPrinter struct {
	PrinterConfig
	err ErrorCodeResponse
}

// Print default entrance for printer
func (lp *LogPrinter) Print() error {
	if !common.IsSuccessHTTPCode(lp.HTTPCode) {
		return lp.printError()
	}
	return lp.printLog()
}

func (lp *LogPrinter) printLog() error {
	ret := lp.RawResponse
	if len(ret) > 1 {
		ret = ret[:len(ret)-1]
	}
	fmt.Println(string(ret))
	return nil
}

func (lp *LogPrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s", lp.Action, lp.Resource, lp.ResourceName, lp.err.Message)
}
