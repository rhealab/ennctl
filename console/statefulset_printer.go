package console

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console statefulset printer =========================

// StatefulSetPrinter struct to print
type StatefulSetPrinter struct {
	PrinterConfig
	err              ErrorCodeResponse
	statefulSetSlice []api.StatefulSet
}

// Print default entrance for printer
func (sp *StatefulSetPrinter) Print() error {
	if !common.IsSuccessHTTPCode(sp.HTTPCode) {
		return sp.printError()
	}
	if sp.Action != Get {
		return sp.printAction()
	}
	if sp.OutputFormat == JSONFormat {
		return sp.filterByName().applyFilter().printJSON()
	} else if sp.OutputFormat == YamlFormat {
		return sp.filterByName().applyFilter().printYAML()
	}
	return sp.filterByName().applyFilter().printName()
}

// Implement printerInterface
func (sp *StatefulSetPrinter) printName() error {
	if len(sp.statefulSetSlice) == 0 {
		fmt.Println(NoResourceFound)
		return nil
	}
	sort.Slice(sp.statefulSetSlice, func(i int, j int) bool {
		return sp.statefulSetSlice[i].Name() < sp.statefulSetSlice[j].Name()
	})
	var content [][]string
	if sp.OutputFormat == WideFormat {
		content = [][]string{{"NAME", "APP", "DESIRED", "CURRENT", "AGE", "CONTAINER(S)", "IMAGE(S)"}}
		for _, statefulSet := range sp.statefulSetSlice {
			content = append(content, []string{
				statefulSet.Name(),
				statefulSet.App(),
				statefulSet.Desired(),
				statefulSet.Current(),
				statefulSet.Age(),
				statefulSet.Containers(),
				statefulSet.Image(),
			})
		}
	} else {
		content = [][]string{{"NAME", "APP", "DESIRED", "CURRENT", "AGE"}}
		for _, statefulSet := range sp.statefulSetSlice {
			content = append(content, []string{
				statefulSet.Name(),
				statefulSet.App(),
				statefulSet.Desired(),
				statefulSet.Current(),
				statefulSet.Age(),
			})
		}
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (sp *StatefulSetPrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", sp.Action, sp.Resource, sp.ResourceName, sp.err.Message, sp.err.Payload)
}

func (sp *StatefulSetPrinter) printAction() error {
	fmt.Printf("%s %s %s\n", sp.Resource, sp.ResourceName, actionMap[sp.Action])
	return nil
}

func (sp *StatefulSetPrinter) printJSON() error {
	var j []byte
	var err error
	if len(sp.statefulSetSlice) == 1 {
		j, err = json.Marshal(sp.statefulSetSlice[0])
	} else {
		l := api.StatefulSetList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      sp.statefulSetSlice,
		}
		j, err = json.Marshal(l)
	}
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (sp *StatefulSetPrinter) printYAML() error {
	var y []byte
	var err error
	if len(sp.statefulSetSlice) == 1 {
		y, err = yaml.Marshal(&sp.statefulSetSlice[0])
	} else {
		l := api.StatefulSetList{
			APIVersion: api.ListVersion,
			Kind:       api.ListKind,
			Items:      sp.statefulSetSlice,
		}
		y, err = yaml.Marshal(&l)
	}
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}

func (sp *StatefulSetPrinter) applyFilter() *StatefulSetPrinter {
	if sp.Filter == EmptyResource {
		return sp
	}
	// Filter by Legacy
	if sp.Filter == Legacy {
		ret := sp.statefulSetSlice[:0]
		for _, statefulSet := range sp.statefulSetSlice {
			if _, ok := statefulSet.ObjectMeta.Label[ConsoleAppLabel]; !ok {
				ret = append(ret, statefulSet)
			}
		}
		sp.statefulSetSlice = ret
		return sp
	}
	// Filter by appName
	ret := sp.statefulSetSlice[:0]
	for _, statefulSet := range sp.statefulSetSlice {
		if statefulSet.ObjectMeta.Label[ConsoleAppLabel] == sp.Filter {
			ret = append(ret, statefulSet)
		}
	}
	sp.statefulSetSlice = ret
	return sp
}

func (sp *StatefulSetPrinter) filterByName() *StatefulSetPrinter {
	if sp.ResourceNames == nil {
		return sp
	}
	m := make(map[string]bool)
	for _, rn := range sp.ResourceNames {
		m[rn] = true
	}
	ret := sp.statefulSetSlice[:0]
	for _, s := range sp.statefulSetSlice {
		if m[s.Name()] {
			ret = append(ret, s)
		}
	}
	sp.statefulSetSlice = ret
	return sp
}
