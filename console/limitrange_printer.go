package console

import (
	"cc/ennctl/common"
	"encoding/json"
	"fmt"

	"github.com/ghodss/yaml"
)

//========================= console limit range printer =========================

// LimitRangePrinter struct to print
type LimitRangePrinter struct {
	PrinterConfig
	err        ErrorCodeResponse
	limitRange LimitRangeCreateRequest
}

// Print default entrance for printer
func (lmp *LimitRangePrinter) Print() error {
	if !common.IsSuccessHTTPCode(lmp.HTTPCode) {
		return lmp.printError()
	}
	if lmp.Action != Get {
		return lmp.printAction()
	}
	if lmp.OutputFormat == JSONFormat {
		return lmp.printJSON()
	} else if lmp.OutputFormat == YamlFormat {
		return lmp.printYAML()
	}
	return lmp.printName()
}

func (lmp *LimitRangePrinter) printName() error {
	ret := fmt.Sprintf(
		"Global limit range\nMinimum setting\n\tCPU: %.2f\n\tMemory: %d bytes\nDefault setting\n\tCPU request: %.2f\n\tMemory request: %d bytes\n\tCPU limit: %.2f\n\tMemory limit: %d bytes",
		lmp.limitRange.MinCPU,
		lmp.limitRange.MinMemory,
		lmp.limitRange.DefaultRequestCPU,
		lmp.limitRange.DefaultRequestMemory,
		lmp.limitRange.DefaultLimitCPU,
		lmp.limitRange.DefaultLimitMemory)
	fmt.Println(ret)
	return nil
}

func (lmp *LimitRangePrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s\ndetail: %s\npayload: %v", lmp.Action, lmp.Resource, lmp.err.Message, lmp.err.Payload)
}

func (lmp *LimitRangePrinter) printAction() error {
	fmt.Printf("%s %s\n", lmp.Resource, actionMap[lmp.Action])
	return nil
}

func (lmp *LimitRangePrinter) printJSON() error {
	jsonBytes, err := json.Marshal(lmp.limitRange)
	if err != nil {
		return err
	}
	dispJSON(jsonBytes)
	return nil
}

func (lmp *LimitRangePrinter) printYAML() error {
	y, err := yaml.Marshal(&lmp.limitRange)
	if err != nil {
		return err
	}
	dispYAML(y)
	return nil
}
