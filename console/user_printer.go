package console

import (
	"cc/ennctl/common"
	"encoding/json"
	"errors"
	"fmt"
	"sort"

	"github.com/ghodss/yaml"
)

//========================= console user printer =========================

// UserPrinter struct to print
type UserPrinter struct {
	PrinterConfig
	err            ErrorCodeResponse
	userSlice      []UserResponse
	updateResponse UserUpdateResponse
}

// Print is the default print method for user printer
func (up *UserPrinter) Print() error {
	if !common.IsSuccessHTTPCode(up.HTTPCode) {
		return up.printError()
	}
	switch up.Action {
	case Get:
		if up.OutputFormat == JSONFormat {
			return up.filterByName().applyFilter().printJSON()
		} else if up.OutputFormat == YamlFormat {
			return up.filterByName().applyFilter().printYAML()
		}
		return up.filterByName().applyFilter().printName()
	case Create:
		return up.updateResponse.getCreateResponse()
	case Delete:
		return up.updateResponse.getDeleteResponse()
	default:
		return errors.New(ToDo)
	}
}

// Implement printerInterface
func (up *UserPrinter) printName() error {
	if len(up.userSlice) == 0 {
		return errors.New(NoResourceFound)
	}
	// Sort by userId
	sort.Slice(up.userSlice, func(i int, j int) bool {
		return up.userSlice[i].ID < up.userSlice[j].ID
	})
	content := [][]string{{"USERID", "ROLE", "STATUS", "ASSIGNEDBY", "AGE"}}
	for _, user := range up.userSlice {
		content = append(content, []string{
			user.ID,
			user.getRole(),
			user.getStatus(),
			user.getAssignedBy(),
			user.getAge(),
		})
	}
	fmt.Println(printBeautyMatrix(content))
	return nil
}

func (up *UserPrinter) printJSON() error {
	j, err := json.Marshal(up.userSlice)
	if err != nil {
		return err
	}
	dispJSON(j)
	return nil
}

func (up *UserPrinter) applyFilter() *UserPrinter {
	// Filter by roleName
	ret := up.userSlice[:0]
	if up.Filter == NsAdmin {
		for _, user := range up.userSlice {
			if user.Rolename == "ns_admin" {
				ret = append(ret, user)
			}
		}
		up.userSlice = ret
	} else if up.Filter == Developer {
		for _, user := range up.userSlice {
			if user.Rolename == "developer" {
				ret = append(ret, user)
			}
		}
		up.userSlice = ret
	}
	return up
}

func (up *UserPrinter) filterByName() *UserPrinter {
	if up.ResourceNames == nil {
		return up
	}
	m := make(map[string]bool)
	for _, rn := range up.ResourceNames {
		m[rn] = true
	}
	ret := up.userSlice[:0]
	for _, u := range up.userSlice {
		if m[u.getName()] {
			ret = append(ret, u)
		}
	}
	up.userSlice = ret
	return up
}

func (up *UserPrinter) printYAML() error {
	ret, err := yaml.Marshal(&up.userSlice)
	if err != nil {
		return err
	}
	dispYAML(ret)
	return nil
}

func (up *UserPrinter) printError() error {
	return fmt.Errorf("error: fail to %s %s %s\ndetail: %s\npayload: %v", up.Action, up.Resource, up.ResourceName, up.err.Message, up.err.Payload)
}

func (up *UserPrinter) printAction() error {
	fmt.Printf("%s %s %s", up.Resource, up.ResourceName, actionMap[up.Action])
	return nil
}
