package console

import (
	"cc/ennctl/common"
	"fmt"
)

// StorageResponse is the structure of get storage API
type StorageResponse struct {
	ID                    int32         `json:"id"`
	Name                  string        `json:"storageName"`
	Namespace             string        `json:"namespaceName"`
	Type                  string        `json:"storageType"`
	AccessMode            string        `json:"accessMode"`
	AmountBytes           int64         `json:"amountBytes"`
	Persisted             bool          `json:"persisted"`
	Unshared              bool          `json:"unshared"`
	ReadOnly              bool          `json:"readOnly"`
	Status                string        `json:"status"`
	Mounted               bool          `json:"mounted"`
	PodList               []string      `json:"pods"`
	UsedAmountBytes       int64         `json:"usedAmountBytes"`
	ModifiedOn            int64         `json:"modifiedOn"`
	ModifiedBy            string        `json:"modifiedBy"`
	CreatedOn             int64         `json:"createdOn"`
	CreatedBy             string        `json:"createdBy"`
	HostPathUsedBytesList []interface{} `json:"hostPathUsedBytesList"`
	RelatedWorkloadList   []WorkLoad    `json:"relatedWorkloadList"`
}

func (sr *StorageResponse) getName() string {
	return sr.Name
}

func (sr *StorageResponse) getApp() string {
	ret := EmptyResource
	for _, wl := range sr.RelatedWorkloadList {
		ret += fmt.Sprintf("%s,", wl.AppName)
	}
	if ret == EmptyResource {
		return None
	}
	return ret[:len(ret)-1]
}

func (sr *StorageResponse) getWorkLoad() string {
	ret := EmptyResource
	for _, wl := range sr.RelatedWorkloadList {
		ret += fmt.Sprintf("%s(%s),", wl.WorkLoadName, wl.WorkLoadType)
	}
	if ret == EmptyResource {
		return None
	}
	return ret[:len(ret)-1]
}

func (sr *StorageResponse) getPod() string {
	ret := EmptyResource
	for _, pod := range sr.PodList {
		ret += fmt.Sprintf("%s,", pod)
	}
	if ret == EmptyResource {
		return None
	}
	return ret[:len(ret)-1]
}

func (sr *StorageResponse) getType() string {
	return sr.Type
}

func (sr *StorageResponse) getAmount() string {
	return common.BytesToString(float64(sr.AmountBytes))
}

func (sr *StorageResponse) getUsedAmount() string {
	return common.BytesToString(float64(sr.UsedAmountBytes))
}

func (sr *StorageResponse) getAge() string {
	return common.TimestampToAge(sr.CreatedOn)
}

func (sr *StorageResponse) getReplica() string {
	if sr.Type != HostPath {
		return "None"
	}
	return fmt.Sprintf("%d", len(sr.HostPathUsedBytesList))
}

func (sr *StorageResponse) getAccessMode() string {
	return accessModeMap[sr.AccessMode]
}

func (sr *StorageResponse) getStatus() string {
	return storageStatusMap[sr.Status]
}

func (sr *StorageResponse) getMountStatus() string {
	if sr.Mounted {
		return "Mounted"
	}
	return "Unmount"
}

// WorkLoad the workload which use this storage
type WorkLoad struct {
	AppName       string `json:"appName" yaml:"appName"`
	StorageName   string `json:"storageName" yaml:"storageName"`
	WorkLoadName  string `json:"workloadName" yaml:"workloadName"`
	WorkLoadType  string `json:"workloadType" yaml:"workloadType"`
	NamespaceName string `json:"namespaceName" yaml:"namespaceName" `
}
