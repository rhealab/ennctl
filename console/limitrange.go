package console

import (
	"encoding/json"
)

//========================= console limit range request =========================

// LimitRangeCreateRequest is used to construct post body in app creation API
type LimitRangeCreateRequest struct {
	MinCPU               float32 `yaml:"minCpu" json:"minCpu"`
	MinMemory            int64   `yaml:"minMemory" json:"minMemory"`
	DefaultRequestCPU    float32 `yaml:"defaultRequestCpu" json:"defaultRequestCpu"`
	DefaultRequestMemory int64   `yaml:"defaultRequestMemory" json:"defaultRequestMemory"`
	DefaultLimitCPU      float32 `yaml:"defaultLimitCpu" json:"defaultLimitCpu"`
	DefaultLimitMemory   int64   `yaml:"defaultLimitMemory" json:"defaultLimitMemory"`
}

func (lm LimitRangeCreateRequest) toJSON() ([]byte, error) {
	return json.Marshal(lm)
}

// GetJSONBytes toJSON
func (lm LimitRangeCreateRequest) GetJSONBytes() ([]byte, error) {
	return lm.toJSON()
}
