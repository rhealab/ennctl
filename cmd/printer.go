package cmd

import (
	"bytes"
	"cc/ennctl/common"
	"encoding/json"
	"fmt"
	"log"
)

func printerJSON(src []byte) string {
	var prettyJSON bytes.Buffer
	json.Indent(&prettyJSON, src, "", "    ")
	return string(prettyJSON.Bytes())
}

func dispJSON(src []byte) {
	fmt.Println(printerJSON(src))
}

func dispBytes(src []byte) {
	fmt.Println(string(src))
}

func logErr(err error) {
	fmt.Println(err.Error())
	common.Logger.Log("error", err)
	log.Fatal("ERROR:", err)
}
