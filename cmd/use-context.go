package cmd

import (
	"cc/ennctl/common"
	"fmt"

	"github.com/spf13/cobra"
)

// ======================= useContextCmd =========================
var useContextCmd = &cobra.Command{
	Use:   "use-context",
	Short: "use a specific context setting",
	Run: func(cmd *cobra.Command, args []string) {
		useContext(cmd, args)
	},
}

func init() {
	configCmd.AddCommand(useContextCmd)
	configCmd.AddCommand(currentContextCmd)
	configCmd.AddCommand(viewContextCmd)
	configCmd.AddCommand(loadContextCmd)
}

func useContext(cmd *cobra.Command, args []string) {
	ec, err := common.LoadConfig()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	ec.CurrentContext = args[0]
	if err = ec.EnnConfigMap().Valid(); err != nil {
		fmt.Println(err.Error())
		return
	}
	err = ec.RefreshEnnConfig()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Printf("Switched to context \"%s\".\n", ec.CurrentContext)
}

// ======================= currentContextCmd =========================
var currentContextCmd = &cobra.Command{
	Use:   "current-context",
	Short: "Displays the current-context",
	Run: func(cmd *cobra.Command, args []string) {
		currentContext(cmd, args)
	},
}

func currentContext(cmd *cobra.Command, args []string) {
	ec, err := common.LoadConfig()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(ec.CurrentContext)
}

// ======================= viewCmd =========================
var viewContextCmd = &cobra.Command{
	Use:   "view",
	Short: "Displays the enn config",
	Run: func(cmd *cobra.Command, args []string) {
		viewContext(cmd, args)
	},
}

func viewContext(cmd *cobra.Command, args []string) {
	ec, err := common.LoadConfig()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	ecBytes, err := ec.ToYAML()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(string(ecBytes[:len(ecBytes)-1]))
}

// ======================= viewCmd =========================
var loadContextCmd = &cobra.Command{
	Use:   "load",
	Short: "Load changes from enn config",
	Run: func(cmd *cobra.Command, args []string) {
		loadContext(cmd, args)
	},
}

func loadContext(cmd *cobra.Command, args []string) {
	common.LoadContext()
	fmt.Println("Enn config reloaded.")
}
