package cmd

import (
	"testing"
)

func TestCmdGetStateful(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "ss"})
	RootCmd.Execute()
}

func TestFuncGetStatefulSet(t *testing.T) {
	initContext()
	getStatefulSets(nil)
}

func TestCreateAndDeleteStatefulSetFromFile(t *testing.T) {
	initContext()
	createFromFile("../resource/statefulset.yaml")
	replaceFromFile("../resource/statefulset.yaml")
	deleteService("ss")
	deleteFromFile("../resource/statefulset.yaml")
}
