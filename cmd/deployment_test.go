package cmd

import (
	"testing"
)

func TestGetDeploy(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "deploy"})
	RootCmd.Execute()
}

func TestFuncGetDeployment(t *testing.T) {
	initContext()
	getDeployments(nil)
}

func TestCreateAndDeleteDeploymentFromFile(t *testing.T) {
	initContext()
	createFromFile("../resource/deployment.yaml")
	ReplicaCount = 2
	scaleDeployment("mysql")
	replaceFromFile("../resource/deployment.yaml")
	deleteService("mysql")
	deleteFromFile("../resource/deployment.yaml")
}
