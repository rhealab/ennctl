package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var logCmd = &cobra.Command{
	Use:                "logs",
	Short:              "Get log of container in a pod",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		routeToKubectl(cmd.Name(), args)
	},
}

func init() {
	logCmd.PersistentFlags().StringVarP(&ContainerName, "container", "c", "", "container name")
	RootCmd.AddCommand(logCmd)
}

func runLog(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	return logCommand(args)
}

func logCommand(args []string) error {
	if len(args) == 0 {
		return errors.New(api.PodNameIsRequiredError)
	}
	podName := args[0]
	var containerName string
	if ContainerName == EmptyResource {
		p, err := getPodByName(podName, NamespaceName)
		if err != nil {
			return err
		}
		if p.ContainerSum() == 0 {
			return errors.New(api.PodHaveNoContainerError)
		}
		containerName = p.DefaultContainerName()
	} else {
		containerName = ContainerName
	}
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/pods/%s/container/%s/logs/origin", common.AppCtx.GatewayURL, NamespaceName, podName, containerName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Get,
		Resource:     "log",
		ResourceName: podName,
		RawResponse:  res,
	}
	lp, err := cfg.LogPrinter()
	if err != nil {
		return err
	}
	return lp.Print()
}
