package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/utils/client"
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var cpCmd = &cobra.Command{
	Use:   "cp (SRC DEST) [flags]",
	Short: "Copy files from/to container",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run cp cmd
		if err := runCp(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	cpCmd.PersistentFlags().StringVarP(&ContainerName, "container", "c", "", "container name")
	RootCmd.AddCommand(cpCmd)
}

func runCp(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	return cpCommand(args)
}

func cpCommand(args []string) error {
	var dest, src client.FileSpec
	if args == nil || len(args) == 0 {
		return errors.New(api.CpRequirePodError)
	}
	src.Init(args[0])
	dest.Init(args[1])
	if src.PodName != EmptyResource && dest.PodName != EmptyResource {
		return errors.New(api.CpRequirePodError)
	} else if src.PodName != EmptyResource {
		return copyFromPod(src, dest)
	} else if dest.PodName != EmptyResource {
		return copyToPod(src, dest)
	}
	return errors.New(api.CpRequirePodError)
}

func copyFromPod(src client.FileSpec, dest client.FileSpec) error {
	var containerName string
	if ContainerName == EmptyResource {
		p, err := getPodByName(src.PodName, NamespaceName)
		if err != nil {
			return err
		}
		if p.ContainerSum() == 0 {
			return errors.New(api.PodHaveNoContainerError)
		}
		containerName = p.DefaultContainerName()
	} else {
		containerName = ContainerName
	}
	url := fmt.Sprintf("%s/kubernetes/namespaces/%s/pods/%s?container=%s&stdin=true&stdout=true&stderr=false&command=tar&command=cf&command=-&command=-P&command=%s&Authorization=Bearer%s%s", common.AppCtx.TerminalURL, NamespaceName, src.PodName, containerName, src.GetEscapedFile(), "%20", common.AppCtx.Token)
	common.Logger.Log("url", url)
	return client.CopyFromPodClient(url, src, dest)
}

func copyToPod(src client.FileSpec, dest client.FileSpec) error {
	var containerName string
	if ContainerName == EmptyResource {
		p, err := getPodByName(dest.PodName, NamespaceName)
		if err != nil {
			return err
		}
		if p.ContainerSum() == 0 {
			return errors.New(api.PodHaveNoContainerError)
		}
		containerName = p.DefaultContainerName()
	} else {
		containerName = ContainerName
	}
	url := fmt.Sprintf("%s/kubernetes/namespaces/%s/pods/%s?container=%s&stdin=true&stdout=true&stderr=true&command=tar&command=xf&command=-&command=-C&command=%s&Authorization=Bearer%s%s", common.AppCtx.TerminalURL, NamespaceName, dest.PodName, containerName, "%2F", "%20", common.AppCtx.Token)
	common.Logger.Log("url", url)
	return client.CopyToPodClient(url, src, dest)
}
