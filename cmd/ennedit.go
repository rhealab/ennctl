package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var ennEditCmd = &cobra.Command{
	Use:   "ennedit RESOURCE NAME",
	Short: "Edit a console-specific resource from default editor.",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run edit cmd
		if err := runEdit(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	ennEditCmd.PersistentFlags().StringVarP(&AppName, "app", "a", "", "app name")
	ennEditCmd.PersistentFlags().StringVarP(&NamespaceName, "namespace", "n", "", "namespace name")
	RootCmd.AddCommand(ennEditCmd)
}

func runEdit(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	var err error
	var resourceName string
	if len(args) > 1 {
		resourceName = args[1]
	} else {
		resourceName = EmptyResource
	}
	switch args[0] {
	case Storage, Storages:
		err = editStorage(resourceName)
	case Namespace, Namespaces, NamespaceShortAlias:
		err = editNamespace(resourceName)
	case Services, Service, ServiceShortAlias:
		err = editService(resourceName)
	case StatefulSet, StatefulSets, StatefulSetShortAlias:
		err = editStatefulSet(resourceName)
	case Deployment, Deployments, DeploymentShortAlias:
		err = editDeployment(resourceName)
	default:
		err = errors.New(api.EditNotAllowedError)
	}
	return err
}
