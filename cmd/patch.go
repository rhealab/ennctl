package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var patchCmd = &cobra.Command{
	Use:                "patch",
	Short:              "Update field(s) of a resource using strategic merge patch",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		routeToKubectl(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(patchCmd)
}
