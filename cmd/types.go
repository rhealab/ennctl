package cmd

import (
	"strings"
)

// ErrorCodeResponse is the struct used in cc backend response.
type ErrorCodeResponse struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Payload map[string]interface{} `json:"payload"`
}

func (err ErrorCodeResponse) Error() string {
	return err.Message
}

//ArgParser used to manually parse args
type ArgParser struct {
	Command          string
	Resource         string
	Arguments        []string
	ParsedSubCommand []string
	ParsedOptions    []string
}

// RouteToKubectl decide route based on arguments
func (ap *ArgParser) RouteToKubectl() bool {
	switch ap.Command {
	case Create:
		return ap.routeCreateToKubectl()
	case Delete:
		return ap.routeDeleteToKubectl()
	case Replace:
		return ap.routeReplaceToKubectl()
	case Get:
		return ap.routeGetToKubectl()
	case Edit:
		return ap.routeEditToKubectl()
	}
	return false
}

func (ap *ArgParser) routeReplaceToKubectl() bool {
	for _, option := range ap.ParsedOptions {
		if ennFromFileOption[option] {
			return false
		}
	}
	return !ennResources[ap.Resource]
}

func (ap *ArgParser) routeGetToKubectl() bool {
	return !ennResources[ap.Resource]
}

func (ap *ArgParser) routeEditToKubectl() bool {
	return !ennResources[ap.Resource]
}

func (ap *ArgParser) routeCreateToKubectl() bool {
	for _, option := range ap.ParsedOptions {
		if ennFromFileOption[option] {
			return false
		}
	}
	return !ennResources[ap.Resource]
}

func (ap *ArgParser) routeDeleteToKubectl() bool {
	for _, option := range ap.ParsedOptions {
		if ennFromFileOption[option] {
			return false
		}
	}
	// Handle pod as special case since requriement from LiYe to support --force.
	if ap.Resource == Pod || ap.Resource == PodShortAlias || ap.Resource == Pods {
		tmp := ap.ParsedOptions[:0]
		for _, option := range ap.ParsedOptions {
			if !ennDeleteOption[option] {
				tmp = append(tmp, option)
			}
		}
		if len(tmp) > 0 {
			return true
		}
		return false
	}
	return !ennResources[ap.Resource]
}

//init manually parse arguments to options
func (ap *ArgParser) init() *ArgParser {
	var options []string
	var commands []string
	partOfOption := false
	for _, arg := range ap.Arguments {
		if partOfOption && !strings.HasPrefix(arg, "-") {
			// options = append(options, arg)
			partOfOption = false
			continue
		}
		if strings.HasPrefix(arg, "--") {
			options = append(options, arg)
			if strings.Contains(arg, "=") {
				continue
			}
			partOfOption = true
			continue
		}
		if strings.HasPrefix(arg, "-") {
			if len(arg) > 2 {
				options = append(options, arg[0:2])
				continue
			}
			options = append(options, arg)
			partOfOption = true
			continue
		}
		commands = append(commands, arg)
	}
	ap.ParsedSubCommand = commands
	ap.ParsedOptions = options
	if len(commands) > 0 {
		ap.Resource = commands[0]
	} else {
		ap.Resource = EmptyResource
	}
	return ap
}
