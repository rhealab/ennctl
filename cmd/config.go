package cmd

import (
	"github.com/spf13/cobra"
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Manage ennctl related configs",
}

func init() {
	RootCmd.AddCommand(configCmd)
}
