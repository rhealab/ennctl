package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the client version information",
	Run: func(cmd *cobra.Command, args []string) {
		RunVersion()
	},
}

// RunVersion prints the version information
func RunVersion() {
	fmt.Println(VersionString)
}

func init() {
	RootCmd.AddCommand(versionCmd)
}
