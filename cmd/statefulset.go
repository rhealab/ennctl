package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/ghodss/yaml"
)

//========================= console statefulset RESTful functions =========================

func createStatefulSetFromResource(r *api.Resource) error {
	s, err := r.StatefulSet()
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	// no app name
	if AppName == EmptyResource && s.ObjectMeta.App == nil {
		return errors.New(api.AppIsRequiredError)
	}
	// Analyze app name
	if AppName != EmptyResource {
		s.ObjectMeta.App = &AppName
	}
	appName := *s.ObjectMeta.App
	if AppName != EmptyResource {
		appName = AppName
	}
	// @TODO:Workarround, change to nornal change once JINXIN did his work.
	j, err := s.K8sStylize().SliceJSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/batch/k8s_json", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     StatefulSet,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StatefulSetPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func getStatefulSetByName(statefulName string) (*api.StatefulSet, error) {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/statefulsets/%s/origin", common.AppCtx.GatewayURL, NamespaceName, statefulName)
	res, code, _ := common.Exec(false, url, common.GET, nil)
	var s api.StatefulSet
	if code != 200 {
		var errCode ErrorCodeResponse
		json.Unmarshal(res, &errCode)
		return nil, errCode
	}
	json.Unmarshal(res, &s)
	return &s, nil
}

func deleteStatefulSets(statefuls []string) error {
	if statefuls == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, stateful := range statefuls {
		if err := deleteStatefulSet(stateful); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

// deleteStatefulSet function delete StatefulSet
func deleteStatefulSet(statefulName string) error {
	if statefulName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	var url string
	if AppName == EmptyResource {
		stateful, err := getStatefulSetByName(statefulName)
		if err != nil {
			return err
		}
		appName := stateful.App()
		if appName == api.Legacy {
			url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/statefulsets/%s", common.AppCtx.GatewayURL, NamespaceName, statefulName)
		} else {
			url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/statefulsets/%s", common.AppCtx.GatewayURL, NamespaceName, appName, statefulName)
		}
	} else {
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/statefulsets/%s", common.AppCtx.GatewayURL, NamespaceName, AppName, statefulName)
	}
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     StatefulSet,
		ResourceName: statefulName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StatefulSetPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func replaceStatefulSetFromResource(r *api.Resource) error {
	s, err := r.StatefulSet()
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	// no app name
	if AppName == EmptyResource && s.ObjectMeta.App == nil {
		return errors.New(api.AppIsRequiredError)
	}
	// Analyze app name
	if AppName != EmptyResource {
		s.ObjectMeta.App = &AppName
	}
	appName := *s.ObjectMeta.App
	if AppName != EmptyResource {
		appName = AppName
	}
	// @TODO:Workarround, change to nornal change once JINXIN did his work.
	j, err := s.K8sStylize().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/statefulsets/%s", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName, s.ObjectMeta.Name)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Replace,
		Resource:     StatefulSet,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StatefulSetPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

// getStatefulSet function. If serviceName is nil then get service list else get specified name service's info
func getStatefulSets(statefuls []string) error {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/statefulsets/origin_list", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      StatefulSet,
		ResourceNames: statefuls,
		Filter:        AppName,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	sp, err := cfg.StatefulSetPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func editStatefulSet(statefulName string) error {
	if statefulName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	s, err := getStatefulSetByName(statefulName)
	if err != nil {
		return err
	}
	// check app name
	appName := s.App()
	if AppName != EmptyResource && AppName != appName {
		return errors.New(api.AppNameNotMatchError)
	}
	var y, j []byte
	var url string
	if appName == api.Legacy {
		y, err = s.YAML()
	} else {
		y, err = s.ConsoleStylize().YAML()
	}
	if err != nil {
		return err
	}
	yBytes := editBytes(y)
	if compareByteSlice(yBytes, y) {
		fmt.Println(NoChangeIsMade)
		return nil
	}
	s = &api.StatefulSet{}
	yaml.Unmarshal(yBytes, s)
	if err = s.Valid(); err != nil {
		return err
	}
	if appName != api.Legacy {
		j, err = s.K8sStylize().JSON()
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/statefulsets/%s", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName, s.ObjectMeta.Name)
	} else {
		j, err = s.JSON()
		url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/statefulsets", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace)
	}
	if err != nil {
		return err
	}
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Edit,
		Resource:     StatefulSet,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StatefulSetPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

// @TODO once scale statefulset API is ready, switch to that one.
func scaleStatefulSet(statefulName string) error {
	if statefulName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	s, err := getStatefulSetByName(statefulName)
	if err != nil {
		return err
	}
	appName := s.App()
	if AppName != EmptyResource && AppName != appName {
		return errors.New(api.AppNameNotMatchError)
	}
	if ReplicaCount < 0 {
		return errors.New(api.ReplicasMustExistAndBePositiveForScale)
	}
	s.Spec.Replicas = &ReplicaCount
	j, err := s.JSON()
	if err != nil {
		return err
	}
	var url string
	if appName != api.Legacy {
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/statefulsets/%s", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName, statefulName)
	} else {
		url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/statefulsets", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace)
	}

	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Scale,
		Resource:     StatefulSet,
		ResourceName: statefulName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StatefulSetPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}
