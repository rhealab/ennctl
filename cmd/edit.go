package cmd

import "github.com/spf13/cobra"

var editCmd = &cobra.Command{
	Use:                "edit",
	Short:              "Edit a console-specific resource or k8s legacy resource",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		Route(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(editCmd)
}
