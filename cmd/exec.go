package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/utils/client"
	"errors"
	"fmt"
	"net/url"
	"os"

	"github.com/spf13/cobra"
)

var execCmd = &cobra.Command{
	Use:   "exec (POD_NAME COMMAND ...) [flags]",
	Short: "Execute command in container",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run exec cmd
		if err := runExec(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

// Stdin bool flag to enable interacive mode.
var Stdin bool

// Tty bool flag to specify tty
var Tty bool

func init() {
	execCmd.PersistentFlags().StringVarP(&ContainerName, "container", "c", "", "container name")
	execCmd.PersistentFlags().BoolVarP(&Stdin, "stdin", "i", false, "Pass stdin to the container")
	execCmd.PersistentFlags().BoolVarP(&Tty, "tty", "t", false, "Stdin is a TTY")
	RootCmd.AddCommand(execCmd)
}

func runExec(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	return execCommand(args)
}

func execCommand(args []string) error {
	if len(args) < 2 {
		return errors.New(api.PodNameAndCommandAreRequiredError)
	}
	// Extract podName and cmdName from args
	podName := args[0]
	cmds := args[1:]
	cmdName := ""
	for _, item := range cmds {
		cmdName = cmdName + fmt.Sprintf("&command=%s", url.PathEscape(item))
	}
	// generage tty and stdin
	var tty string
	var stdin string
	if Tty {
		tty = "&tty=true"
	} else {
		tty = "&tty=false"
	}
	if Stdin {
		stdin = "&stdin=true"
	} else {
		stdin = "&stdin=false"
	}
	// Get continer name
	var containerName string
	if ContainerName == EmptyResource {
		p, err := getPodByName(podName, NamespaceName)
		if err != nil {
			return err
		}
		if p.ContainerSum() == 0 {
			return errors.New(api.PodHaveNoContainerError)
		}
		containerName = p.DefaultContainerName()
	} else {
		containerName = ContainerName
	}
	token := fmt.Sprintf("Bearer %s", common.AppCtx.Token)
	u := fmt.Sprintf("%s/kubernetes/namespaces/%s/pods/%s?container=%s%s&stdout=true&stderr=true%s%s&Authorization=%s", common.AppCtx.TerminalURL, NamespaceName, podName, containerName, stdin, cmdName, tty, url.PathEscape(token))
	common.Logger.Log("u", u)
	if Tty {
		return client.StartWSClient(u)
	}
	return client.ExecClient(u)
}
