package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var ennGetCmd = &cobra.Command{
	Use:   "ennget (TYPE [NAME]) [flags]",
	Short: "Display a console-specific resource",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run get cmd
		if err := runGet(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	ennGetCmd.PersistentFlags().StringVarP(&OutputFormat, "output", "o", "", "output format")
	ennGetCmd.PersistentFlags().StringVarP(&AppName, "app", "a", "", "app name")
	ennGetCmd.PersistentFlags().StringVarP(&RoleName, "role", "r", "", "role assignment, only work for resource user/sysadmin.")
	RootCmd.AddCommand(ennGetCmd)
}

func runGet(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	if len(args) == 0 {
		return errors.New(api.ResourceIsRequiredError)
	}
	var resources []string
	if len(args) > 1 {
		resources = args[1:]
	} else {
		resources = nil
	}
	switch args[0] {
	case App, Apps:
		return getApps(resources)
	case Storage, Storages:
		return getStorages(resources)
	case User, Users:
		return getUsers(resources)
	case Sysadmin, Sysadmins:
		return getSysAdmins(resources)
	case Namespace, Namespaces, NamespaceShortAlias:
		return getNamespaces(resources)
	case Services, Service, ServiceShortAlias:
		return getServices(resources)
	case StatefulSet, StatefulSets, StatefulSetShortAlias:
		return getStatefulSets(resources)
	case Deployment, Deployments, DeploymentShortAlias:
		return getDeployments(resources)
	case Pod, Pods, PodShortAlias:
		return getPods(resources)
	case LimitRange, LimitRangeShortAlias:
		return getLimitRanges(resources)
	default:
		return errors.New("the resource type '" + args[0] + "' is not exists")
	}
}
