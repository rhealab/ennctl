package cmd

import (
	"testing"
)

func TestCmdGetSysAdmin(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "sysadmin"})
	RootCmd.Execute()
}

func TestFuncGetSysAdmin(t *testing.T) {
	initContext()
	getSysAdmins([]string{"console", "developer"})
}

func TestCreateAndDeleteSysAdmin(t *testing.T) {
	initContext()
	deleteSysAdmins([]string{"console", "developer"})
	createSysAdmin([]string{"console", "developer"})
}
