package cmd

import (
	"testing"
)

func TestCmdGetNamespace(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "ns"})
	RootCmd.Execute()
}

func TestFuncGetNamespace(t *testing.T) {
	initContext()
	getNamespaces(nil)
}

func TestCreateAndDeleteNamespaceFromFile(t *testing.T) {
	initContext()
	createFromFile("../resource/ns.yaml")
	replaceFromFile("../resource/ns.yaml")
	deleteFromFile("../resource/ns.yaml")
}
