package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var setCmd = &cobra.Command{
	Use:                "set",
	Short:              "Set specific features on objects",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		routeToKubectl(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(setCmd)
}
