package cmd

import (
	"bytes"
	"cc/ennctl/common"
	"cc/ennctl/utils/editor"
	"fmt"
	"os"
)

func editBytes(input []byte) []byte {
	tmpFile, _ := os.Create("/tmp/tmp.yaml")
	tmpFile.Write(input)
	edit := editor.NewDefaultEditor([]string{"KUBE_EDITOR", "EDITOR"})
	edited, _, _ := edit.LaunchTempFile("prefix", "suffix", bytes.NewReader(input))
	return edited
}

func dispError(err error) {
	fmt.Println(err.Error())
}

func initContext() error {
	s := common.Setting{
		EnnContext: EnnContext,
	}
	if err := s.InitContext(); err != nil {
		return err
	}
	if NamespaceName == EmptyResource {
		NamespaceName = common.AppCtx.Namespace
	}
	return nil
}

func compareByteSlice(x []byte, y []byte) bool {
	if x == nil && y == nil {
		return true
	}
	if x == nil || y == nil {
		return false
	}
	if len(x) != len(y) {
		return false
	}
	for idx := range x {
		if x[idx] != y[idx] {
			return false
		}
	}
	return true
}
