package cmd

import (
	"cc/ennctl/common"
	"fmt"
	"os"
	"os/exec"
)

var ennDeleteOption = map[string]bool{
	"-f":          true,
	"--filename":  true,
	"-n":          true,
	"--namespace": true,
	"-a":          true,
	"--app":       true,
	"--role":      true,
	"-r":          true,
}

var ennFromFileOption = map[string]bool{
	"-f":         true,
	"--filename": true,
}

var ennResources = map[string]bool{
	App:                   true,
	Apps:                  true,
	Storage:               true,
	Storages:              true,
	User:                  true,
	Users:                 true,
	Sysadmin:              true,
	Sysadmins:             true,
	Namespace:             true,
	Namespaces:            true,
	NamespaceShortAlias:   true,
	Service:               true,
	Services:              true,
	ServiceShortAlias:     true,
	Deployment:            true,
	Deployments:           true,
	DeploymentShortAlias:  true,
	Pod:                   true,
	Pods:                  true,
	PodShortAlias:         true,
	LimitRange:            true,
	LimitRangeShortAlias:  true,
	FileFlag:              true,
	FileNameFlag:          true,
	StatefulSet:           true,
	StatefulSets:          true,
	StatefulSetShortAlias: true,
}

// Route forwards the call to kubectl or backend.
func Route(commandName string, commandArgs []string) {
	ap := ArgParser{
		Command:   commandName,
		Arguments: commandArgs,
	}
	ap.init()
	if ap.RouteToKubectl() {
		routeToKubectl(commandName, commandArgs)
	} else {
		routeToEnnCtl(commandName, commandArgs)
	}
}

func routeToEnnCtl(name string, args []string) {
	common.Logger.Log("route", "ennctl", "name", name, "args", fmt.Sprintf("%s", args))
	arguments := append([]string{"enn" + name}, args...)
	command := exec.Command("ennctl", arguments...)
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	if err := command.Run(); err != nil {
		os.Exit(1)
	}
}

func routeToKubectl(name string, args []string) {
	err := common.InitKubeConfig()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	common.Logger.Log("route", "kubectl", "name", name, "args", fmt.Sprintf("%s", args))
	arguments := append([]string{name}, args...)
	arguments = append(arguments, "--kubeconfig", common.KubeConfigPath)
	command := exec.Command("kubectl", arguments...)
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	if err := command.Run(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
