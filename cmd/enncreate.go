package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"errors"
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var ennCreateCmd = &cobra.Command{
	Use:   "enncreate -f FILENAME [flags]",
	Short: "Create a console-specific resource",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run create cmd
		if err := runCreate(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	ennCreateCmd.PersistentFlags().StringVarP(&FileName, "filename", "f", "", "filename of specification file")
	ennCreateCmd.PersistentFlags().StringVarP(&AppName, "app", "a", "", "app name")
	// user subcommand
	createUserCmd.PersistentFlags().StringVarP(&RoleName, "role", "r", "", "role assignment, only work for resource user/sysadmin.")
	ennCreateCmd.AddCommand(createUserCmd)
	ennCreateCmd.AddCommand(createSysAdminCmd)
	ennCreateCmd.AddCommand(createAppCmd)
	RootCmd.AddCommand(ennCreateCmd)
}

func runCreate(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	if FileName != EmptyResource {
		return createFromFile(FileName)
	}
	return errors.New(cmd.UsageString())
}

func createFromFile(filename string) error {
	r, err := api.Load(filename)
	if err != nil {
		return err
	}
	hasError := false
	c, l := r.Filter()
	// if custom resource yields error, deal it from here.
	if c != nil {
		if err = createFromResource(c); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			hasError = true
		}
	}
	// if error occurs in kubectl, error already displayed
	// so that we just exit with error code 2
	if l != nil {
		if err = createLegacyFromResource(l); err != nil {
			os.Exit(2)
		}
	}
	// if console resource has error, exit with error code 1 here
	if hasError {
		os.Exit(1)
	}
	return nil
}

func createFromResource(r *api.Resource) error {
	var err error
	switch r.Kind {
	case AppKind:
		return createAppFromResource(r)
	case NamespaceKind:
		return createNamespaceFromResource(r)
	case StorageKind:
		return createStorageFromResource(r)
	case UserKind:
		return createUserFromResource(r)
	case SysAdminKind:
		return createSysAdminFromResource(r)
	case DeploymentKind:
		return createDeploymentFromResource(r)
	case ServiceKind:
		return createServiceFromResource(r)
	case StatefulSetKind:
		return createStatefulSetFromResource(r)
	case GlobalLimitRangeKind:
		return createLimitRangeFromResource(r)
	case ListKind:
		msg := EmptyResource
		for _, item := range r.Items {
			if err = createFromResource(&item); err != nil {
				msg += err.Error()
				msg += "\n"
			}
		}
		if msg != EmptyResource {
			return errors.New(msg[:len(msg)-1])
		}
		return nil
	default:
		return fmt.Errorf("kind %s is not support at the moment", r.Kind)
	}
}

func createLegacyFromResource(r *api.Resource) error {
	allow, illegal := r.BlackList()
	if illegal != EmptyResource {
		fmt.Fprintf(os.Stderr, "%s\n", illegal)
	}
	if allow != nil {
		f, err := allow.SaveTmpFile()
		if err != nil {
			return err
		}
		err = common.InitKubeConfig()
		if err != nil {
			return err
		}
		return createFromKubectl(f)
	}
	return nil
}

func createFromKubectl(filename *string) error {
	defer os.Remove(*filename)
	arguments := []string{"create", "-f", *filename, "--kubeconfig", common.KubeConfigPath}
	command := exec.Command("kubectl", arguments...)
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	return command.Run()
}

// ======================= user subcommand =========================
var createUserCmd = &cobra.Command{
	Use:   "user",
	Short: "create user in namespace",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run create cmd
		if err := createUsers(args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

// ======================= app subcommand =========================
var createAppCmd = &cobra.Command{
	Use:   "app",
	Short: "create app in namespace",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run create cmd
		if err := createApps(args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

// ======================= sysadmin subcommand =========================
var createSysAdminCmd = &cobra.Command{
	Use:   "sysadmin",
	Short: "create sysadmin in namespace",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run create cmd
		if err := createSysAdmin(args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}
