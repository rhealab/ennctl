package cmd

import (
	"cc/ennctl/common"
	"cc/ennctl/console"
	"errors"
	"fmt"
	"os"

	"io/ioutil"

	"github.com/spf13/cobra"
)

var importCmd = &cobra.Command{
	Use:   "import -f FILENAME [flags]",
	Short: "Import a console app",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run import cmd
		if err := runImport(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	importCmd.PersistentFlags().StringVarP(&FileName, "filename", "f", "", "filename of specification file")
	RootCmd.AddCommand(importCmd)
}

func runImport(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	if FileName != EmptyResource {
		return importFromeFile(FileName)
	}
	return errors.New(cmd.UsageString())
}

func importFromeFile(filename string) error {
	j, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/script/isCover/N/import", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Import,
		Resource:     App,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	ap, err := cfg.AppPrinter()
	ap.Filename = filename
	if err != nil {
		return err
	}
	return ap.Print()
}
