package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"errors"
	"fmt"
)

//========================= console sysadmin RESTful functions =========================

func createSysAdminFromResource(r *api.Resource) error {
	s, err := r.SysAdmin()
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	j, err := s.SysAdminCreateRequest(api.AddUserActionType).JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/sys_admins", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     Sysadmin,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}

func createSysAdmin(sysAdminList []string) error {
	if len(sysAdminList) == 0 {
		return errors.New(api.NameIsRequiredError)
	}
	sysAdminCreateRequest := api.SysAdminCreateRequest{}
	sysAdminCreateRequest.AddList = append(sysAdminCreateRequest.AddList, sysAdminList...)
	jsonBytes, err := sysAdminCreateRequest.JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/sys_admins", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.POST, jsonBytes)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     Sysadmin,
		ResourceName: sysAdminList[0],
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}

func deleteSysAdmins(admins []string) error {
	if len(admins) == 0 {
		return errors.New(api.NameIsRequiredError)
	}
	scr := api.SysAdminCreateRequest{}
	scr.RemoveList = append(scr.RemoveList, admins...)
	j, err := scr.JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/sys_admins", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Delete,
		Resource:      Sysadmin,
		ResourceNames: admins,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	sp, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func getSysAdmins(users []string) error {
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/sys_admins", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      Sysadmin,
		ResourceNames: users,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	sp, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func deleteSysAdminFromResource(r *api.Resource) error {
	s, err := r.SysAdmin()
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	j, err := s.SysAdminCreateRequest(api.RemoveUserActionType).JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/sys_admins", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     Sysadmin,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}
