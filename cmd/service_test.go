package cmd

import (
	"testing"
)

func TestCmdGetService(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "svc"})
	RootCmd.Execute()
}

func TestFuncGetService(t *testing.T) {
	initContext()
	getServices(nil)
	getServices([]string{"test", "wiki-svc"})
}

func TestCreateAndDeleteServiceFromFile(t *testing.T) {
	initContext()
	createFromFile("../resource/service.yaml")
	replaceFromFile("../resource/service.yaml")
	deleteService("test")
	deleteFromFile("../resource/service.yaml")
}
