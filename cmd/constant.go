package cmd

const (
	// App constant string for app resource
	App string = "app"

	// Apps constant string for app resource
	Apps string = "apps"

	// Storage constant string for storage resource
	Storage string = "storage"

	// Storages constant string for storage resource
	Storages string = "storages"

	// User constant string for role_assignment resource.
	User string = "user"

	// Users constant string for role_assignment resource.
	Users string = "users"

	// Sysadmins constant string for sysdmins
	Sysadmins string = "sysadmins"

	// Sysadmin constant string for sysdmins
	Sysadmin string = "sysadmin"

	// Developer is constant string for developer
	Developer string = "developer"

	// NsAdmin is constant string for nsadmin
	NsAdmin string = "nsadmin"
)

// FileFlag constant string for -f flag
const FileFlag string = "-f"

// FileNameFlag constant string for --filename flag
const FileNameFlag string = "--filename"

const (
	// Namespaces constant string for namespaces
	Namespaces string = "namespaces"

	// Namespace constant string for namespace
	Namespace string = "namespace"

	// NamespaceShortAlias is constant string for namespace short alias ns
	NamespaceShortAlias string = "ns"

	// Services constant string for services
	Services string = "services"

	// Service constant string for service
	Service string = "service"

	// StatefulSet constant string for statefulset
	StatefulSet string = "statefulset"

	// StatefulSets constant string for statefulsets
	StatefulSets string = "statefulsets"

	// StatefulSetShortAlias is constant string for statefulset short alias ss
	StatefulSetShortAlias string = "ss"

	// ServiceShortAlias is constant string for service short alias svc
	ServiceShortAlias string = "svc"

	//Deployment is constant string for deployment
	Deployment string = "deployment"

	// Deployments is constant string for deployments
	Deployments string = "deployments"

	// DeploymentShortAlias is constant string for deployment short alias deploy
	DeploymentShortAlias string = "deploy"

	// LimitRange is constant string for limit range
	LimitRange string = "globalLimitRange"

	// LimitRangeShortAlias is constant string for limit range shor alias
	LimitRangeShortAlias string = "lm"

	// Pod is constant string for pod
	Pod string = "pod"

	// Pods is constant string for pods
	Pods string = "pods"

	// PodShortAlias is constant string for pod short alias po
	PodShortAlias string = "po"
)

const (
	// Kind the key of k8s kind field
	Kind string = "kind"

	// ServiceKind constant string for kind of Service
	ServiceKind string = "Service"

	// StatefulSetKind constant string for kind of StatefulSet
	StatefulSetKind string = "StatefulSet"

	// ListKind constant string for kind of List
	ListKind string = "List"

	// AppKind is constant string for kind of App
	AppKind string = "App"

	// StorageKind is constant string for kind of storage
	StorageKind string = "Storage"

	// UserKind is constant string for kind of user
	UserKind string = "User"

	// NamespaceKind is constant string for kind of namespace
	NamespaceKind string = "Namespace"

	// SysAdminKind is constant string for kind of sysadmin
	SysAdminKind string = "SysAdmin"

	// DeploymentKind is constant string for kind of deployment
	DeploymentKind string = "Deployment"

	// GlobalLimitRangeKind is constant string for kind of limitRange
	GlobalLimitRangeKind string = "GlobalLimitRange"
)

const (
	// TCP is the consant string for TCP
	TCP string = "TCP"
	// UDP is the constant string for UDP
	UDP string = "UDP"
	// NoneStr is used to display none at port, externalIPs and etc.
	NoneStr string = "<none>"
	// NodePort is constant string for node port svc
	NodePort string = "NodePort"
	// NodeStr is used to display external IP for nodePort svc.
	NodeStr string = "<nodes>"
)
const (
	// NsadminAllCap is constant string for ns admin
	NsadminAllCap string = "NS_ADMIN"
	// DeveloperAllCap is constant string for developer
	DeveloperAllCap string = "DEVELOPER"
)

const (
	// Rbd : constant string for RBD
	Rbd string = "RBD"
	// CephFs is constant string for CephFS
	CephFs string = "CephFS"
	// HostPath is constant string for HostPath
	HostPath string = "HostPath"
)

const (
	//ReadWriteOnce is constant string for accessmode ReadWriteOnce
	ReadWriteOnce string = "ReadWriteOnce"
	// ReadOnlyMany is constant string for accessmode ReadOnlyMany
	ReadOnlyMany string = "ReadOnlyMany"
	// ReadWriteMany is constant string for accessmode ReadWriteMany
	ReadWriteMany string = "ReadWriteMany"
)

// action string
const (
	// Create is constant string for action create
	Create string = "create"
	// Delete is constant string for action delete
	Delete string = "delete"
	// Get is constant string for action get
	Get string = "get"
	// Export is constant string for action get
	Export string = "export"
	// Import is constant string for action get
	Import string = "import"
	// Replace is constant string for action replace
	Replace string = "replace"
	// Edit is constant string for action edit
	Edit string = "edit"
	// Scale is constant string for action edit
	Scale string = "scale"
)

// VersionString constant string for version information
const VersionString string = "ennctl 1.2.0-release-hotfix1"

// all ennctl supported operation type
const (
	get        = "get"
	ennget     = "ennget"
	delete     = "delete"
	edit       = "edit"
	enndelete  = "enndelete"
	create     = "create"
	enncreate  = "enncreate"
	replace    = "replace"
	ennreplace = "ennreplace"
	ennedit    = "ennedit"
)

const (
	// YamlFormat constant string for yaml output format
	YamlFormat string = "yaml"

	// JSONFormat constant string for json output format
	JSONFormat string = "json"
)

const (
	//EnngetDescription description of ennget command.
	EnngetDescription string = "Display one or many resources.\n\nValid resource types include:\n\n * apps\n * storages\n * deployments (aka 'deploy')\n * services (aka 'svc')\n * statefulsets (aka 'ss')\n * pods (aka 'po')\n * nodes\n * users"
)

// CurrentAPIVersion constant string for current supported API
const CurrentAPIVersion string = "v1"

// K8sDeployAPIVersion constant string for legacy deployment
const K8sDeployAPIVersion string = "extensions/v1beta1"

// K8sStatefulSetAPIVersion constant string for legacy statefulset
const K8sStatefulSetAPIVersion string = "apps/v1beta1"

// ToDo constant string for todo placeholder
const ToDo string = "TODO"

// Invalid constant string for invalid argument
const Invalid string = "The argument is invalid"

// ResourceTypeMustSpecified error code for ResourceTypeMustSpecified
const ResourceTypeMustSpecified string = "type of the resource must be specified"

// EmptyResource constant string for empty resource
const EmptyResource string = ""

// NoResourceFound constant string for no resource found
const NoResourceFound string = "No resources found."

// NoChangeIsMade constant string for no change is made
const NoChangeIsMade string = "Edit cancelled, no changes made."

// AppAllCap string label for apps
const AppAllCap string = "X_APP"
