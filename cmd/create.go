package cmd

import "github.com/spf13/cobra"

var createCmd = &cobra.Command{
	Use:                "create",
	Short:              "Create a console-specific resource or k8s legacy resource",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		Route(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(createCmd)
}
