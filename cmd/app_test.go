package cmd

import (
	"testing"
)

func TestCmdGetApp(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "po"})
	RootCmd.Execute()
}

func TestFuncGetApp(t *testing.T) {
	initContext()
	getApps(nil)
	getApps([]string{"app", "poe"})
}

func TestCreateAndDeleteApp(t *testing.T) {
	RootCmd.SetArgs([]string{"enncreate", "app", "poe"})
	RootCmd.Execute()
	RootCmd.SetArgs([]string{"enndelete", "app", "poe"})
	RootCmd.Execute()
}

func TestCreateAndDeleteAppFromFile(t *testing.T) {
	initContext()
	createFromFile("../resource/app.yaml")
	deleteFromFile("../resource/app.yaml")
}
