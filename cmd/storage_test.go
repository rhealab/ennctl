package cmd

import (
	"testing"
)

func TestCmdGetStorage(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "storage"})
	RootCmd.Execute()
}

func TestFuncGetStorage(t *testing.T) {
	initContext()
	getStorages(nil)
	getStorages([]string{"enn-ss-ss-0", "enn-ss-ss-1"})
}

func TestCreateAndDeleteStorageFromFile(t *testing.T) {
	initContext()
	createFromFile("../resource/storage.yaml")
	replaceFromFile("../resource/storage.yaml")
	deleteFromFile("../resource/storage.yaml")
}
