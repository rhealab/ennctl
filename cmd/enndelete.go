package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"errors"
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var ennDeleteCmd = &cobra.Command{
	Use:   "enndelete TYPE [NAME] [flags]",
	Short: "Delete a console-specific resource",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run delete cmd
		if err := runDelete(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	// -f specify delete from file
	ennDeleteCmd.PersistentFlags().StringVarP(&FileName, "filename", "f", "", "filename of specification file")
	// -a specify app name from command line
	ennDeleteCmd.PersistentFlags().StringVarP(&AppName, "app", "a", "", "app name")
	// -r specify rolename for user management
	ennDeleteCmd.PersistentFlags().StringVarP(&RoleName, "role", "r", "", "role assignment, only work for resource user/sysadmin.")
	RootCmd.AddCommand(ennDeleteCmd)
}

func runDelete(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	if FileName != EmptyResource {
		if err := deleteFromFile(FileName); err != nil {
			return err
		}
		return nil
	}
	if len(args) == 0 {
		return errors.New(api.ResourceIsRequiredError)
	}
	var resources []string
	if len(args) > 1 {
		resources = args[1:]
	} else {
		resources = nil
	}
	switch args[0] {
	case App, Apps:
		return deleteApps(resources)
	case Storage, Storages:
		return deleteStorages(resources)
	case User, Users:
		return deleteUsers(resources)
	case Namespace, Namespaces, NamespaceShortAlias:
		return deleteNamespaces(resources)
	case Sysadmin, Sysadmins:
		return deleteSysAdmins(resources)
	case Service, Services, ServiceShortAlias:
		return deleteServices(resources)
	case StatefulSet, StatefulSets, StatefulSetShortAlias:
		return deleteStatefulSets(resources)
	case Deployment, Deployments, DeploymentShortAlias:
		return deleteDeployments(resources)
	case Pod, Pods, PodShortAlias:
		return deletePods(resources)
	default:
		return fmt.Errorf("%s is forbidden", args[0])
	}
}

func deleteFromFile(filename string) error {
	r, err := api.Load(filename)
	if err != nil {
		return err
	}
	hasError := false
	c, l := r.Filter()
	// if console resource yields error, deal it from here.
	if c != nil {
		if err = deleteFromResource(c); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			hasError = true
		}
	}
	// if error occurs in kubectl, error already displayed
	// so that we just exit with error code 2
	if l != nil {
		if err := deleteLegacyFromResource(l); err != nil {
			os.Exit(2)
		}
	}
	// if console resource has error, exit with error code 1 here
	if hasError {
		os.Exit(1)
	}
	return nil
}

func deleteFromResource(r *api.Resource) error {
	switch r.Kind {
	case AppKind:
		a, err := r.App()
		if err != nil {
			return err
		}
		if a.ObjectMeta.Namespace != EmptyResource {
			NamespaceName = a.ObjectMeta.Namespace
		}
		return deleteApp(a.ObjectMeta.Name)
	case NamespaceKind:
		n, err := r.Namespace()
		if err != nil {
			return err
		}
		return deleteNamespace(n.ObjectMeta.Name)
	case StorageKind:
		s, err := r.Storage()
		if err != nil {
			return err
		}
		if s.ObjectMeta.Namespace != EmptyResource {
			NamespaceName = s.ObjectMeta.Namespace
		}
		return deleteStorage(s.ObjectMeta.Name)
	case DeploymentKind:
		d, err := r.Deployment()
		if err != nil {
			return err
		}
		if d.ObjectMeta.Namespace != EmptyResource {
			NamespaceName = d.ObjectMeta.Namespace
		}
		return deleteDeployment(d.ObjectMeta.Name)
	case ServiceKind:
		s, err := r.Service()
		if err != nil {
			return err
		}
		if s.ObjectMeta.Namespace != EmptyResource {
			NamespaceName = s.ObjectMeta.Namespace
		}
		return deleteService(s.ObjectMeta.Name)
	case StatefulSetKind:
		s, err := r.StatefulSet()
		if err != nil {
			return err
		}
		if s.ObjectMeta.Namespace != EmptyResource {
			NamespaceName = s.ObjectMeta.Namespace
		}
		return deleteStatefulSet(s.ObjectMeta.Name)
	case SysAdminKind:
		return deleteSysAdminFromResource(r)
	case UserKind:
		return deleteUserFromResource(r)
	case ListKind:
		msg := ""
		for _, item := range r.Items {
			if err := deleteFromResource(&item); err != nil {
				msg += err.Error()
				msg += "\n"
			}
		}
		if msg != "" {
			return errors.New(msg[:len(msg)-1])
		}
		return nil
	default:
		return fmt.Errorf("error: kind %s does not suport delete at the moment", r.Kind)
	}
}

func deleteLegacyFromResource(r *api.Resource) error {
	allow, illegal := r.BlackList()
	if illegal != EmptyResource {
		fmt.Fprintf(os.Stderr, "%s\n", illegal)
	}
	if allow != nil {
		f, err := allow.SaveTmpFile()
		if err != nil {
			return err
		}
		err = common.InitKubeConfig()
		if err != nil {
			return err
		}
		return deleteFromKubectl(f)
	}
	return nil
}

func deleteFromKubectl(filename *string) error {
	defer os.Remove(*filename)
	arguments := []string{"delete", "-f", *filename, "--kubeconfig", common.KubeConfigPath}
	command := exec.Command("kubectl", arguments...)
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	return command.Run()
}
