package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/ghodss/yaml"
)

//========================= console storage RESTful functions =========================

func createStorageFromResource(r *api.Resource) error {
	s, err := r.Storage()
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	j, err := s.StorageCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/storage/", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     Storage,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StoragePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func deleteStorages(storages []string) error {
	if storages == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, storage := range storages {
		if err := deleteStorage(storage); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func deleteStorage(storageName string) error {
	if storageName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/storage/name/%s", common.AppCtx.GatewayURL, NamespaceName, storageName)
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     Storage,
		ResourceName: storageName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StoragePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func getStorages(storages []string) error {
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/storage", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      Storage,
		ResourceNames: storages,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	sp, err := cfg.StoragePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func replaceStorageFromResource(r *api.Resource) error {
	s, err := r.Storage()
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	j, err := s.StorageCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/storage/name/%s", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, s.ObjectMeta.Name)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Replace,
		Resource:     Storage,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StoragePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func getStorageResponseByName(storageName string) (*api.StorageResponse, error) {
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/storage/name/%s", common.AppCtx.GatewayURL, NamespaceName, storageName)
	res, code, _ := common.Exec(false, url, common.GET, nil)
	var s = &api.StorageResponse{}
	if code != 200 {
		var errCode ErrorCodeResponse
		json.Unmarshal(res, &errCode)
		return nil, errCode
	}
	json.Unmarshal(res, s)
	return s, nil
}

func editStorage(storageName string) error {
	if storageName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	sr, err := getStorageResponseByName(storageName)
	if err != nil {
		return err
	}
	y, err := sr.Storage().YAML()
	if err != nil {
		return err
	}
	yBytes := editBytes(y)
	if compareByteSlice(yBytes, y) {
		fmt.Println(NoChangeIsMade)
		return nil
	}
	s := &api.Storage{}
	err = yaml.Unmarshal(yBytes, s)
	if err != nil {
		return err
	}
	if err = s.Valid(); err != nil {
		return err
	}
	j, err := s.StorageCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/storage/%d", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, sr.ID)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Edit,
		Resource:     Storage,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.StoragePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}
