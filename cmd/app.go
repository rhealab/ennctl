package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"errors"
	"fmt"
	"os"
)

//========================= console app RESTful functions =========================

func createApps(apps []string) error {
	if apps == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, app := range apps {
		if err := createApp(app); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func createApp(appName string) error {
	if appName == EmptyResource {
		return errors.New(api.AppIsRequiredError)
	}
	a := api.AppCreateRequest{
		Name: appName,
	}
	j, err := a.JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     App,
		ResourceName: appName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	ap, err := cfg.AppPrinter()
	if err != nil {
		return err
	}
	return ap.Print()
}

func createAppFromResource(r *api.Resource) error {
	a, err := r.App()
	if err != nil {
		return err
	}
	if err = a.Valid(); err != nil {
		return err
	}
	j, err := a.AppCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps", common.AppCtx.GatewayURL, a.ObjectMeta.Namespace)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     App,
		ResourceName: a.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	ap, err := cfg.AppPrinter()
	if err != nil {
		return err
	}
	return ap.Print()
}

func deleteApps(apps []string) error {
	if apps == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, app := range apps {
		if err := deleteApp(app); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func deleteApp(appName string) error {
	if appName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s", common.AppCtx.GatewayURL, NamespaceName, appName)
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     App,
		ResourceName: appName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	ap, err := cfg.AppPrinter()
	if err != nil {
		return err
	}
	return ap.Print()
}

func getApps(apps []string) error {
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      App,
		ResourceNames: apps,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	ap, err := cfg.AppPrinter()
	if err != nil {
		return err
	}
	return ap.Print()
}

func exportApps(apps []string) error {
	for _, app := range apps {
		common.Logger.Log("app_to_export", app)
		exportApp(app)
	}
	return nil
}

func exportApp(app string) error {
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/script/export", common.AppCtx.GatewayURL, NamespaceName, app)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	saveAppToFile(res, app, NamespaceName)
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Export,
		Resource:     App,
		ResourceName: app,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	ap, err := cfg.AppPrinter()
	ap.Filename = getFileName(app, NamespaceName)
	if err != nil {
		return err
	}
	return ap.Print()
}

func saveAppToFile(data []byte, app string, namespace string) error {
	f, err := os.Create(getFileName(app, namespace))
	if err != nil {
		return err
	}
	if _, err = f.Write(data); err != nil {
		return err
	}
	return f.Close()
}

func getFileName(app string, namespace string) string {
	return fmt.Sprintf("import-%s_%s.json", app, namespace)
}
