package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/ghodss/yaml"
)

func createServiceFromResource(r *api.Resource) error {
	s, err := r.Service()
	if err != nil {
		return err
	}
	// no app name
	if AppName == EmptyResource && s.ObjectMeta.App == nil {
		return errors.New(api.AppIsRequiredError)
	}
	// Analyze app name
	if AppName != EmptyResource {
		s.ObjectMeta.App = &AppName
	}
	appName := *s.ObjectMeta.App
	if AppName != EmptyResource {
		appName = AppName
	}
	// @TODO:Workarround, change to nornal change once JINXIN did his work.
	j, err := s.K8sStylize().SliceJSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/batch/k8s_json", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     Service,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.ServicePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func replaceServiceFromResource(r *api.Resource) error {
	s, err := r.Service()
	if err != nil {
		return err
	}
	// no app name
	if AppName == EmptyResource && s.ObjectMeta.App == nil {
		return errors.New(api.AppIsRequiredError)
	}
	// Analyze app name
	if AppName != EmptyResource {
		s.ObjectMeta.App = &AppName
	}
	appName := *s.ObjectMeta.App
	if AppName != EmptyResource {
		appName = AppName
	}
	j, err := s.K8sStylize().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/services/%s", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName, s.ObjectMeta.Name)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Replace,
		Resource:     Deployment,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.ServicePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func deleteServices(services []string) error {
	if services == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, service := range services {
		if err := deleteService(service); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func deleteService(serviceName string) error {
	if serviceName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	var url string
	if AppName == EmptyResource {
		svc, err := getServiceByName(serviceName)
		if err != nil {
			return err
		}
		appName := svc.App()
		if appName == api.Legacy {
			url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/services/%s", common.AppCtx.GatewayURL, NamespaceName, serviceName)
		} else {
			url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/services/%s", common.AppCtx.GatewayURL, NamespaceName, appName, serviceName)
		}
	} else {
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/services/%s", common.AppCtx.GatewayURL, NamespaceName, AppName, serviceName)
	}
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     Service,
		ResourceName: serviceName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.ServicePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func getServiceByName(serviceName string) (*api.Service, error) {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/services/%s/origin", common.AppCtx.GatewayURL, NamespaceName, serviceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return nil, err
	}
	var s api.Service
	if code != 200 {
		var errCode ErrorCodeResponse
		json.Unmarshal(res, &errCode)
		return nil, errCode
	}
	json.Unmarshal(res, &s)
	return &s, nil
}

// getService function. If serviceName is nil then get service list else get specified name service's info
func getServices(services []string) error {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/services/origin_list", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      Service,
		ResourceNames: services,
		OutputFormat:  OutputFormat,
		Filter:        AppName,
		RawResponse:   res,
	}
	sp, err := cfg.ServicePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}

func editService(svcName string) error {
	if svcName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	s, err := getServiceByName(svcName)
	if err != nil {
		return err
	}
	appName := s.App()
	if AppName != EmptyResource && AppName != appName {
		return errors.New(api.AppNameNotMatchError)
	}
	var y, j []byte
	var url string
	if appName == api.Legacy {
		y, err = s.YAML()
	} else {
		y, err = s.ConsoleStylize().YAML()
	}
	if err != nil {
		return err
	}
	yBytes := editBytes(y)
	if compareByteSlice(yBytes, y) {
		fmt.Println(NoChangeIsMade)
		return nil
	}
	s = &api.Service{}
	yaml.Unmarshal(yBytes, s)

	if appName != api.Legacy {
		j, err = s.K8sStylize().JSON()
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/services/%s", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace, appName, s.ObjectMeta.Name)
	} else {
		j, err = s.JSON()
		url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/services", common.AppCtx.GatewayURL, s.ObjectMeta.Namespace)
	}
	if err != nil {
		return err
	}
	res, code, _ := common.Exec(false, url, common.PUT, j)
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Edit,
		Resource:     Service,
		ResourceName: s.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	sp, err := cfg.ServicePrinter()
	if err != nil {
		return err
	}
	return sp.Print()
}
