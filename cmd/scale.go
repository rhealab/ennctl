package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// ReplicaCount flag for scale up/down
var ReplicaCount int32

var scaleCmd = &cobra.Command{
	Use:   "scale",
	Short: "scale a single resource by name",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run scale cmd
		if err := runScale(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	scaleCmd.PersistentFlags().StringVarP(&AppName, "app", "a", "", "app name")
	scaleCmd.PersistentFlags().Int32VarP(&ReplicaCount, "replicas", "", -1, "scale to the specified replicas")
	RootCmd.AddCommand(scaleCmd)
}

func runScale(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	var err error
	var resourceName string
	if len(args) > 1 {
		resourceName = args[1]
	} else {
		resourceName = EmptyResource
	}
	switch args[0] {
	case StatefulSet, StatefulSets, StatefulSetShortAlias:
		err = scaleStatefulSet(resourceName)
	case Deployment, Deployments, DeploymentShortAlias:
		err = scaleDeployment(resourceName)
	default:
		err = errors.New(api.ScaleNotAllowedError)
	}
	return err
}
