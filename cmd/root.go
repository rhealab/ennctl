package cmd

import (
	"cc/ennctl/common"
	"fmt"
	"log"

	"github.com/spf13/cobra"
)

// Verbose is a flag which indicates to display detailed logs
var Verbose bool

// NamespaceName is a flag which indicates the namespace name
var NamespaceName string

// EnnContext is a flag which indicates current context from command line
var EnnContext string

// RoleName is a flag which indicates role name
var RoleName string

// OutputFormat is a flag which specify output format
var OutputFormat string

// AppName is a flag which specify app name
var AppName string

// FileName is a flag which specify the path of source file.
var FileName string

// ContainerName is a flag which specify the container name.
var ContainerName string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                "ennctl",
	Short:              "A brief description of your application",
	Long:               `ennctl controls the enn console management.`,
	DisableFlagParsing: false,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		checkErr(err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	RootCmd.PersistentFlags().StringVarP(&NamespaceName, "namespace", "n", "", "namespace name")
	RootCmd.PersistentFlags().StringVarP(&EnnContext, "context", "", "", "Set current context.")
}

func initConfig() {
	// DO NOTHING
}

func checkErr(err error) {
	if err != nil {
		common.Logger.Log("err", fmt.Sprintf("%v", err))
		log.Fatal("ERROR:", err)
	}
}
