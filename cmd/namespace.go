package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/ghodss/yaml"
)

//========================= console namespace RESTful functions =========================

func createNamespaceFromResource(r *api.Resource) error {
	n, err := r.Namespace()
	if err != nil {
		return err
	}
	if err = n.Valid(); err != nil {
		return err
	}
	jsonBytes, err := n.NamespaceCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/kubernetes/namespaces", common.AppCtx.GatewayURL)

	res, code, err := common.Exec(false, url, common.POST, jsonBytes)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     Namespace,
		ResourceName: n.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	np, err := cfg.NamespacePrinter()
	if err != nil {
		return err
	}
	return np.Print()
}

func replaceNamespaceFromResource(r *api.Resource) error {
	n, err := r.Namespace()
	if err != nil {
		return err
	}
	if err = n.Valid(); err != nil {
		return err
	}
	jsonBytes, err := n.NamespaceCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespace_stats/%s", common.AppCtx.GatewayURL, n.ObjectMeta.Name)
	res, code, err := common.Exec(false, url, common.PUT, jsonBytes)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Replace,
		Resource:     Namespace,
		ResourceName: n.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	np, err := cfg.NamespacePrinter()
	if err != nil {
		return err
	}
	return np.Print()
}

func deleteNamespaces(namespaces []string) error {
	if namespaces == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, namespace := range namespaces {
		if err := deleteNamespace(namespace); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func deleteNamespace(namespaceName string) error {
	if namespaceName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/kubernetes/namespaces/%s", common.AppCtx.GatewayURL, namespaceName)
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     Namespace,
		ResourceName: namespaceName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	np, err := cfg.NamespacePrinter()
	if err != nil {
		return err
	}
	return np.Print()
}

func getNamespaces(namespaces []string) error {
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      Namespace,
		ResourceNames: namespaces,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	np, err := cfg.NamespacePrinter()
	if err != nil {
		return err
	}
	return np.Print()
}

func getNamespaceStatByname(nsName string) (*api.NamespaceStat, error) {
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespace_stats/%s/new", common.AppCtx.GatewayURL, nsName)
	res, code, _ := common.Exec(false, url, common.GET, nil)
	var ns = &api.NamespaceStat{}
	if code != 200 {
		var errCode ErrorCodeResponse
		json.Unmarshal(res, &errCode)
		return ns, errCode
	}
	json.Unmarshal(res, ns)
	return ns, nil
}

func editNamespace(nsName string) error {
	if nsName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	nsStat, err := getNamespaceStatByname(nsName)
	if err != nil {
		return err
	}
	y, err := nsStat.Namespace().YAML()
	if err != nil {
		return err
	}
	yBytes := editBytes(y)
	if compareByteSlice(yBytes, y) {
		fmt.Println(NoChangeIsMade)
		return nil
	}
	n := &api.Namespace{}
	err = yaml.Unmarshal(yBytes, n)
	if err != nil {
		return err
	}
	if err = n.Valid(); err != nil {
		return err
	}
	j, err := n.NamespaceCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespace_stats/%s", common.AppCtx.GatewayURL, n.ObjectMeta.Name)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Edit,
		Resource:     Namespace,
		ResourceName: n.ObjectMeta.Name,
		RawResponse:  res,
	}
	np, err := cfg.NamespacePrinter()
	if err != nil {
		return err
	}
	return np.Print()
}
