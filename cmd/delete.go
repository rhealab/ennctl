package cmd

import "github.com/spf13/cobra"

var deleteCmd = &cobra.Command{
	Use:                "delete",
	Short:              "Delete a console-specific resource or k8s legacy resource",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		Route(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(deleteCmd)
}
