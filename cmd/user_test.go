package cmd

import (
	"testing"
)

func TestCmdGetUser(t *testing.T) {
	RootCmd.SetArgs([]string{"ennget", "user"})
	RootCmd.Execute()
}

func TestFuncGetUser(t *testing.T) {
	initContext()
	getUsers([]string{"console", "developer"})
}

func TestCreateAndDeleteUser(t *testing.T) {
	initContext()
	RoleName = "nsadmin"
	deleteUsers([]string{"console", "developer"})
	createSysAdmin([]string{"console", "developer"})
}
