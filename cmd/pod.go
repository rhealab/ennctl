package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"encoding/json"
	"errors"
	"fmt"
)

func getPods(pods []string) error {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/pods/origin_list", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      Pod,
		ResourceNames: pods,
		Filter:        AppName,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	pp, err := cfg.PodPrinter()
	if err != nil {
		return err
	}
	return pp.Print()
}

func deletePods(pods []string) error {
	if pods == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, pod := range pods {
		if err := deletePod(pod); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func deletePod(podName string) error {
	if podName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/pods/%s", common.AppCtx.GatewayURL, NamespaceName, podName)
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     Pod,
		ResourceName: podName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	pp, err := cfg.PodPrinter()
	if err != nil {
		return err
	}
	return pp.Print()
}

func getPodByName(podName string, namespaceName string) (*api.Pod, error) {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/pods/%s/origin", common.AppCtx.GatewayURL, namespaceName, podName)
	res, code, _ := common.Exec(false, url, common.GET, nil)
	var p api.Pod
	if !common.IsSuccessHTTPCode(code) {
		var errCode ErrorCodeResponse
		json.Unmarshal(res, &errCode)
		return nil, errCode
	}
	json.Unmarshal(res, &p)
	return &p, nil
}
