package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"errors"
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var ennReplaceCmd = &cobra.Command{
	Use:   "ennreplace -f FILE_NAME [flags]",
	Short: "Replace a console-specific resource",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run replace cmd
		if err := runReplace(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	ennReplaceCmd.PersistentFlags().StringVarP(&FileName, "filename", "f", "", "filename of specification file")
	ennReplaceCmd.PersistentFlags().StringVarP(&AppName, "app", "a", "", "app name")
	ennReplaceCmd.PersistentFlags().StringVarP(&RoleName, "role", "r", "", "role assignment, only work for resource user/sysadmin.")
	RootCmd.AddCommand(ennReplaceCmd)
}

func runReplace(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	if FileName == EmptyResource {
		return errors.New(api.FilenameIsRequiredError)
	}
	return replaceFromFile(FileName)
}

func replaceFromFile(filename string) error {
	r, err := api.Load(filename)
	if err != nil {
		return err
	}
	hasError := false
	c, l := r.Filter()
	// if console resource yields error, deal it from here.
	if c != nil {
		if err = replaceFromResource(c); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			hasError = true
		}
	}
	// if error occurs in kubectl, error already displayed
	// so that we just exit with error code 2
	if l != nil {
		if err := replaceLegacyFromResource(l); err != nil {
			os.Exit(2)
		}
	}
	// if console resource has error, exit with error code 1 here
	if hasError {
		os.Exit(1)
	}
	return nil
}

func replaceFromResource(r *api.Resource) error {
	switch r.Kind {
	case NamespaceKind:
		return replaceNamespaceFromResource(r)
	case StorageKind:
		return replaceStorageFromResource(r)
	case DeploymentKind:
		return replaceDeploymentFromResource(r)
	case ServiceKind:
		return replaceServiceFromResource(r)
	case StatefulSetKind:
		return replaceStatefulSetFromResource(r)
	case ListKind:
		msg := ""
		for _, item := range r.Items {
			if err := replaceFromResource(&item); err != nil {
				msg += err.Error()
				msg += "\n"
			}
		}
		if msg != "" {
			return errors.New(msg[:len(msg)-1])
		}
		return nil
	default:
		return fmt.Errorf("error: kind %s does not suport replace at the moment", r.Kind)
	}
}

func replaceLegacyFromResource(r *api.Resource) error {
	allow, illegal := r.BlackList()
	if illegal != EmptyResource {
		fmt.Fprintf(os.Stderr, "%s\n", illegal)
	}
	if allow != nil {
		f, err := allow.SaveTmpFile()
		if err != nil {
			return err
		}
		err = common.InitKubeConfig()
		if err != nil {
			return err
		}
		return replaceFromKubectl(f)
	}
	return nil
}

func replaceFromKubectl(filename *string) error {
	defer os.Remove(*filename)
	arguments := []string{"replace", "-f", *filename, "--kubeconfig", common.KubeConfigPath}
	command := exec.Command("kubectl", arguments...)
	command.Stdin = os.Stdin
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	return command.Run()
}
