package cmd

import (
	"github.com/spf13/cobra"
)

var replaceCmd = &cobra.Command{
	Use:                "replace -f [FILE_NAME] ... [flags]",
	Short:              "Replace resource in namespace",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		Route(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(replaceCmd)
}
