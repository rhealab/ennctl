package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"fmt"
	"os"
)

//========================= console limit range RESTful functions =========================

func createLimitRangeFromResource(r *api.Resource) error {
	l, err := r.LimitRange()
	if err != nil {
		return err
	}
	// get deploy create request payload in json
	j, err := l.LimitRangeCreateRequest().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/kubernetes/limitranges", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     LimitRange,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	lmp, err := cfg.LimitRangePrinter()
	if err != nil {
		return err
	}
	return lmp.Print()
}

func getLimitRanges(lms []string) error {
	if lms != nil {
		fmt.Fprintf(os.Stderr, "warning: we only support globale setting of limit range. No limit range name ls needed.\n")
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/sys/kubernetes/limitranges", common.AppCtx.GatewayURL)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Get,
		Resource:     Deployment,
		OutputFormat: OutputFormat,
		Filter:       AppName,
		RawResponse:  res,
	}
	lmp, err := cfg.LimitRangePrinter()
	if err != nil {
		return err
	}
	return lmp.Print()
}
