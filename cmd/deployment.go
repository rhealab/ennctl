package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/ghodss/yaml"
)

//========================= console deployment RESTful functions =========================

func createDeploymentFromResource(r *api.Resource) error {
	d, err := r.Deployment()
	if err != nil {
		return err
	}
	if err = d.Valid(); err != nil {
		return err
	}
	// no app name
	if AppName == EmptyResource && d.ObjectMeta.App == nil {
		return errors.New(api.AppIsRequiredError)
	}
	// Analyze app name
	if AppName != EmptyResource {
		d.ObjectMeta.App = &AppName
	}
	appName := *d.ObjectMeta.App
	if AppName != EmptyResource {
		appName = AppName
	}
	// @TODO:Workarround, change to nornal change once JINXIN did his work.
	j, err := d.K8sStylize().SliceJSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/batch/k8s_json", common.AppCtx.GatewayURL, d.ObjectMeta.Namespace, appName)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     Deployment,
		ResourceName: d.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	dp, err := cfg.DeploymentPrinter()
	if err != nil {
		return err
	}
	return dp.Print()
}

func replaceDeploymentFromResource(r *api.Resource) error {
	d, err := r.Deployment()
	if err != nil {
		return err
	}
	if err = d.Valid(); err != nil {
		return err
	}
	// no app name
	if AppName == EmptyResource && d.ObjectMeta.App == nil {
		return errors.New(api.AppIsRequiredError)
	}
	// Analyze app name
	if AppName != EmptyResource {
		d.ObjectMeta.App = &AppName
	}
	appName := *d.ObjectMeta.App
	if AppName != EmptyResource {
		appName = AppName
	}
	j, err := d.K8sStylize().JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/deployments/%s", common.AppCtx.GatewayURL, d.ObjectMeta.Namespace, appName, d.ObjectMeta.Name)
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Replace,
		Resource:     Deployment,
		ResourceName: d.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	dp, err := cfg.DeploymentPrinter()
	if err != nil {
		return err
	}
	return dp.Print()
}

func getDeployments(deploys []string) error {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/deployments/origin_list", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      Deployment,
		ResourceNames: deploys,
		OutputFormat:  OutputFormat,
		Filter:        AppName,
		RawResponse:   res,
	}
	dp, err := cfg.DeploymentPrinter()
	if err != nil {
		return err
	}
	return dp.Print()
}

func getDeploymentByName(deployName string) (*api.Deployment, error) {
	url := fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/deployments/%s/origin", common.AppCtx.GatewayURL, NamespaceName, deployName)
	res, code, _ := common.Exec(false, url, common.GET, nil)
	var d api.Deployment
	if code != 200 {
		var errCode ErrorCodeResponse
		json.Unmarshal(res, &errCode)
		return nil, errCode
	}
	json.Unmarshal(res, &d)
	return &d, nil
}

func deleteDeployments(deploys []string) error {
	if deploys == nil {
		return errors.New(api.NoResourceNameError)
	}
	msg := ""
	for _, deploy := range deploys {
		if err := deleteDeployment(deploy); err != nil {
			msg += fmt.Sprintf("%s\n", err.Error())
		}
	}
	if msg != "" {
		return errors.New(msg[:len(msg)-1])
	}
	return nil
}

func deleteDeployment(deployName string) error {
	if deployName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	var url string
	if AppName == EmptyResource {
		deploy, err := getDeploymentByName(deployName)
		if err != nil {
			return err
		}
		appName := deploy.App()
		if appName == api.Legacy {
			url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/deployments/%s", common.AppCtx.GatewayURL, NamespaceName, deployName)
		} else {
			url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/deployments/%s", common.AppCtx.GatewayURL, NamespaceName, appName, deployName)
		}
	} else {
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/deployments/%s", common.AppCtx.GatewayURL, NamespaceName, AppName, deployName)
	}
	res, code, err := common.Exec(false, url, common.DELETE, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     Deployment,
		ResourceName: deployName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	dp, err := cfg.DeploymentPrinter()
	if err != nil {
		return err
	}
	return dp.Print()
}

func editDeployment(deployName string) error {
	if deployName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	d, err := getDeploymentByName(deployName)
	if err != nil {
		return err
	}
	// check app name
	appName := d.App()
	if AppName != EmptyResource && AppName != appName {
		return errors.New(api.AppNameNotMatchError)
	}
	var y, j []byte
	var url string
	if appName == api.Legacy {
		y, err = d.YAML()
	} else {
		y, err = d.ConsoleStylize().YAML()
	}

	if err != nil {
		return err
	}
	yBytes := editBytes(y)
	if compareByteSlice(yBytes, y) {
		fmt.Println(NoChangeIsMade)
		return nil
	}
	d = &api.Deployment{}
	yaml.Unmarshal(yBytes, d)
	if err = d.Valid(); err != nil {
		return err
	}
	if appName != api.Legacy {
		j, err = d.K8sStylize().JSON()
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/deployments/%s", common.AppCtx.GatewayURL, d.ObjectMeta.Namespace, appName, d.ObjectMeta.Name)
	} else {
		j, err = d.JSON()
		url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/deployments", common.AppCtx.GatewayURL, d.ObjectMeta.Namespace)
	}
	if err != nil {
		return err
	}
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Edit,
		Resource:     Deployment,
		ResourceName: d.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	dp, err := cfg.DeploymentPrinter()
	if err != nil {
		return err
	}
	return dp.Print()
}

// @TODO once scale deployment API is ready, switch to that one.
func scaleDeployment(deployName string) error {
	if deployName == EmptyResource {
		return errors.New(api.NoResourceNameError)
	}
	d, err := getDeploymentByName(deployName)
	if err != nil {
		return err
	}
	appName := d.App()
	if AppName != EmptyResource && AppName != appName {
		return errors.New(api.AppNameNotMatchError)
	}
	// Check replicaCount
	if ReplicaCount < 0 {
		return errors.New(api.ReplicasMustExistAndBePositiveForScale)
	}
	d.Spec.Replicas = &ReplicaCount
	j, err := d.JSON()
	if err != nil {
		return err
	}
	var url string
	if appName != api.Legacy {
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/apps/%s/deployments/%s", common.AppCtx.GatewayURL, d.ObjectMeta.Namespace, appName, deployName)
	} else {
		url = fmt.Sprintf("%s/gw/ko/api/v1/origin/namespaces/%s/deployments", common.AppCtx.GatewayURL, d.ObjectMeta.Namespace)
	}
	res, code, err := common.Exec(false, url, common.PUT, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Scale,
		Resource:     Deployment,
		ResourceName: deployName,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	dp, err := cfg.DeploymentPrinter()
	if err != nil {
		return err
	}
	return dp.Print()
}
