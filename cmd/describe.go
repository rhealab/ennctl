package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var describeCmd = &cobra.Command{
	Use:                "describe",
	Short:              "Describe a console-specific resource or k8s legacy resource",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run describe cmd
		runDescribe(cmd.Name(), args)
	},
}

func init() {
	RootCmd.AddCommand(describeCmd)
}

func runDescribe(cmdName string, args []string) {
	routeToKubectl(cmdName, args)
}
