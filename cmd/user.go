package cmd

import (
	"cc/ennctl/api"
	"cc/ennctl/common"
	"cc/ennctl/console"
	"errors"
	"fmt"
)

//========================= console user RESTful functions =========================

func createUsers(userList []string) error {
	if len(userList) == 0 {
		return errors.New(api.NameIsRequiredError)
	}
	if RoleName != NsAdmin && RoleName != Developer {
		return errors.New(api.RoleNameIsNotValidError)
	}
	var roleName string
	var url string
	switch RoleName {
	case NsAdmin:
		roleName = NsadminAllCap
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/role_assignments/admin", common.AppCtx.GatewayURL, NamespaceName)
	case Developer:
		roleName = DeveloperAllCap
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/role_assignments/developer", common.AppCtx.GatewayURL, NamespaceName)
	default:
		return errors.New(api.RoleIsRequiredError)
	}
	payload := api.UserCreateRequest{}
	for _, user := range userList {
		tmp := api.RoleInfo{
			UserID: user,
			Role:   roleName,
		}
		payload.AddList = append(payload.AddList, tmp)
	}
	jsonBytes, err := payload.JSON()
	if err != nil {
		return err
	}
	res, code, err := common.Exec(false, url, common.POST, jsonBytes)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     User,
		ResourceName: userList[0],
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}

func createUserFromResource(r *api.Resource) error {
	u, err := r.User()
	if err != nil {
		return err
	}
	if err = u.Valid(); err != nil {
		return err
	}
	j, err := u.UserCreateRequest(api.AddUserActionType).JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/role_assignments/maintain", common.AppCtx.GatewayURL, u.ObjectMeta.Namespace)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Create,
		Resource:     User,
		ResourceName: u.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}

func deleteUsers(users []string) error {
	if users == nil {
		return errors.New(api.NameIsRequiredError)
	}
	if RoleName != NsAdmin && RoleName != Developer {
		return errors.New(api.RoleNameIsNotValidError)
	}
	var roleName string
	var url string
	switch RoleName {
	case NsAdmin:
		roleName = NsadminAllCap
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/role_assignments/admin", common.AppCtx.GatewayURL, NamespaceName)
	case Developer:
		roleName = DeveloperAllCap
		url = fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/role_assignments/developer", common.AppCtx.GatewayURL, NamespaceName)
	default:
		return errors.New(api.RoleIsRequiredError)
	}
	ucr := api.UserCreateRequest{}
	for _, user := range users {
		tmp := api.RoleInfo{
			UserID: user,
			Role:   roleName,
		}
		ucr.RemoveList = append(ucr.RemoveList, tmp)
	}
	j, err := ucr.JSON()
	if err != nil {
		return err
	}
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Delete,
		Resource:      User,
		ResourceNames: users,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}

func getUsers(users []string) error {
	if RoleName != NsAdmin && RoleName != Developer && RoleName != EmptyResource {
		return errors.New(api.RoleNameIsNotValidError)
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/users", common.AppCtx.GatewayURL, NamespaceName)
	res, code, err := common.Exec(false, url, common.GET, nil)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:      code,
		Action:        Get,
		Resource:      User,
		ResourceNames: users,
		Filter:        RoleName,
		OutputFormat:  OutputFormat,
		RawResponse:   res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}

func deleteUserFromResource(r *api.Resource) error {
	u, err := r.User()
	if err != nil {
		return err
	}
	if err = u.Valid(); err != nil {
		return err
	}
	j, err := u.UserCreateRequest(api.RemoveUserActionType).JSON()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/gw/be/api/v1/kubernetes/namespaces/%s/role_assignments/maintain", common.AppCtx.GatewayURL, u.ObjectMeta.Namespace)
	res, code, err := common.Exec(false, url, common.POST, j)
	if err != nil {
		return err
	}
	cfg := console.PrinterConfig{
		HTTPCode:     code,
		Action:       Delete,
		Resource:     User,
		ResourceName: u.ObjectMeta.Name,
		OutputFormat: OutputFormat,
		RawResponse:  res,
	}
	up, err := cfg.UserPrinter()
	if err != nil {
		return err
	}
	return up.Print()
}
