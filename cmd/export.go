package cmd

import (
	"fmt"
	"os"

	"cc/ennctl/common"

	"cc/ennctl/api"
	"errors"

	"github.com/spf13/cobra"
)

var exportCmd = &cobra.Command{
	Use:   "export (APP [NAME]) [flags]",
	Short: "export an app to file.",
	Run: func(cmd *cobra.Command, args []string) {
		// init context
		if err := initContext(); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
		// run exec cmd
		if err := runExport(cmd, args); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	RootCmd.AddCommand(exportCmd)
}

func runExport(cmd *cobra.Command, args []string) error {
	common.Logger.Log("Namespace", NamespaceName, "Verbose", Verbose, "args", fmt.Sprintf("%s", args))
	if len(args) == 0 {
		return errors.New(api.ResourceIsRequiredError)
	}
	var resources []string
	if len(args) > 1 {
		resources = args[1:]
	} else {
		resources = nil
	}
	switch args[0] {
	case App, Apps:
		return exportApps(resources)
	default:
		return errors.New("export " + args[0] + " is not support")
	}
}
