.PHONY: glide
glide:
	@ mkdir -p $$GOPATH/bin
	@ curl https://glide.sh/get | sh;

.PHONY: vendor
vendor: glide.yaml glide.lock
	@ glide install

.PHONY: install
install: vendor
	@ go install

.PHONY: clean
clean:
	@ rm -f $$GOPATH/bin/ennctl

.PHONY: deepclean
deepclean:
	@ rm -f $$GOPATH/bin/ennctl && rm -rf vendor
